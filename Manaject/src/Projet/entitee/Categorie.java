package Projet.entitee;

import java.util.TreeSet;


public class Categorie {
	private String nom;
	private String colorFond;
	private String colorEcriture;
	private TreeSet<Goal> objectifs;
	
	public Categorie(String nom, TreeSet<Goal> objectifs, String colorFond, String colorEcriture) {
		this.nom=nom;
		this.objectifs=objectifs;
		this.colorFond=colorFond;
		this.colorEcriture=colorEcriture;
		}
	
	public Categorie(String nom, String colorFond, String colorEcriture) {
		this(nom, new TreeSet<Goal>(), colorFond, colorEcriture);
	}

	public String getNom() {
		return nom;
	}

	public TreeSet<Goal> getObjectifs() {
		return objectifs;
	}
	
	public String getColorFond() {
		return colorFond;
	}
	
	public String getColorEcriture() {
		return colorEcriture;
	}

	public String toString() {
		return this.nom;
	}

	public void setObjectifs(TreeSet<Goal> objectifs) {
		this.objectifs=objectifs;
		
	}
}
