package Projet.entitee;

import java.util.Set;
import java.util.regex.Matcher;

import Projet.Constante;
import Projet.affichage.InterfaceProjet;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

public class ClientEntreprise {
	private String nomEntreprise;
	private String adresseEntreprise;
	private String adresseEmailEntreprise;
	private String numEntreprise;
	private String faxEntreprise;
	private Set<ClientEmploye> personeAContacter;

	private double budjetDonne;
	private String descriptionProjet;

	public ClientEntreprise(String nomEntreprise, String adresseEntreprise, String adresseEmailEntreprise,
			String numEntreprise, String faxEntreprise, double budjetDonne, String descriptionProjet,
			Set<ClientEmploye> personeAContacter) {
		this.nomEntreprise = nomEntreprise;
		this.adresseEntreprise = adresseEntreprise;
		this.adresseEmailEntreprise = adresseEmailEntreprise;
		this.numEntreprise = numEntreprise;
		this.faxEntreprise = faxEntreprise;
		this.budjetDonne = budjetDonne;
		this.descriptionProjet = descriptionProjet;
		this.personeAContacter = personeAContacter;
	}

	public String getNomEntreprise() {
		return nomEntreprise;
	}

	public String getAdresseEntreprise() {
		return adresseEntreprise;
	}

	public String getAdresseEmailEntreprise() {
		return adresseEmailEntreprise;
	}

	public String getNumEntreprise() {
		return numEntreprise;
	}

	public String getFaxEntreprise() {
		return faxEntreprise;
	}

	public double getBudjetDonne() {
		return budjetDonne;
	}

	public String getDescriptionProjet() {
		return descriptionProjet;
	}

	public Set<ClientEmploye> getPersoneAContacter() {
		return personeAContacter;
	}

	public boolean verifier(Label warning, TextField textFieldNom, TextField textFieldNumero, TextField textFieldFax,
			TextField textFieldEmail) {
		return nomVerif(warning, textFieldNom) && numVerif(textFieldNumero, warning) && numVerif(textFieldFax, warning)
				&& emailVerif(warning, textFieldEmail);
	}

	private boolean emailVerif(Label warning, TextField textFieldEmail) {
		Matcher controleEmail = Constante.PATTERN_EMAIL.matcher(textFieldEmail.getText());
		if (!controleEmail.matches() && !(InterfaceProjet.textFieldVide(textFieldEmail))) {
			warning.setText("Email invalide");
			return false;
		} else {
			return true;
		}
	}

	private boolean numVerif(TextField textField, Label warning) {
		Matcher controleTel = Constante.PATTERN_PHONE.matcher(textField.getText());
		if (!controleTel.matches() && !(InterfaceProjet.textFieldVide(textField))) {
			warning.setText("Telephone invalide");
			return false;
		} else {
			return true;
		}
	}

	private boolean nomVerif(Label warning, TextField textFiedlNom) {
		if (InterfaceProjet.textFieldVide(textFiedlNom)) {
			warning.setText("Le nom de l'entreprise est obligatoire");
			return false;
		} else {
			return true;
		}
	}
}
