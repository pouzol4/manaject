package Projet.entitee;

import java.util.TreeSet;

public class Utilisateur extends Ressource {

	public Utilisateur(String nom, String prenom, String surnom, String eMail, String telephone, boolean user,
			String chemin, double salaireHorraire, TreeSet<PointFort> pointsForts) {
		super(nom, prenom, surnom, eMail, telephone, user, chemin, salaireHorraire, pointsForts);
	}

}
