package Projet.entitee;

public enum EtatObjectif {
	FAIT("FAIT", "#008000"),
	EN_COURS("EN COURS", "#05B8ED"),
	A_FAIRE("A FAIRE", "#808080"), 
	EN_RETARD("EN RETARD", "#FF0000"),
	WARNING("ATTENTION","#FFA500");
	
	private String nom;
	private String color;
	
	private EtatObjectif(String nom, String color) {
		this.nom=nom;
		this.color=color;
	}

	public String getNom() {
		return nom;
	}

	public String getColor() {
		return color;
	}	
}
