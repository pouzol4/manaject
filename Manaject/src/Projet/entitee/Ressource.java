package Projet.entitee;

import java.util.Iterator;
import java.util.TreeSet;
import java.util.regex.Matcher;

import Projet.Constante;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;

public class Ressource implements Comparable<Ressource> {
	private String nom;
	private String prenom;
	private String surnom;
	private String eMail;
	private String telephone;
	private boolean user;
	private double salaireHorraire;
	private String chemin;
	private TreeSet<PointFort> pointsForts;

	public Ressource(String nom, String prenom, String surnom, String eMail, String telephone, boolean user,
			String chemin, double salaireHorraire, TreeSet<PointFort> pointsForts) {
		this.nom = nom;
		this.prenom = prenom;
		this.surnom = surnom;
		this.eMail = eMail;
		this.telephone = telephone;
		this.user = user;
		this.chemin = chemin;
		this.pointsForts = pointsForts;
		this.salaireHorraire = salaireHorraire;
	}

	public String getNom() {
		return this.nom;
	}

	public String getPrenom() {
		return this.prenom;
	}

	public String geteMail() {
		return eMail;
	}

	public String getTelephone() {
		return telephone;
	}

	public String getSurnom() {
		return surnom;
	}

	public boolean getUser() {
		return user;
	}

	public void setSurnom(String surnom) {
		this.surnom = surnom;
	}

	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}

	public void setEmail(String email) {
		this.eMail = email;
	}
	
	public void setUser(boolean user) {
		this.user = user;
	}

	public String toString() {
		return this.nom + " " + this.prenom;
	}

	public boolean verif(Label warningLabel, Projet projet, TextField textFieldNom, TextField textFieldPrenom,
			TextField textFieldEmail, TextField textFieldTelephone, boolean modifier) {

		return verifNom(warningLabel, textFieldNom) && verifPrenom(warningLabel, textFieldPrenom)
				&& verifMemeNomPrenom(warningLabel, projet, textFieldNom, textFieldPrenom, modifier)
				&& verifEmail(warningLabel, textFieldEmail) && verifTelephone(warningLabel, textFieldTelephone);
	}

	private boolean textFieldVide(TextField textField) {
		return textField == null || textField.getText() == null || "".equals(textField.getText());
	}

	private boolean verifNom(Label warningLabel, TextField textFieldNom) {
		if (textFieldVide(textFieldNom) || textFieldNom.getText().equals("FIN_RESSOURCES")) {
			warningLabel.setText("Veuillez ajouter un bon nom");
			return false;
		} else {
			return true;
		}
	}

	private boolean verifPrenom(Label warningLabel, TextField textFieldPrenom) {
		if (textFieldVide(textFieldPrenom) || textFieldPrenom.getText().indexOf(' ') != -1) {
			warningLabel.setText("Veuillez ajouter un bon prenom");
			return false;
		} else {
			return true;
		}
	}

	private boolean verifMemeNomPrenom(Label warningLabel, Projet projet, TextField textFieldNom,
			TextField textFieldPrenom, boolean modifier) {
		Iterator<Ressource> monIterateur = projet.getRessources().iterator();
		boolean mmNomMmPrenom = false;
		while (monIterateur.hasNext()) {
			Ressource ressourceIterator = monIterateur.next();
			if (ressourceIterator.getNom().equalsIgnoreCase(textFieldNom.getText())
					&& ressourceIterator.getPrenom().equalsIgnoreCase(textFieldPrenom.getText())) {
				mmNomMmPrenom = true;
			}
		}

		if (modifier && this.nom.equalsIgnoreCase(textFieldNom.getText())
				&& this.prenom.equalsIgnoreCase(textFieldPrenom.getText())) {
			mmNomMmPrenom = false;
		}

		if (mmNomMmPrenom) {
			warningLabel.setText("Ce nom avec ce m�me pr�nom est utilis�");
			return false;
		} else {
			return true;
		}
	}

	private boolean verifEmail(Label warningLabel, TextField textFieldEmail) {
		if (!(textFieldVide(textFieldEmail))) {
			Matcher controleEmail = Constante.PATTERN_EMAIL.matcher(textFieldEmail.getText());
			if (!controleEmail.matches()) {
				warningLabel.setText("Email Invalide");
				return false;
			} else {

				return true;
			}
		} else {
			textFieldEmail.setText("null");
			return true;
		}

	}

	private boolean verifTelephone(Label warningLabel, TextField textFieldTelephone) {
		Matcher controlePhone = Constante.PATTERN_PHONE.matcher(textFieldTelephone.getText());
		if (!controlePhone.matches() && !(textFieldVide(textFieldTelephone))) {
			warningLabel.setText("Telephone Invalide");
			return false;
		} else {
			if (textFieldVide(textFieldTelephone)) {
				textFieldTelephone.setText("null");
			}
			return true;
		}
	}

	public int compareTo(Ressource ressource) {
		if (ressource.getNom().equals(nom)) {
			return prenom.compareTo(ressource.getPrenom());
		} else {
			return nom.compareTo(ressource.getNom());
		}
	}

	public String getChemin() {
		return chemin;
	}

	public TreeSet<PointFort> getPointsForts() {
		return pointsForts;
	}

	public void affichageDetails(Double taille, VBox vBox) {
		Label nomPrenom = new Label(nom + ' ' + prenom + " (" + salaireHorraire + " �/h)");
		nomPrenom.setMaxWidth(Double.MAX_VALUE);
		nomPrenom.setId("titreVosObjectifs");

		String emailAfficher = eMail;
		if (eMail.equals("null")) {
			emailAfficher = "INCONNU";
		}
		Label email = new Label("Email : " + emailAfficher);
		email.setMinWidth(230);
		email.setId("labelAfficherRessource");

		String telAfficher = telephone.replace('_', '/');
		if (telephone.equals("null")) {
			telAfficher = "INCONNU";
		}
		Label tel = new Label("Tel : " + telAfficher);
		tel.setMinWidth(140);
		tel.setId("labelAfficherRessource");

		GridPane pointsFortsPane = new GridPane();
		int numLigne = 0;
		int numcolonne = 0;

		Iterator<PointFort> iterateur = pointsForts.iterator();
		while (iterateur.hasNext()) {
			if (numcolonne >= 3) {
				numcolonne = 0;
				numLigne++;
			}
			Label pointFortLabel = new Label(iterateur.next().getNom());
			pointFortLabel.setId("pointFortsRessource");
			pointsFortsPane.add(pointFortLabel, numcolonne, numLigne);
			numcolonne++;
		}

		vBox.getChildren().add(nomPrenom);
		vBox.getChildren().add(email);
		vBox.getChildren().add(tel);
		vBox.getChildren().add(pointsFortsPane);
	}

	public double getSalaireHorraire() {
		return salaireHorraire;
	}
}
