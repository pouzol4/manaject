package Projet.entitee;

public enum VolumePriorite {
	FAIBLE("Faible", "#00CB0C"),
	NORMAL("Normal", "#FF9207"),
	FORTE("Forte", "#FF0000");
		
	private String nom;
	private String color;

	private VolumePriorite(String nom, String color) {
		this.nom=nom;
		this.color=color;
	}

	public String getNom() {
		return nom;
	}

	public String getColor() {
		return color;
	}
	
	public String toString() {
		return this.nom;
	}
	
}
