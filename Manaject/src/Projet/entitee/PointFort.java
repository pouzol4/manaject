package Projet.entitee;

import java.util.Iterator;

public class PointFort implements Comparable<PointFort> {
	private String nom;
	
	public PointFort(String nom) {
		this.nom = nom;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}
	
	public int compareTo(PointFort pointFort) {
		return nom.compareTo(pointFort.getNom());
	}
	
	public String toString() {
		return this.nom;
	}

	public boolean existe(Projet projet) {
		boolean trouve = false;
		Iterator<PointFort> monIterateur = projet.getPointForts().iterator();
		while(monIterateur.hasNext()) {
			if(monIterateur.next().getNom().equals(this.nom)) {
				trouve = true;
			}
		}
		return trouve;
	}
	
}
