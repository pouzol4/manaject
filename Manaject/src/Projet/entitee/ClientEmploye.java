package Projet.entitee;

public class ClientEmploye {
	private String civilite;
	private String prenom;
	private String nom;
	private String poste;
	private String telephone;
	private String email;
	private ClientEntreprise entreprise;
	
	public ClientEmploye(String civilite, String prenom, String nom, String poste, String telephone, String email,
			ClientEntreprise entreprise) {
		this.civilite = civilite;
		this.prenom = prenom;
		this.nom = nom;
		this.poste = poste;
		this.telephone = telephone;
		this.email = email;
		this.entreprise = entreprise;
	}

	public String getCivilite() {
		return civilite;
	}

	public String getPrenom() {
		return prenom;
	}

	public String getNom() {
		return nom;
	}

	public String getPoste() {
		return poste;
	}

	public String getTelephone() {
		return telephone;
	}

	public String getEmail() {
		return email;
	}

	public ClientEntreprise getEntreprise() {
		return entreprise;
	}
}
