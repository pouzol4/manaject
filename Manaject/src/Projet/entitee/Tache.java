package Projet.entitee;

public class Tache {
	private String nom;
	private boolean fait;
	
	public Tache(String nom, boolean fait) {
		this.nom = nom;
		this.fait = fait;
	}
	
	public 	Tache(String nom) {
		this(nom, false);
	}

	public String getNom() {
		return nom;
	}

	public boolean isFait() {
		return fait;
	}
	
	public void setFait(boolean fait) {
		this.fait = fait;
	}
}
