package Projet.entitee;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map.Entry;
import java.util.Scanner;
import java.util.Set;
import java.util.TreeSet;

import Projet.Constante;
import Projet.VariableCommune;
import Projet.affichage.InterfaceProjet;
import Projet.autre.FonctionnaliteInterfaceProjet;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.layout.GridPane;
import javafx.stage.Modality;
import javafx.stage.Stage;

public class Projet {

	/*----------ATTRIBUTS----------*/
	private String nom;
	private TreeSet<Ressource> ressources;
	private TreeSet<Goal> objectifs;
	private Set<Categorie> categories;
	private Set<PointFort> pointForts;
	private ClientEntreprise client;

	/*----------CONSTRUCTEURS----------*/
	public Projet(String nom, ClientEntreprise client) {
		this.nom = nom;
		this.ressources = new TreeSet<>();
		this.objectifs = new TreeSet<>();
		this.categories = new HashSet<>();
		this.pointForts = new HashSet<>();
		this.client = client;
	}

	/*----------GETTERS----------*/
	public String getNom() {
		return nom;
	}

	public TreeSet<Ressource> getRessources() {
		return ressources;

	}

	public TreeSet<Goal> getObjectifs() {
		return objectifs;
	}

	public Set<Categorie> getCategories() {
		return categories;
	}

	public Categorie getCategorieVide() {
		Iterator<Categorie> iterateurCategorie = this.categories.iterator();
		while (iterateurCategorie.hasNext()) {
			Categorie categorieVide = iterateurCategorie.next();
			if ("Vide".equals(categorieVide.getNom())) {
				return categorieVide;
			}
		}
		return null;
	}

	public Set<PointFort> getPointForts() {
		return this.pointForts;
	}

	public ClientEntreprise getClient() {
		return this.client;
	}

	public Ressource getUtilisateur() {
		Ressource ressource;
		Iterator<Ressource> iterateur = this.ressources.iterator();
		while (iterateur.hasNext()) {
			ressource = iterateur.next();
			if (ressource.getUser()) {
				return ressource;
			}
		}
		return null;
	}

	/*----------SETTERS----------*/

	public void setNom(String nom) {
		this.nom = nom;
	}

	public void setRessource(Ressource ressource) {
		this.ressources.add(ressource);
	}

	public void setRessources(TreeSet<Ressource> ressources) {
		this.ressources = ressources;
	}

	public void setObjectifs(TreeSet<Goal> objectifs) {
		this.objectifs = objectifs;
	}

	public void setObjectif(Goal objectif) {
		this.objectifs.add(objectif);
	}

	public void setCategorie(Categorie categorie) {
		this.categories.add(categorie);
	}

	/*----------METHODES----------*/

	public void nouveau(Stage primaryStage, Label info) throws IOException {
		if (this.nom == null) {
			FonctionnaliteInterfaceProjet.nouveauProjet(primaryStage, false, this, info);
		} else {
			try (FileReader lecteur = new FileReader("./src/ressources/user.txt"); Scanner s = new Scanner(lecteur)) {
				String nomUser = s.next();
				String prenomUser = s.next();
				s.next();
				String emailUser = s.next();
				String telephoneUser = s.next();
				boolean user = s.nextBoolean();
				Ressource ressource = new Ressource(nomUser, prenomUser, null, emailUser, telephoneUser, user,
						"nothing", 0, new TreeSet<PointFort>());
				this.ressources.add(ressource);

			} catch (Exception e) {
				System.err.println("Erreur dans l'ecriture du fichier de sauvegarde");

			}

			new File(Constante.CHEMIN_DOSSIER_SAVE + this.nom).mkdir();
			new File(Constante.CHEMIN_DOSSIER_SAVE + this.nom + "/images").mkdir();
			this.getCategories().add(new Categorie("Vide", "#565656", "#000000"));
			sauvegarder(info, primaryStage);
			InterfaceProjet interfaceProjet = new InterfaceProjet(primaryStage);
			interfaceProjet.pageProjet(this, InterfaceProjet.pageCentreNum�ro, "");
		}
	}

	public void sauvegarder(Label info, Stage primaryStage) throws IOException {
		sauvegardeClient();
		sauvegardePointFort();
		sauvegardeRessources();
		sauvegardeObjectifs();
		sauvegardeTache();
		sauvegardeCategorie();
		sauvegardeSaveProjet();
		primaryStage.setTitle("Manaject - " + this.nom);
		InterfaceProjet.historique("La sauvegarde du projet " + nom + " a �t� effectu�\n"
				+ "________________________________________________________________");
		InterfaceProjet.modifier = false;
		info.setText("Sauvegarde effectu�e");

	}

	private void sauvegardeClient() throws IOException {
		if (this.client != null) {
			try (FileWriter ecrivain = new FileWriter(
					Constante.CHEMIN_DOSSIER_SAVE + this.nom + "/" + this.nom + Constante.FIN_CLIENT_TXT);
					PrintWriter afficheur = new PrintWriter(ecrivain)) {
				afficheur.write(this.client.getNomEntreprise() + '\n' + this.client.getAdresseEntreprise() + '\n'
						+ this.client.getAdresseEmailEntreprise() + '\n' + +this.client.getBudjetDonne() + '\n'
						+ this.client.getNumEntreprise() + '\n' + this.client.getFaxEntreprise() + '\n'
						+ this.client.getDescriptionProjet() + "\nFIN_DESCRIPTION_CLIENT");
			}
		}

	}

	private void sauvegardeTache() throws IOException {
		try (FileWriter ecrivain = new FileWriter(
				Constante.CHEMIN_DOSSIER_SAVE + this.nom + "/" + this.nom + Constante.FIN_TACHE_TXT);
				PrintWriter afficheur = new PrintWriter(ecrivain)) {

			Iterator<Goal> monIterateur = objectifs.iterator();
			while (monIterateur.hasNext()) {
				Goal objectif = monIterateur.next();
				afficheur.write(objectif.getNom() + '\n');

				Iterator<Entry<Integer, Tache>> monIterateurTache = objectif.getTaches().entrySet().iterator();
				while (monIterateurTache.hasNext()) {
					Tache tache = monIterateurTache.next().getValue();
					afficheur.write(tache.getNom() + '\n' + tache.isFait() + '\n');
				}
				afficheur.write("FIN_TACHE\n");
			}

		}

	}

	public void sauvegardeSaveProjet() {
		try (FileWriter ecrivain = new FileWriter(Constante.GESTION_PROJET_SAVE)) {
			ecrivain.write(this.nom + '\n');
			ecrivain.write(String.valueOf(InterfaceProjet.afficheRessourceBool) + '\n');
			ecrivain.write(VariableCommune.faitAfficher + " " + VariableCommune.enCoursAfficher + " "
					+ VariableCommune.aFaireAfficher + " " + VariableCommune.auRetardAfficher + " "
					+ VariableCommune.warningAfficher + '\n' + InterfaceProjet.styleCss);
		} catch (Exception e) {
			System.err.println("Erreur");
		}

	}

	private void sauvegardeCategorie() throws IOException {
		try (FileWriter ecrivain = new FileWriter(
				Constante.CHEMIN_DOSSIER_SAVE + this.nom + "/" + this.nom + Constante.FIN_CATEGORIE_TXT);
				PrintWriter afficheur = new PrintWriter(ecrivain)) {

			Iterator<Categorie> monIterateur = categories.iterator();
			while (monIterateur.hasNext()) {
				Categorie categorie = monIterateur.next();
				afficheur.write(categorie.getNom() + '\n' + categorie.getColorFond() + '\n'
						+ categorie.getColorEcriture() + '\n');
			}

		}

	}

	@SuppressWarnings("deprecation")
	private void sauvegardeObjectifs() throws IOException {
		try (FileWriter ecrivain = new FileWriter(
				Constante.CHEMIN_DOSSIER_SAVE + this.nom + "/" + this.nom + Constante.FIN_OBJECTIFS_TXT);
				PrintWriter afficheur = new PrintWriter(ecrivain)) {
			Iterator<Goal> monIterateur = objectifs.iterator();
			while (monIterateur.hasNext()) {
				Goal objectif = monIterateur.next();
				afficheur.write(objectif.getNom() + "\n" + objectif.getEtat() + "\n" + objectif.getVolumePriorite()
						+ "\n" + objectif.getDescription() + "\nEnd_Fin\n" + objectif.getDateDebut().getYear() + " "
						+ objectif.getDateDebut().getMonth() + " " + objectif.getDateDebut().getDate() + "\n"
						+ objectif.getDateFin().getYear() + " " + objectif.getDateFin().getMonth() + " "
						+ objectif.getDateFin().getDate() + "\n" + objectif.getNbrTache() + '\n');

				if (objectif.getDateValidation() != null) {
					afficheur.write(objectif.getDateValidation().getYear() + " " + objectif.getDateValidation().getMonth() + " "
						+ objectif.getDateValidation().getDate() + '\n');
				}else {
					afficheur.write("null" + '\n');
				}

				Iterator<Ressource> monIterateurRessource = objectif.getRessources().iterator();
				String ressourcesString = "";
				while (monIterateurRessource.hasNext()) {
					Ressource ressource = monIterateurRessource.next();
					ressourcesString += ressource.getNom() + " " + ressource.getPrenom() + " ";
				}
				ressourcesString += "FIN_RESSOURCES\n";
				afficheur.write(ressourcesString);
				afficheur.write(objectif.getCategorie().getNom() + '\n');
			}
		}

	}

	private void sauvegardeRessources() throws IOException {
		// Sauvegarde des ressources
		try (FileWriter ecrivain = new FileWriter(
				Constante.CHEMIN_DOSSIER_SAVE + this.nom + "/" + this.nom + Constante.FIN_RESSOURCE_TXT);
				PrintWriter afficheur = new PrintWriter(ecrivain)) {
			afficheur.write(this.nom + "\n");

			Iterator<Ressource> monIterateur = ressources.iterator();
			while (monIterateur.hasNext()) {
				Ressource ressource = monIterateur.next();

				String surnomString = null;
				if (ressource.getSurnom() != null) {
					surnomString = ressource.getSurnom().replace(' ', '_');
				}

				afficheur.write(ressource.getNom().replace(' ', '_') + " " + ressource.getPrenom() + " " + surnomString
						+ " " + ressource.geteMail() + " " + ressource.getTelephone() + " " + ressource.getUser() + " "
						+ ressource.getSalaireHorraire() + " " + ressource.getChemin() + "\n");

				Iterator<PointFort> iterateurPointFort = ressource.getPointsForts().iterator();
				while (iterateurPointFort.hasNext()) {
					afficheur.write(iterateurPointFort.next().getNom() + '\n');
				}
				afficheur.write("FIN_POINT_FORT\n");
			}

		}

	}

	private void sauvegardePointFort() throws IOException {
		// Sauvegarde des ressources
		try (FileWriter ecrivain = new FileWriter(
				Constante.CHEMIN_DOSSIER_SAVE + this.nom + "/" + this.nom + Constante.FIN_POINT_FORT_TXT);
				PrintWriter afficheur = new PrintWriter(ecrivain)) {
			Iterator<PointFort> monIterateur = this.pointForts.iterator();
			while (monIterateur.hasNext()) {
				PointFort pointFort = monIterateur.next();
				afficheur.write(pointFort.getNom() + '\n');
			}

		}

	}

	public void chargement() {
		try {
			chargementPointFort();
			chargementClient();
			chargementRessources();
			chargementCategorie();
			chargementObjectifs();
			chargementTaches();
		} catch (FileNotFoundException e) {
			Alert fichierNonTrouve = new Alert(AlertType.ERROR);
			fichierNonTrouve.setContentText("Fichier non trouv�");
			fichierNonTrouve.showAndWait();
			System.exit(2);

		} catch (IOException e) {
			System.err.println("Erreur fichier");
			System.exit(3);
		}
	}

	private void chargementClient() throws FileNotFoundException, IOException {
		File fichierClient = new File(
				Constante.CHEMIN_DOSSIER_SAVE + this.nom + "/" + this.nom + Constante.FIN_CLIENT_TXT);
		if (fichierClient.isFile()) {
			try (FileReader lecteur = new FileReader(
					Constante.CHEMIN_DOSSIER_SAVE + this.nom + "/" + this.nom + Constante.FIN_CLIENT_TXT);
					Scanner s3 = new Scanner(lecteur)) {
				String nomEntreprise = s3.nextLine();
				String adresse = s3.nextLine();
				String adresseEmail = s3.nextLine();
				Double budjet = s3.nextDouble();
				s3.nextLine();
				String numEntreprise = s3.nextLine();
				String faxEntreprise = s3.nextLine();

				String description = "";
				String ligneDescription = s3.nextLine();
				while (!(ligneDescription.equals("FIN_DESCRIPTION_CLIENT"))) {
					description.concat(ligneDescription + '\n');
					ligneDescription = s3.nextLine();
				}
				client = new ClientEntreprise(nomEntreprise, adresse, adresseEmail, numEntreprise, faxEntreprise,
						budjet, description, null);
			}
		}
	}

	private void chargementPointFort() throws FileNotFoundException, IOException {
		try (FileReader lecteur = new FileReader(
				Constante.CHEMIN_DOSSIER_SAVE + this.nom + "/" + this.nom + Constante.FIN_POINT_FORT_TXT);
				Scanner s3 = new Scanner(lecteur)) {
			while (s3.hasNext()) {
				PointFort pointFort = new PointFort(s3.nextLine());
				this.pointForts.add(pointFort);
			}
		}
	}

	private void chargementTaches() throws FileNotFoundException, IOException {
		try (FileReader lecteur = new FileReader(
				Constante.CHEMIN_DOSSIER_SAVE + this.nom + "/" + this.nom + Constante.FIN_TACHE_TXT);
				Scanner s3 = new Scanner(lecteur)) {

			String nomObj;
			String nom;

			while (s3.hasNext()) {
				nomObj = s3.nextLine();
				Goal objectif;

				Iterator<Goal> iterateurGoal = this.objectifs.iterator();
				while (iterateurGoal.hasNext()) {
					objectif = iterateurGoal.next();
					if (objectif.getNom().equals(nomObj)) {

						nom = s3.nextLine();
						while (!(nom.equals("FIN_TACHE"))) {
							objectif.getTaches().put(objectif.getTaches().size(), new Tache(nom, s3.nextBoolean()));
							s3.nextLine();
							nom = s3.nextLine();
						}
					}
				}

			}

		}

	}

	private void chargementCategorie() throws FileNotFoundException, IOException {
		try (FileReader lecteur3 = new FileReader(
				Constante.CHEMIN_DOSSIER_SAVE + this.nom + "/" + this.nom + Constante.FIN_CATEGORIE_TXT);
				Scanner s3 = new Scanner(lecteur3)) {

			String nom;
			String colorFond;
			String colorEcriture;

			while (s3.hasNext()) {
				nom = s3.nextLine();
				colorFond = s3.nextLine();
				colorEcriture = s3.nextLine();
				TreeSet<Goal> objectifSet = new TreeSet<>();
				Categorie categorie = new Categorie(nom, objectifSet, colorFond, colorEcriture);
				this.setCategorie(categorie);
			}

		}
	}

	@SuppressWarnings("deprecation")
	private void chargementObjectifs() throws FileNotFoundException, IOException {
		try (FileReader lecteur2 = new FileReader(
				Constante.CHEMIN_DOSSIER_SAVE + this.nom + "/" + this.nom + Constante.FIN_OBJECTIFS_TXT);
				Scanner s2 = new Scanner(lecteur2)) {
			String nomObjetifs;
			String lectureEtatObjectifs;
			String lectureVolumePriorite;
			String mot1Description;
			EtatObjectif etatObjectifs;
			VolumePriorite volumePriorite;
			int yearObjectifs;
			int mouthObjectifs;
			int dayObjectifs;
			Date dateDebut;
			Date dateFin;
			int nbrTache;
			Date dateValidation;

			while (s2.hasNext()) {
				String descriptionObjectifs = "";
				nomObjetifs = s2.nextLine();
				lectureEtatObjectifs = s2.nextLine();
				etatObjectifs = lectureEtatObjectifs(lectureEtatObjectifs);
				lectureVolumePriorite = s2.nextLine();
				volumePriorite = lectureVolumePriorite(lectureVolumePriorite);

				mot1Description = s2.next();
				while (!("End_Fin".equals(mot1Description))) {
					descriptionObjectifs += mot1Description;
					descriptionObjectifs += s2.nextLine();
					descriptionObjectifs += "\n";
					mot1Description = s2.next();
				}
				s2.nextLine();
				yearObjectifs = s2.nextInt();
				mouthObjectifs = s2.nextInt();
				dayObjectifs = s2.nextInt();
				dateDebut = new Date(yearObjectifs, mouthObjectifs, dayObjectifs);
				yearObjectifs = s2.nextInt();
				mouthObjectifs = s2.nextInt();
				dayObjectifs = s2.nextInt();
				dateFin = new Date(yearObjectifs, mouthObjectifs, dayObjectifs);

				nbrTache = s2.nextInt();
				s2.nextLine();
				String date = s2.nextLine();
				if (date.equals("null")) {
					dateValidation = null;
				} else {
					// dateValidation = new Date(s2.nextInt(), s2.nextInt(), s2.nextInt())
					String[] split = date.split(" ");
					dateValidation = new Date(Integer.parseInt(split[0]), Integer.parseInt(split[1]), Integer.parseInt(split[2]));
				}

				HashSet<Ressource> ressourcesSet = new HashSet<>();
				String nomString = s2.next();
				String prenom;
				while (!(nomString.equals("FIN_RESSOURCES"))) {

					if (!(nomString.equals("FIN_RESSOURCES"))) {
						prenom = s2.next();
						Iterator<Ressource> monIteratorRessource = this.getRessources().iterator();
						while (monIteratorRessource.hasNext()) {
							Ressource ressource = monIteratorRessource.next();
							if (ressource.getNom().equals(nomString) && ressource.getPrenom().equals(prenom)) {
								ressourcesSet.add(ressource);
							}
						}

						nomString = s2.next();
					}
				}
				s2.nextLine();
				Categorie categorieGood = new Categorie("", "", "");
				Iterator<Categorie> iterateurCategorie = this.categories.iterator();
				String categorieNom = s2.nextLine();
				while (iterateurCategorie.hasNext()) {
					Categorie categorie = iterateurCategorie.next();
					if (categorieNom.equals(categorie.getNom())) {
						categorieGood = categorie;
					}
				}
				Goal objectif = new Goal(nomObjetifs, etatObjectifs, volumePriorite, descriptionObjectifs, this,
						dateDebut, dateFin, ressourcesSet, categorieGood, new HashMap<Integer, Tache>(), dateValidation);
				categorieGood.getObjectifs().add(objectif);
				this.setObjectif(objectif);
				objectif.setNbrTache(nbrTache);
			}
		}
	}

	private void chargementRessources() throws FileNotFoundException, IOException {
		try (FileReader lecteur = new FileReader(
				Constante.CHEMIN_DOSSIER_SAVE + this.nom + "/" + this.nom + Constante.FIN_RESSOURCE_TXT);
				Scanner s = new Scanner(lecteur)) {
			s.nextLine();

			String nomRessources;
			String prenomRessources;
			String surnomRessources;
			String emailRessources;
			String telephoneRessources;
			boolean user;
			double salaire;
			String cheminPhoto;
			String nomPointFort;

			while (s.hasNext()) {
				nomRessources = s.next();
				prenomRessources = s.next();
				surnomRessources = s.next();
				emailRessources = s.next();
				telephoneRessources = s.next();
				user = s.nextBoolean();
				salaire = s.nextDouble();
				cheminPhoto = s.next();
				Ressource ressource = new Ressource(nomRessources.replace('_', ' '), prenomRessources,
						surnomRessources.replace('_', ' '), emailRessources, telephoneRessources, user, cheminPhoto,
						salaire, new TreeSet<PointFort>());
				this.setRessource(ressource);

				nomPointFort = s.nextLine();
				while (!(nomPointFort.equals("FIN_POINT_FORT"))) {
					Iterator<PointFort> pointFortIterateur = this.getPointForts().iterator();
					while (pointFortIterateur.hasNext()) {
						PointFort pointFort = pointFortIterateur.next();
						if (pointFort.getNom().equals(nomPointFort)) {
							ressource.getPointsForts().add(pointFort);
						}
					}
					nomPointFort = s.nextLine();
				}
			}

		}

	}

	private VolumePriorite lectureVolumePriorite(String lectureVolumePriorite) {
		VolumePriorite volumePriorite;
		switch (lectureVolumePriorite) {
		case "Faible":
			volumePriorite = VolumePriorite.FAIBLE;
			break;
		case "Normal":
			volumePriorite = VolumePriorite.NORMAL;
			break;
		case "Forte":
			volumePriorite = VolumePriorite.FORTE;
			break;
		default:
			volumePriorite = null;
			break;
		}
		return volumePriorite;
	}

	public EtatObjectif lectureEtatObjectifs(String lectureEtatObjectifs) {
		EtatObjectif etatObjectifs;
		switch (lectureEtatObjectifs) {
		case "FAIT":
			etatObjectifs = EtatObjectif.FAIT;
			break;
		case "EN_COURS":
			etatObjectifs = EtatObjectif.EN_COURS;
			break;
		case "A_FAIRE":
			etatObjectifs = EtatObjectif.A_FAIRE;
			break;
		case "EN_RETARD":
			etatObjectifs = EtatObjectif.EN_RETARD;
			break;
		case "WARNING":
			etatObjectifs = EtatObjectif.WARNING;
			break;
		default:
			etatObjectifs = null;
			break;
		}
		return etatObjectifs;
	}

	public boolean existe(Categorie categorie) {
		boolean existe = false;

		Iterator<Categorie> iterateurCategorie = this.categories.iterator();
		while (iterateurCategorie.hasNext()) {
			if (iterateurCategorie.next().getNom().equals(categorie.getNom())) {
				existe = true;
			}
		}
		return existe;
	}

	public void ajouterPointFort(ComboBox<PointFort> comboBoxPointFort) {
		Stage stageAjouterPointFort = new Stage();
		stageAjouterPointFort.initModality(Modality.APPLICATION_MODAL);
		stageAjouterPointFort.setResizable(false);

		GridPane gridPaneAjouterPointFort = new GridPane();

		/* CREATION DES LABELS, DES BOUTONS ET DES TEXTFIELD */
		Label nomPointFort = new Label("Nom : ");

		TextField textFieldNom = new TextField();

		Button valider = new Button(Constante.VALIDER);
		Button annuler = new Button(Constante.ANNULER);

		/* ACTION */
		valider.setOnMouseClicked(event -> {
			PointFort pointFort = new PointFort(textFieldNom.getText());
			this.pointForts.add(pointFort);
			comboBoxPointFort.setValue(pointFort);
			stageAjouterPointFort.close();
		});

		annuler.setOnMouseClicked(event -> {
			stageAjouterPointFort.close();
		});

		gridPaneAjouterPointFort.add(nomPointFort, 0, 0);
		gridPaneAjouterPointFort.add(textFieldNom, 1, 0);
		gridPaneAjouterPointFort.add(valider, 0, 1);
		gridPaneAjouterPointFort.add(annuler, 1, 1);

		Scene sceneAjouterRessource = new Scene(gridPaneAjouterPointFort);

		stageAjouterPointFort.setTitle("Ajouter Point Fort");
		stageAjouterPointFort.setScene(sceneAjouterRessource);
		stageAjouterPointFort.show();

	}

}
