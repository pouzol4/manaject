package Projet.entitee;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import Projet.Constante;
import Projet.affichage.InterfaceProjet;
import Projet.autre.FonctionnaliteInterfaceProjet;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

public class Goal implements Comparable<Goal> {
	private String nom;
	private EtatObjectif etat;
	private VolumePriorite volumePriorite;
	private String description;
	private Projet projet;
	private Date dateDebut;
	private Date dateFin;
	private Set<Ressource> ressources;
	private Map<Integer, Tache> taches;
	private Categorie categorie;
	private int nbrTache = 0;
	private Date dateValidation;

	public Goal(String nom, EtatObjectif etat, VolumePriorite volumePriorite, String description, Projet projet,
			Date dateDebut, Date dateFin, Set<Ressource> ressources, Categorie categorie, Map<Integer, Tache> taches,
			Date dateValidation) {
		this.etat = etat;
		this.volumePriorite = volumePriorite;
		this.nom = nom;
		this.description = description;
		this.projet = projet;
		this.dateDebut = dateDebut;
		this.dateFin = dateFin;
		this.ressources = ressources;
		this.categorie = categorie;
		this.taches = taches;
		this.dateValidation = dateValidation;
	}

	public Goal(String nom, VolumePriorite volumePriorite, String description, Projet projet, Date dateDebut,
			Date dateFin, Set<Ressource> ressources, Categorie categorie, Map<Integer, Tache> taches) {
		this(nom, EtatObjectif.A_FAIRE, volumePriorite, description, projet, dateDebut, dateFin, ressources, categorie,
				taches, null);
	}

	public String getNom() {
		return this.nom;
	}

	public EtatObjectif getEtat() {
		return this.etat;
	}

	public VolumePriorite getVolumePriorite() {
		return volumePriorite;
	}

	public String getEtatNom() {
		return this.etat.getNom();
	}

	public String getDescription() {
		return description;
	}

	public Projet getProjet() {
		return projet;
	}

	public Date getDateDebut() {
		return dateDebut;
	}

	public Date getDateFin() {
		return dateFin;
	}

	public Set<Ressource> getRessources() {
		return ressources;
	}

	public Map<Integer, Tache> getTaches() {
		return taches;
	}

	public int getNbrTache() {
		return nbrTache;
	}

	public Categorie getCategorie() {
		return categorie;
	}

	public Date getDateValidation() {
		return this.dateValidation;
	}

	public void setNbrTache(int nbrTache) {
		this.nbrTache = nbrTache;
	}

	public void setDateFin(Date dateFin) {
		this.dateFin = dateFin;
	}

	public void setEtat(EtatObjectif etatObjectif) {
		this.etat = etatObjectif;
	}

	public void setCategorie(Categorie categorie) {
		this.categorie = categorie;
	}

	@SuppressWarnings("deprecation")
	public void objectifclot() {
		this.dateValidation = Date.from(LocalDate.now().atStartOfDay(ZoneId.systemDefault()).toInstant());
		this.dateValidation.setYear(this.dateValidation.getYear() + 1900);
		this.dateValidation.setMonth(this.dateValidation.getMonth() + 1);
	}

	public void addNbrTache() {
		this.nbrTache++;
	}

	public String toString() {
		return this.nom;
	}

	public boolean verif(Label warning, TextField textFieldNom, TextArea textAreaDescription, Projet projet,
			String nomModifie) {
		return verifNom(warning, textFieldNom, projet, nomModifie) && verifDescription(warning, textAreaDescription);
	}

	private boolean verifDescription(Label warning, TextArea textAreaDescription) {
		if (textAreaDescription.getText().contains("End_Fin")) {
			warning.setText("End_Fin est interdit");
			return false;
		} else {
			if (textAreaDescriptionVide(textAreaDescription)) {
				textAreaDescription.setText("");

			}
			return true;
		}
	}

	private boolean verifNom(Label warning, TextField textFieldNom, Projet projet, String nomModifie) {
		if (InterfaceProjet.textFieldVide(textFieldNom)) {
			warning.setText("Un objectif doit avoir un nom !");
			return false;
		} else {
			if (textFieldNom.getText().contains("End_Fin")) {
				warning.setText("End_Fin est interdit");
				return false;
			} else {
				if (existe(textFieldNom, projet, nomModifie)) {
					warning.setText("ce nom d'objectif est d�j� existant");
					return false;
				} else {
					return true;
				}
			}
		}
	}

	private boolean existe(TextField textFieldNom, Projet projet, String nomModifie) {
		Iterator<Goal> iterateurObjectif = projet.getObjectifs().iterator();
		boolean trouve = false;

		while (iterateurObjectif.hasNext()) {
			Goal objectif = iterateurObjectif.next();
			if (objectif.getNom().equals(textFieldNom.getText())) {
				if (nomModifie != null && !(nomModifie.equals(objectif.getNom())))
					trouve = true;

			}
		}
		return trouve;
	}

	public boolean textAreaDescriptionVide(TextArea textAreaDescription) {
		return textAreaDescription == null || textAreaDescription.getText() == null
				|| "".equals(textAreaDescription.getText());
	}

	public boolean verif(Label warning, ComboBox<Goal> comboBoxObjectif) {
		if (comboBoxObjectif.getValue().getEtat() == EtatObjectif.FAIT) {
			warning.setText("Cet objectif a d�j� �t� valid�");
			return false;
		} else {
			if (comboBoxObjectif.getValue().getEtat() != EtatObjectif.EN_COURS) {
				warning.setText("L'objectif n'est pas en cours, Impossible de le valider");
				return false;
			} else {
				this.setEtat(EtatObjectif.FAIT);
				return true;
			}
		}
	}

	public void affichage(GridPane gridPaneObjectifs, int numeroLigne, Stage primaryStage) {
		Label priorite = new Label("");
		priorite.setMinWidth(6);
		priorite.setMaxHeight(Double.MAX_VALUE);
		priorite.setBackground(new Background(
				new BackgroundFill(Color.web(this.volumePriorite.getColor()), CornerRadii.EMPTY, Insets.EMPTY)));

		Label verif = new Label(this.getNom());
		verif.setMinWidth(150);
		verif.setMaxWidth(150);

		Label description = new Label();
		@SuppressWarnings("deprecation")
		int mouth = this.getDateFin().getMonth() + 1;
		@SuppressWarnings("deprecation")
		int year = this.getDateFin().getYear() + 1900;
		@SuppressWarnings("deprecation")
		Label datePrevu = new Label(this.getDateFin().getDate() + "/" + mouth + "/" + year);

		Label personnes = new Label("");
		personnes.setMaxHeight(Double.MAX_VALUE);

		Label etatObjectif = etatObjectif(this);
		etatObjectif.setMaxHeight(Double.MAX_VALUE);

		description.setMinWidth(180);
		description.setMaxWidth(180);

		if (this.getDescription() == null) {
			description.setText("");
		} else {
			description.setText(this.getDescription());
		}

		Date today = Date.from(LocalDate.now().atStartOfDay(ZoneId.systemDefault()).toInstant());

		if (this.getEtat().equals(EtatObjectif.EN_COURS) && this.getDateFin().compareTo(today) < 0)
			datePrevu.setId("warning");

		HBox hBoxRessources = new HBox();
		hBoxRessources.setSpacing(5);
		hBoxRessources.setMinWidth(135);
		hBoxRessources.setMaxWidth(hBoxRessources.getMinWidth());

		if (this.getRessources().size() <= 4) {
			Iterator<Ressource> monIterateurRessource = this.getRessources().iterator();
			while (monIterateurRessource.hasNext()) {
				Ressource ressource = monIterateurRessource.next();

				ImageView imageView;
				if (ressource.getChemin().equals("nothing")) {
					char charImage;
					char ch = ressource.getNom().charAt(0);
					if ((ch >= 'A' && ch <= 'Z') || (ch >= 'a' && ch <= 'z')) {
						charImage = ressource.getNom().charAt(0);
					} else {
						charImage = '#';
					}
					imageView = new ImageView(
							new Image(getClass().getResourceAsStream("/lettre/" + charImage + ".png")));

				} else {
					imageView = new ImageView(new Image(ressource.getChemin()));
				}

				imageView.setFitWidth(Constante.TAILLE_IMAGE_RESSOURCE);
				imageView.setFitHeight(Constante.TAILLE_IMAGE_RESSOURCE);

				Button buttonImageRessource = new Button("", imageView);
				buttonImageRessource.setTooltip(new Tooltip(ressource.getNom() + " " + ressource.getPrenom()));
				buttonImageRessource.setId("buttonImageRessource");
				hBoxRessources.getChildren().add(buttonImageRessource);
			}
		} else {
			Iterator<Ressource> monIterateurRessource = this.getRessources().iterator();
			for (int i = 0; i < 4; i++) {
				Ressource ressource = monIterateurRessource.next();
				ImageView imageView;
				if (ressource.getChemin().equals("nothing")) {
					char ch = ressource.getNom().charAt(0);
					if ((ch < 'A' && ch > 'Z') || (ch < 'a' && ch > 'z')) {
						ch = '#';
					}
					imageView = new ImageView(new Image(getClass().getResourceAsStream("/lettre/" + ch + ".png")));

				} else {
					imageView = new ImageView(new Image(ressource.getChemin()));
				}

				imageView.setFitWidth(Constante.TAILLE_IMAGE_RESSOURCE);
				imageView.setFitHeight(Constante.TAILLE_IMAGE_RESSOURCE);

				Button buttonImageRessource = new Button("", imageView);
				buttonImageRessource.setTooltip(new Tooltip(ressource.getNom() + " " + ressource.getPrenom()));
				buttonImageRessource.setId("buttonImageRessource");
				hBoxRessources.getChildren().add(buttonImageRessource);
			}
			ImageView imageViewPlus = new ImageView(new Image(getClass().getResourceAsStream("/lettre/+.png")));
			imageViewPlus.setFitWidth(Constante.TAILLE_IMAGE_RESSOURCE);
			imageViewPlus.setFitHeight(Constante.TAILLE_IMAGE_RESSOURCE);

			Button buttonImageRessource = new Button("", imageViewPlus);
			buttonImageRessource.setId("buttonImageRessource");
			
			Ressource ressource = monIterateurRessource.next();
			Tooltip tooltipBouttonPlus = new Tooltip(ressource.getNom() + ' ' + ressource.getPrenom());

			while (monIterateurRessource.hasNext()) {
				ressource = monIterateurRessource.next();
				tooltipBouttonPlus.setText(
						tooltipBouttonPlus.getText() + '\n' + ressource.getNom() + ' ' + ressource.getPrenom());

			}
			buttonImageRessource.setTooltip(tooltipBouttonPlus);
			hBoxRessources.getChildren().add(buttonImageRessource);

		}
		int restant;

		if (this.taches.isEmpty()) {
			restant = 0;
		} else {
			double nbrTacheDouble = nbrTache;
			restant = (int) ((nbrTacheDouble / this.taches.size()) * 100);
		}

		Label pourcentageRestant = new Label(restant + "%");
		pourcentageRestant.setMaxHeight(Double.MAX_VALUE);
		pourcentageRestant.setAlignment(Pos.CENTER);
		pourcentageRestant.setMinWidth(50);

		if (!(this.etat.equals(EtatObjectif.FAIT))) {
			ImageView modifier = new ImageView(new Image(getClass().getResourceAsStream("/imagesIcon/modifier.png")));
			modifier.setFitHeight(Constante.TAILLE_ICON);
			modifier.setFitWidth(Constante.TAILLE_ICON);

			Button buttonModifier = new Button("", modifier);
			buttonModifier.setId("buttonModifier");
			buttonModifier.setPadding(new Insets(7, 16, 7, 16));
			buttonModifier.setTooltip(new Tooltip("modifier"));

			Goal objectif = this;
			buttonModifier.setOnAction(new EventHandler<ActionEvent>() {
				public void handle(ActionEvent event) {
					FonctionnaliteInterfaceProjet.updateObjectif(projet, primaryStage, objectif);
				}
			});
			
			gridPaneObjectifs.add(buttonModifier, 7, numeroLigne);
		} else {
			@SuppressWarnings("deprecation")
			Label dateValidationLabel = new Label("Valid� le \n" +this.dateValidation.getDate() + "/" + this.dateValidation.getMonth() + "/"
					+ this.dateValidation.getYear());
			gridPaneObjectifs.add(dateValidationLabel, 7, numeroLigne);

		}

		gridPaneObjectifs.add(priorite, 0, numeroLigne);
		gridPaneObjectifs.add(verif, 1, numeroLigne);
		gridPaneObjectifs.add(description, 2, numeroLigne);
		gridPaneObjectifs.add(etatObjectif, 3, numeroLigne);
		gridPaneObjectifs.add(datePrevu, 4, numeroLigne);
		gridPaneObjectifs.add(hBoxRessources, 5, numeroLigne);
		gridPaneObjectifs.add(pourcentageRestant, 6, numeroLigne);
	}

	public Label etatObjectif(Goal objectif) {
		Label etatObjectif = new Label(objectif.getEtatNom());
		etatObjectif.setTextFill(Color.WHITE);
		etatObjectif.setAlignment(Pos.CENTER);

		String couleurEtatObjectif = objectif.getEtat().getColor();
		etatObjectif.setBackground(
				new Background(new BackgroundFill(Color.web(couleurEtatObjectif), CornerRadii.EMPTY, Insets.EMPTY)));
		etatObjectif.setMinWidth(75);
		return etatObjectif;
	}

	public boolean dateBonne(Label warning, DatePicker datePikerDateFin) {
		if (this.getDateFin().compareTo(this.getDateDebut()) < 0) {
			warning.setText("Erreur dans les dates !");
			return false;
		} else {
			return true;
		}
	}

	@Override
	public int compareTo(Goal goal) {
		return nom.compareTo(goal.getNom());
	}
}
