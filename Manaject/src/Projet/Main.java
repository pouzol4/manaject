package Projet;

import javafx.stage.Stage;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.Scanner;

import Projet.affichage.InterfaceProjet;
import Projet.autre.FonctionnaliteInterfaceProjet;
import Projet.entitee.Projet;
import javafx.application.Application;
import javafx.scene.control.Label;

public class Main extends Application {	

	public static void main(String[] args) {
		launch(args);
	}

	public void start(final Stage primaryStage) {
		File repertoire = new File("./src/ressources");
		String liste[] = repertoire.list();

		if (liste.length == 0) {
			new File("./src/ressources/save").mkdir();
			try (FileWriter ecrivain = new FileWriter("./src/ressources/users.txt")) {
				ecrivain.write("");
				
			} catch (Exception e) {
				System.err.println("Erreur");
			}
			
			try (FileWriter ecrivain = new FileWriter(Constante.GESTION_PROJET_SAVE)) {
				ecrivain.write("rien");
				Projet projet = new Projet(null, null);

				InscriptionConnexion.inscriptionAffichage(projet, primaryStage);
				
			} catch (Exception e) {
				System.err.println("Erreur");
			}
		} else {
			try (FileReader lecteur = new FileReader(Constante.GESTION_PROJET_SAVE); Scanner s = new Scanner(lecteur)) {
				String nomProj = s.nextLine();
				InterfaceProjet.afficheRessourceBool = s.nextBoolean();
				s.nextLine();
				VariableCommune.faitAfficher = s.nextBoolean();
				VariableCommune.enCoursAfficher = s.nextBoolean();
				VariableCommune.aFaireAfficher = s.nextBoolean();
				VariableCommune.auRetardAfficher = s.nextBoolean();
				VariableCommune.warningAfficher = s.nextBoolean();
				s.nextLine();
				InterfaceProjet.styleCss = s.nextLine();

				InscriptionConnexion.lireUtilisateur();
				
				if (!("rien".equals(nomProj))) {
					Projet projet = new Projet(nomProj, null);
					projet.chargement();
					InterfaceProjet interfaceProjet = new InterfaceProjet(primaryStage);
					interfaceProjet.pageProjet(projet, InterfaceProjet.pageCentreNuméro, "");
				} else {
					Projet projet = new Projet(null, null);
					projet.nouveau(primaryStage, new Label(""));
				}
			} catch (Exception e) {
				File repertoireRessource = new File("./src/ressources/");
				FonctionnaliteInterfaceProjet.emptyDirectory(repertoireRessource);
			}
		}

	}
}
