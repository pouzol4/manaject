package Projet.autre;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.TreeSet;
import java.util.Map.Entry;

import Projet.Constante;
import Projet.affichage.InterfaceProjet;
import Projet.entitee.Categorie;
import Projet.entitee.ClientEmploye;
import Projet.entitee.ClientEntreprise;
import Projet.entitee.EtatObjectif;
import Projet.entitee.Goal;
import Projet.entitee.PointFort;
import Projet.entitee.Projet;
import Projet.entitee.Ressource;
import Projet.entitee.Tache;
import Projet.entitee.VolumePriorite;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.CheckMenuItem;
import javafx.scene.control.ColorPicker;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.FileChooser.ExtensionFilter;

public class FonctionnaliteInterfaceProjet {

	static ClientEntreprise clientEntreprise = null;

	private FonctionnaliteInterfaceProjet() {
	}

	public static void rangerObjectif(Projet projet, Stage primaryStage, Label info) {
		if (!(projet.getObjectifs().isEmpty()) && projet.getCategories().size() > 1) {
			Stage stageAjouterCategorie = new Stage();
			stageAjouterCategorie.initModality(Modality.APPLICATION_MODAL);
			stageAjouterCategorie.setResizable(false);

			GridPane gridPaneRangerObjectif = new GridPane();

			/* LABELS, TEXTFIELD ET BOTTON */
			Label nomCategorie = new Label("Nom de la cat�gorie : ");
			Label nomObjectif = new Label("Nom de l'objectif : ");

			Label warning = new Label("");
			warning.setTextFill(Color.RED);

			Button btnValider = new Button(Constante.VALIDER);
			Button btnAnnuler = new Button(Constante.ANNULER);

			ComboBox<Categorie> comboBoxCategorie = new ComboBox<>();
			comboBoxCategorie.getItems().addAll(projet.getCategories());
			comboBoxCategorie.getItems().remove(projet.getCategorieVide());
			comboBoxCategorie.setValue(comboBoxCategorie.getItems().get(0));

			ComboBox<Goal> comboBoxObjectif = new ComboBox<>();
			comboBoxObjectif.getItems().addAll(projet.getObjectifs());
			comboBoxObjectif.setValue(comboBoxObjectif.getItems().get(0));

			InterfaceProjet interfaceProjet = new InterfaceProjet(primaryStage);
			/* VERIF NOM BON */
			btnValider.setOnMouseClicked(eventValider -> {
				Goal objectif = comboBoxObjectif.getValue();
				Categorie categorie = comboBoxCategorie.getValue();

				if (!(objectif.getCategorie().equals(projet.getCategorieVide()))) {
					Alert changerCategorieAlert = new Alert(AlertType.CONFIRMATION);
					changerCategorieAlert.setTitle("Verification de changement de cat�gorie");
					changerCategorieAlert.setHeaderText(
							"Etes-vous s�r de mettre " + objectif.getNom() + " dans " + categorie.getNom() + " ?");
					ButtonType buttonOui = new ButtonType("Oui");
					ButtonType buttonNon = new ButtonType("Non");
					changerCategorieAlert.getButtonTypes().setAll(buttonOui, buttonNon);
					Optional<ButtonType> choix = changerCategorieAlert.showAndWait();

					if (choix.get() == buttonOui) {
						objectif.getCategorie().getObjectifs().remove(objectif);
						categorie.getObjectifs().add(objectif);
						objectif.setCategorie(categorie);
						info.setText("Vous avez  ranger un objectif");
						stageAjouterCategorie.close();
						interfaceProjet.pageProjet(projet, InterfaceProjet.pageCentreNum�ro, "");
					}

					if (choix.get() == buttonNon) {
						info.setText("Vous avez rang� aucun objectif");
						stageAjouterCategorie.close();
						interfaceProjet.pageProjet(projet, InterfaceProjet.pageCentreNum�ro, "");
					}
				} else {
					projet.getCategorieVide().getObjectifs().remove(objectif);
					categorie.getObjectifs().add(objectif);
					objectif.setCategorie(categorie);
					info.setText("Vous avez  ranger un objectif");
					stageAjouterCategorie.close();
					interfaceProjet.pageProjet(projet, InterfaceProjet.pageCentreNum�ro, "");
				}
			});

			btnAnnuler.setOnMouseClicked(eventAnnuler -> {
				info.setText("Vous avez annul� le rangement d'un objectif");
				stageAjouterCategorie.close();
				interfaceProjet.pageProjet(projet, InterfaceProjet.pageCentreNum�ro, "");
			});

			/* REALISATION DU GRIDPANE */
			gridPaneRangerObjectif.add(nomCategorie, 0, 1);
			gridPaneRangerObjectif.add(comboBoxCategorie, 1, 1);
			gridPaneRangerObjectif.add(nomObjectif, 0, 2);
			gridPaneRangerObjectif.add(comboBoxObjectif, 1, 2);
			gridPaneRangerObjectif.add(btnValider, 0, 3);
			gridPaneRangerObjectif.add(btnAnnuler, 1, 3);
			gridPaneRangerObjectif.add(warning, 0, 4, 2, 1);

			Scene sceneAjouterCategorie = new Scene(gridPaneRangerObjectif);

			stageAjouterCategorie.setTitle("Validation d'un objectif");
			stageAjouterCategorie.setScene(sceneAjouterCategorie);
			stageAjouterCategorie.show();

		} else {
			info.setText("Impossible : il faut au moins une cat�gorie et au moins un objectif");
		}
	}

	public static void nouveauProjet(final Stage primaryStage, boolean supprimer, Projet projetEnCours, Label info) {
		Stage stageCreerProjet = new Stage();
		stageCreerProjet.initModality(Modality.APPLICATION_MODAL);
		stageCreerProjet.setResizable(false);
		GridPane gridPaneCreerProjet = new GridPane();
		gridPaneCreerProjet.setId("gridPaneCreerProjet");
		clientEntreprise = null;

		/* CREATION DES LABELS, DES BOUTONS ET DES TEXTFIELD */
		Label titre = new Label("NOUVEAU PROJET");
		titre.setId("titrePartie");
		titre.setMaxWidth(Double.MAX_VALUE);

		Label nomProjet = new Label("Nom du projet : ");
		Label labelClient = new Label("Client : ");
		Label clientLabel = new Label("");
		clientLabel.setMinWidth(150);

		Button ajoutClient = new Button("Ajouter");
		ajoutClient.setMaxWidth(Double.MAX_VALUE);

		Label warning = new Label("");
		warning.setId("warning");

		TextField textFieldNomProjet = new TextField();

		Button buttonValider = new Button(Constante.VALIDER);
		buttonValider.setId("Valider");
		Button buttonAnnuler = new Button(Constante.ANNULER);
		buttonAnnuler.setId("Annuler");

		HBox hBoxClient = new HBox(labelClient, clientLabel);
		HBox hBoxNomProjet = new HBox(nomProjet, textFieldNomProjet);
		HBox btn = new HBox(buttonValider, buttonAnnuler);
		btn.setSpacing(40);
		btn.setId("barBtn");

		/* ACTION */
		ajoutClient.setOnMouseClicked(event -> {
			ajoutClient(clientLabel, ajoutClient);

		});

		buttonValider.setOnMouseClicked(event -> {
			File repertoire = new File("./src/ressources/save");
			String liste[] = repertoire.list();
			boolean nomExiste = false;

			for (int i = 0; i < liste.length; i++) {
				if (textFieldNomProjet.getText().equals(liste[i])) {
					nomExiste = true;
				}
			}

			if (textFieldVide(textFieldNomProjet)) {
				warning.setText("Veuillez ajouter un nom � votre Projet");
			} else {
				if (nomExiste) {
					warning.setText("Ce nom de projet existe d�j�");
				} else {
					if (textFieldNomProjet.getText().indexOf('<') != -1
							|| textFieldNomProjet.getText().indexOf('>') != -1
							|| textFieldNomProjet.getText().indexOf('"') != -1
							|| textFieldNomProjet.getText().indexOf(':') != -1
							|| textFieldNomProjet.getText().indexOf('|') != -1
							|| textFieldNomProjet.getText().indexOf('?') != -1
							|| textFieldNomProjet.getText().indexOf('/') != -1
							|| textFieldNomProjet.getText().indexOf('\\') != -1
							|| textFieldNomProjet.getText().indexOf('*') != -1) {
						warning.setText("Mauvais caract�re dans le nom du projet");
					} else {
						if (textFieldNomProjet.getText().length() > 30) {
							warning.setText("Nom de projet trop long (30 charact�res max)");
						} else {
							if (supprimer) {
								File repertoireASup = new File(Constante.CHEMIN_DOSSIER_SAVE + projetEnCours.getNom());
								emptyDirectory(repertoireASup);

								InterfaceProjet.historique("Projet supprim� du nom de " + projetEnCours.getNom());
								if (!(repertoireASup.delete())) {
									System.err.println("Mauvaise supression d'un fichier ");
								}

							}
							InterfaceProjet.historique("nouveau projet cr�� du nom de " + textFieldNomProjet.getText());
							fermeturePageSave(projetEnCours, info, textFieldNomProjet.getText().trim(), primaryStage,
									stageCreerProjet, false);

						}
					}
				}
			}
		});

		buttonAnnuler.setOnMouseClicked(event -> {
			stageCreerProjet.close();
			if (projetEnCours.getNom().equals("rien")) {
				InterfaceProjet interfaceProjet = new InterfaceProjet(primaryStage);
				interfaceProjet.pageProjet(projetEnCours, InterfaceProjet.pageCentreNum�ro, "");
			}
		});

		/* GRID PANE ADD */
		gridPaneCreerProjet.add(titre, 0, 0, 2, 1);
		gridPaneCreerProjet.add(hBoxNomProjet, 0, 1, 2, 1);
		gridPaneCreerProjet.add(hBoxClient, 0, 2);
		gridPaneCreerProjet.add(ajoutClient, 1, 2);
		gridPaneCreerProjet.add(btn, 0, 3, 2, 1);
		gridPaneCreerProjet.add(warning, 0, 4, 2, 1);
		gridPaneCreerProjet.setHgap(10);
		gridPaneCreerProjet.setVgap(10);

		Scene sceneCreerProjet = new Scene(gridPaneCreerProjet);
		sceneCreerProjet.getStylesheets().add("CSS/" + InterfaceProjet.styleCss + ".css");

		stageCreerProjet.setTitle("Nouveau projet");
		stageCreerProjet.setScene(sceneCreerProjet);
		stageCreerProjet.show();
	}

	private static void ajoutClient(Label clientLabel, Button ajoutClient) {
		Stage stageCreerClient = new Stage();
		stageCreerClient.initModality(Modality.APPLICATION_MODAL);
		stageCreerClient.setResizable(false);
		clientLabel.setText("");

		GridPane gridPaneCreerClient = new GridPane();
		gridPaneCreerClient.setHgap(10);
		gridPaneCreerClient.setVgap(10);
		gridPaneCreerClient.setPadding(new Insets(10, 10, 10, 10));

		Label titre = new Label("ENTREPRISE");
		titre.setId("titrePartie");
		titre.setMaxWidth(Double.MAX_VALUE);

		Label nomEntreprise = new Label("Nom de l'entreprise *: ");
		Label adresseEntreprise = new Label("Adresse : ");
		Label numEntreprise = new Label("Num�ro de t�l�phone : ");
		Label faxEntreprise = new Label("fax : ");
		Label adresseEmail = new Label("Adresse Email : ");
		Label budjetDonne = new Label("Budget donn� : ");
		Label descriptionProjet = new Label("Description du Projet : ");
		Label warning = new Label("");
		warning.setId("warning");

		TextField textFieldNom = new TextField();
		textFieldNom.setPromptText("Nom de l'entreprise");
		TextField textFieldAdresse = new TextField();
		textFieldAdresse.setPromptText("00 rue azerty 00000 ville");
		TextField textFieldNumero = new TextField();
		textFieldNumero.setPromptText("06 06 06 06 06");
		TextField textFieldFax = new TextField();
		textFieldFax.setPromptText("06 06 06 06 06");
		TextField textFieldEmail = new TextField();
		textFieldEmail.setPromptText("azerty@qsd.cvb");
		TextField textFiedlBudget = new TextField("0");
		TextArea textFiedlDescriptionProjet = new TextArea();

		Button valider = new Button("Valider");
		valider.setId("Valider");
		Button annuler = new Button("Annuler");
		annuler.setId("Annuler");
		Button ajoutContact = new Button("Ajout d'un contact");

		HBox btns = new HBox(valider, annuler);
		btns.setAlignment(Pos.CENTER);
		btns.setSpacing(150);

		if (clientEntreprise != null) {
			textFieldNom.setText(clientEntreprise.getNomEntreprise());
			textFieldAdresse.setText(clientEntreprise.getAdresseEntreprise());
			textFieldNumero.setText(clientEntreprise.getNumEntreprise());
			textFieldFax.setText(clientEntreprise.getFaxEntreprise());
			textFieldEmail.setText(clientEntreprise.getAdresseEmailEntreprise());
			Double budDonne = clientEntreprise.getBudjetDonne();
			textFiedlBudget.setText(budDonne.toString());
			textFiedlDescriptionProjet.setText(clientEntreprise.getDescriptionProjet());

			Button supprimer = new Button("Supprimer");
			supprimer.setId("Annuler");
			btns.getChildren().add(1, supprimer);

			supprimer.setOnMouseClicked(eventSupprimer -> {
				clientEntreprise = null;
				stageCreerClient.close();
				ajoutClient.setText("Ajouter");
			});
		}

		valider.setOnMouseClicked(eventValide -> {
			if (!(InterfaceProjet.textFieldVide(textFiedlBudget))) {
				if (textFiedlBudget.getText().matches("^[0-9]*") || textFiedlBudget.getText().matches("^[0-9]*.[0-9]")
						|| textFiedlBudget.getText().matches("^[0-9]*.[0-9][0-9]")) {

					clientEntreprise = new ClientEntreprise(textFieldNom.getText().trim(), textFieldAdresse.getText().trim(),
							textFieldEmail.getText().trim(), textFieldNumero.getText().trim(), textFieldFax.getText().trim(),
							Double.parseDouble(textFiedlBudget.getText()), textFiedlDescriptionProjet.getText().trim(),
							new HashSet<ClientEmploye>());

					if (clientEntreprise.verifier(warning, textFieldNom, textFieldNumero, textFieldFax,
							textFieldEmail)) {
						clientLabel.setText(clientEntreprise.getNomEntreprise());
						if (clientEntreprise == null) {
							ajoutClient.setText("ajouter");
						} else {
							ajoutClient.setText("modifier");
						}

						stageCreerClient.close();
					}
				} else {
					warning.setText("Le prix indiqu� est incorrect");
				}
			}
		});

		annuler.setOnMouseClicked(event -> {
			stageCreerClient.close();
		});

		gridPaneCreerClient.add(titre, 0, 0, 4, 1);
		gridPaneCreerClient.add(nomEntreprise, 0, 1);
		gridPaneCreerClient.add(textFieldNom, 1, 1, 3, 1);
		gridPaneCreerClient.add(adresseEntreprise, 0, 2);
		gridPaneCreerClient.add(textFieldAdresse, 1, 2, 3, 1);
		gridPaneCreerClient.add(numEntreprise, 0, 3);
		gridPaneCreerClient.add(textFieldNumero, 1, 3);
		gridPaneCreerClient.add(faxEntreprise, 2, 3);
		gridPaneCreerClient.add(textFieldFax, 3, 3);
		gridPaneCreerClient.add(adresseEmail, 0, 4);
		gridPaneCreerClient.add(textFieldEmail, 1, 4);
		gridPaneCreerClient.add(budjetDonne, 2, 4);
		gridPaneCreerClient.add(textFiedlBudget, 3, 4);
		gridPaneCreerClient.add(descriptionProjet, 0, 5);
		gridPaneCreerClient.add(textFiedlDescriptionProjet, 1, 5, 3, 1);
		gridPaneCreerClient.add(ajoutContact, 0, 6);
		gridPaneCreerClient.add(btns, 0, 7, 4, 1);
		gridPaneCreerClient.add(warning, 0, 8, 4, 1);

		Scene sceneCreerClient = new Scene(gridPaneCreerClient);
		sceneCreerClient.getStylesheets().add("CSS/" + InterfaceProjet.styleCss + ".css");

		stageCreerClient.setScene(sceneCreerClient);
		stageCreerClient.setTitle("Cr�ation du demandeur du projet");
		stageCreerClient.show();
	}

	private static void fermeturePageSave(Projet projet, Label info, String nomProjet, Stage primaryStage,
			Stage stageCreerProjet, boolean ouvrePro) {
		InterfaceProjet interfaceProjet = new InterfaceProjet(primaryStage);
		if (InterfaceProjet.modifier) {
			Alert fermeture = new Alert(AlertType.CONFIRMATION);

			fermeture.setHeaderText(null);
			fermeture.setTitle("Femeture projet");
			fermeture.setContentText("Voulez-vous sauvegarder ?");

			ButtonType sauvegarder = new ButtonType("sauvegarder");
			ButtonType quitter = new ButtonType("quitter");
			ButtonType annuler = new ButtonType("annuler");
			fermeture.getButtonTypes().setAll(sauvegarder, quitter, annuler);

			Optional<ButtonType> choix = fermeture.showAndWait();

			if (choix.get() == sauvegarder) {
				try {
					projet.sauvegarder(info, primaryStage);
				} catch (IOException e) {
					e.printStackTrace();
				}

				Projet projetNow = new Projet(nomProjet, clientEntreprise);
				clientEntreprise = null;
				if (ouvrePro) {
					projetNow.sauvegardeSaveProjet();
					projetNow.chargement();
				} else {
					try {
						projetNow.nouveau(primaryStage, info);
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
				stageCreerProjet.close();
				interfaceProjet.pageProjet(projetNow, InterfaceProjet.pageCentreNum�ro, "");
			}

			if (choix.get() == quitter) {
				Projet projetNow = new Projet(nomProjet, clientEntreprise);
				if (ouvrePro) {
					projetNow.sauvegardeSaveProjet();
					projetNow.chargement();
				} else {
					try {
						projetNow.nouveau(primaryStage, info);
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
				stageCreerProjet.close();
				interfaceProjet.pageProjet(projetNow, InterfaceProjet.pageCentreNum�ro, "");
			}

			if (choix.get() == annuler) {
				fermeture.close();
			}
		} else {
			Projet projetNow = new Projet(nomProjet, clientEntreprise);
			if (ouvrePro) {
				projetNow.sauvegardeSaveProjet();
				projetNow.chargement();
			} else {
				try {
					projetNow.nouveau(primaryStage, info);
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			stageCreerProjet.close();
			interfaceProjet.pageProjet(projetNow, InterfaceProjet.pageCentreNum�ro, "");
		}
	}

	public static void ouvrirProjet(Projet projet, final Stage primaryStage, boolean supprimer, Label info) {
		File repertoire = new File(Constante.CHEMIN_DOSSIER_SAVE);
		String liste[] = repertoire.list();

		if (liste.length > 1) {
			Stage stageOuvrirProjet = new Stage();
			stageOuvrirProjet.initModality(Modality.APPLICATION_MODAL);
			stageOuvrirProjet.setResizable(false);
			GridPane gridPaneOuvrirProjet = new GridPane();
			gridPaneOuvrirProjet.setHgap(5);
			gridPaneOuvrirProjet.setVgap(5);

			/* CREATION DES LABELS, DES BOUTONS ET DES TEXTFIELD */
			Label ouvrirProjet = new Label("Ouvrir projet : ");

			ComboBox<String> comboBoxFile = new ComboBox<>();
			comboBoxFile.setMaxWidth(150);
			comboBoxFile.setMinWidth(150);

			for (int i = 0; i < liste.length; i++) {
				if (!(liste[i].equals(projet.getNom()))) {
					comboBoxFile.getItems().add(liste[i]);
				}

			}
			comboBoxFile.setValue(comboBoxFile.getItems().get(0));

			Button buttonValider = new Button(Constante.VALIDER);
			Button buttonAnnuler = new Button(Constante.ANNULER);

			InterfaceProjet interfaceProjet = new InterfaceProjet(primaryStage);

			/* ACTION */
			buttonValider.setOnMouseClicked(event -> {

				fermeturePageSave(projet, info, comboBoxFile.getValue(), primaryStage, stageOuvrirProjet, true);
				if (supprimer) {
					File repertoireASup = new File(Constante.CHEMIN_DOSSIER_SAVE + projet.getNom());
					emptyDirectory(repertoireASup);
					InterfaceProjet.historique("Projet supprim� du nom de " + projet.getNom());

					if (!(repertoireASup.delete())) {
						System.err.println("Mauvaise supression d'un fichier ");
					}

				}
			});

			buttonAnnuler.setOnMouseClicked(event -> {
				if (supprimer) {
					info.setText("Vous avez annuler la suppression d'un projet");
				} else {
					info.setText("Vous n'avez pas ouvert un nouveau projet");
				}
				stageOuvrirProjet.close();
				interfaceProjet.pageProjet(projet, InterfaceProjet.pageCentreNum�ro, "");
			});

			gridPaneOuvrirProjet.add(ouvrirProjet, 0, 1);
			gridPaneOuvrirProjet.add(comboBoxFile, 1, 1);
			gridPaneOuvrirProjet.add(buttonValider, 0, 2);
			gridPaneOuvrirProjet.add(buttonAnnuler, 1, 2);
			GridPane.setMargin(gridPaneOuvrirProjet, new Insets(10, 10, 10, 10));

			Scene sceneOuvrirProjet = new Scene(gridPaneOuvrirProjet);

			stageOuvrirProjet.setTitle("Ouvrir Projet");
			stageOuvrirProjet.setScene(sceneOuvrirProjet);
			stageOuvrirProjet.show();

		} else {
			Alert ouvertureImpossible = new Alert(AlertType.WARNING);
			ouvertureImpossible.setTitle("Ouverture Impossible !");
			ouvertureImpossible.setHeaderText(null);
			ouvertureImpossible.setContentText("Ouverture impossible d'un nouveau fichier !");
			ouvertureImpossible.show();
		}
	}

	public static void emptyDirectory(File folder) {
		for (File file : folder.listFiles()) {
			if (file.isDirectory()) {
				emptyDirectory(file);
			}
			file.delete();
		}
	}

	public static void addRessource(Projet projet, Stage primaryStage, Label info, TreeSet<PointFort> pointForts) {
		Stage stageAddRessource = new Stage();
		stageAddRessource.initModality(Modality.APPLICATION_MODAL);
		stageAddRessource.setResizable(false);
		GridPane gridPaneAddRessourceCaracteristique = new GridPane();

		/* CREATION DES LABELS, DES BOUTONS ET DES TEXTFIELD */
		Label titre = new Label("RESSOURCE");
		titre.setId("titrePartie");
		titre.setMaxWidth(Double.MAX_VALUE);

		Label nom = new Label("Nom : ");
		Label prenom = new Label("Prenom : ");
		Label surnom = new Label("Surnom : ");
		Label email = new Label("Email :");
		Label telephone = new Label("Telephone : ");
		Label imageLabel = new Label("Image : ");
		Label chemin = new Label("");
		chemin.setId("cheminPhoto");
		chemin.setMaxWidth(Double.MAX_VALUE);

		Label warningLabel = new Label("");
		warningLabel.setId("warning");

		TextField textFieldNom = new TextField();
		textFieldNom.setPromptText("Nom");
		TextField textFieldPrenom = new TextField();
		textFieldPrenom.setPromptText("Prenom");
		TextField textFielSurnom = new TextField();
		textFielSurnom.setPromptText("Surnom");
		TextField textFieldEmail = new TextField();
		textFieldEmail.setPromptText("azerty@ghjk.mlp");
		TextField textFieldTelphone = new TextField();
		textFieldTelphone.setPromptText("06 06 06 06 06");

		Button buttonCaracteristique = new Button("Caract�ristiques");
		buttonCaracteristique.setId("selectedBtn");
		Button buttonPointFort = new Button("Point Fort");
		buttonPointFort.setId("btnMenu");

		Button buttonValider = new Button(Constante.VALIDER);
		buttonValider.setId("Valider");
		Button buttonAnnuler = new Button(Constante.ANNULER);
		buttonAnnuler.setId("Annuler");
		Button buttonNavigation = new Button("Parcourir");
		buttonNavigation.setId("boutonNavigation");

		HBox hBoxBouton = new HBox(buttonValider, buttonAnnuler);
		hBoxBouton.setId("barBtn");
		hBoxBouton.setSpacing(150);

		HBox hBoxOngletRessource = new HBox(buttonCaracteristique, buttonPointFort);
		hBoxOngletRessource.setId("btnMenuBar");
		VBox enTeteRessource = new VBox(titre, hBoxOngletRessource);
		enTeteRessource.setSpacing(10);
		enTeteRessource.setMaxWidth(Double.MAX_VALUE);
		VBox ressourceVbox = new VBox(enTeteRessource, gridPaneAddRessourceCaracteristique, hBoxBouton, warningLabel);

		TextField salaire = new TextField("0");
		salaire.setPadding(new Insets(0, 0, 0, 0));
		salaire.setMaxWidth(30);
		HBox hBoxSalaire = new HBox(new Label("Salaire horraire"), salaire, new Label("�/h"));
		hBoxSalaire.setSpacing(5);

		InterfaceProjet interfaceProjet = new InterfaceProjet(primaryStage);

		buttonCaracteristique.setOnMouseClicked(event -> {
			ressourceVbox.getChildren().remove(1);
			buttonCaracteristique.setId("selectedBtn");
			buttonPointFort.setId("btnMenu");
			ressourceVbox.getChildren().add(1, gridPaneAddRessourceCaracteristique);
		});

		buttonPointFort.setOnMouseClicked(event -> {
			ressourceVbox.getChildren().remove(1);
			buttonPointFort.setId("selectedBtn");
			buttonCaracteristique.setId("btnMenu");
			ressourceVbox.getChildren().add(1, addPointFort(projet, pointForts, warningLabel));
		});

		buttonNavigation.setOnMouseClicked(event -> {
			FileChooser choixPhoto = new FileChooser();
			choixPhoto.getExtensionFilters().addAll(new ExtensionFilter("*.png", "*.jpg", "*.gif"));
			File fileImage = choixPhoto.showOpenDialog(stageAddRessource);
			if (fileImage != null) {
				chemin.setText(fileImage.toString());
			}
		});

		buttonValider.setOnMouseClicked(event -> {

			if (salaire.getText().matches("^[0-9]*") || salaire.getText().matches("^[0-9]*.[0-9][0-9]")) {
				if (textFieldVide(salaire)) {
					salaire.setText("null");
				}

				Ressource ressource = new Ressource(textFieldNom.getText().toUpperCase(), textFieldPrenom.getText(),
						textFielSurnom.getText(), textFieldEmail.getText().trim(), textFieldTelphone.getText().trim(), false,
						chemin.getText(), Double.parseDouble(salaire.getText()), pointForts);

				if (textFieldVide(textFielSurnom)) {
					textFielSurnom.setText(null);
				}

				if (ressource.verif(warningLabel, projet, textFieldNom, textFieldPrenom, textFieldEmail,
						textFieldTelphone, false)) {
					try {
						if (!(chemin.getText().equals(""))) {
							Path fileSrc = Paths.get(chemin.getText());
							Path fileDest = Paths.get(Constante.CHEMIN_DOSSIER_SAVE + projet.getNom() + "/images/"
									+ textFieldNom.getText() + textFieldPrenom.getText()
									+ chemin.getText().substring(chemin.getText().lastIndexOf('.')));
							try {
								Files.copy(fileSrc, fileDest);
							} catch (IOException e) {
								e.printStackTrace();
							}

							chemin.setText(fileDest.toUri().toURL().toString());

						} else {
							chemin.setText("nothing");
						}

						textFieldPrenom.setText(textFieldPrenom.getText().toLowerCase());
						char maj = Character.toUpperCase(textFieldPrenom.getText().charAt(0));
						textFieldPrenom
								.setText(textFieldPrenom.getText().replaceFirst(String.valueOf(textFieldPrenom.getText().charAt(0)),
										String.valueOf(maj)));

						if (!(textFieldVide(textFieldTelphone))) {
							textFieldTelphone.setText(textFieldTelphone.getText().replace(' ', '_').trim());
						}
						Ressource bonneRessource = new Ressource(textFieldNom.getText().toUpperCase().trim(),
								textFieldPrenom.getText().trim(), textFielSurnom.getText(), textFieldEmail.getText(),
								textFieldTelphone.getText(), false, chemin.getText(),
								Double.parseDouble(salaire.getText()), pointForts);
						projet.setRessource(bonneRessource);
						info.setText("Ajout d'une nouvelle Ressource � votre projet");
						InterfaceProjet.modifier = true;
						InterfaceProjet.historique("Une ressource a �t� ajout� au projet " + projet.getNom() + " nomm� "
								+ bonneRessource.getNom() + " " + bonneRessource.getPrenom());
						stageAddRessource.close();
						interfaceProjet.pageProjet(projet, InterfaceProjet.pageCentreNum�ro, "");
					} catch (MalformedURLException e) {
						e.printStackTrace();
					}
				}
			} else {
				warningLabel.setText("Le salaire doit etre valide");
			}

		});

		buttonAnnuler.setOnMouseClicked(event -> {
			info.setText("Vous avez annul� un nouvel ajout d'une ressource");
			stageAddRessource.close();
			interfaceProjet.pageProjet(projet, InterfaceProjet.pageCentreNum�ro, "");
		});

		/* CREATION GRID PANE */
		gridPaneAddRessourceCaracteristique.add(nom, 0, 0);
		gridPaneAddRessourceCaracteristique.add(textFieldNom, 1, 0);
		gridPaneAddRessourceCaracteristique.add(prenom, 2, 0);
		gridPaneAddRessourceCaracteristique.add(textFieldPrenom, 3, 0);
		gridPaneAddRessourceCaracteristique.add(email, 0, 2);
		gridPaneAddRessourceCaracteristique.add(surnom, 0, 1);
		gridPaneAddRessourceCaracteristique.add(textFielSurnom, 1, 1);
		gridPaneAddRessourceCaracteristique.add(textFieldEmail, 1, 2);
		gridPaneAddRessourceCaracteristique.add(telephone, 2, 2);
		gridPaneAddRessourceCaracteristique.add(textFieldTelphone, 3, 2);
		gridPaneAddRessourceCaracteristique.add(hBoxSalaire, 0, 3, 4, 1);
		gridPaneAddRessourceCaracteristique.add(imageLabel, 0, 4);
		gridPaneAddRessourceCaracteristique.add(chemin, 1, 4, 2, 1);
		gridPaneAddRessourceCaracteristique.add(buttonNavigation, 3, 4);
		gridPaneAddRessourceCaracteristique.setId("GridPaneEcart");

		/* Scene */
		Scene sceneAjouterRessource = new Scene(ressourceVbox);
		sceneAjouterRessource.getStylesheets().add("CSS/" + InterfaceProjet.styleCss + ".css");

		stageAddRessource.setTitle("Ajouter Ressource");
		stageAddRessource.setScene(sceneAjouterRessource);
		stageAddRessource.show();
	}

	private static HBox addPointFort(Projet projet, TreeSet<PointFort> pointForts, Label warning) {
		HBox hBox = new HBox();
		hBox.setAlignment(Pos.CENTER);
		hBox.setSpacing(20);

		VBox vBoxPourRessource = new VBox();
		vBoxPourRessource.setMinWidth(200);

		ScrollPane scrollPanePourRessource = new ScrollPane(vBoxPourRessource);
		scrollPanePourRessource.setMinSize(vBoxPourRessource.getMinWidth(), 170);
		scrollPanePourRessource.setMaxHeight(300);

		VBox vBoxPasPourRessource = new VBox();
		vBoxPasPourRessource.setMinWidth(200);

		ScrollPane scrollPanePasPourRessource = new ScrollPane(vBoxPasPourRessource);
		scrollPanePasPourRessource.setMinSize(vBoxPasPourRessource.getMinWidth(), 170);
		scrollPanePasPourRessource.setMaxHeight(300);

		Label vide = new Label("Vide");
		vide.setId("vide");

		Iterator<PointFort> iterateurPointRessource = pointForts.iterator();
		while (iterateurPointRessource.hasNext()) {
			PointFort pointFort = iterateurPointRessource.next();
			Button ressource = new Button(pointFort.getNom());
			ressource.setMinWidth(202);
			ressource.setMaxWidth(202);
			ressource.setId("labelRessourceObjectif");
			vBoxPourRessource.getChildren().add(ressource);

			ressource.setOnMouseClicked(event -> {

				vBoxPourRessource.getChildren().remove(ressource);
				Button buttonPasRessourcePntFort = new Button(ressource.getText());
				pointForts.remove(pointFort);
				buttonPasRessourcePntFort.setMinWidth(202);
				buttonPasRessourcePntFort.setMaxWidth(202);
				buttonPasRessourcePntFort.setId("labelRessourceNonObjectif");

				vBoxPasPourRessource.getChildren().add(vBoxPasPourRessource.getChildren().size() - 1,
						buttonPasRessourcePntFort);

				if (vBoxPourRessource.getChildren().isEmpty()) {
					vBoxPourRessource.getChildren().add(vide);
				}

				buttonPasRessourcePntFort.setOnMouseClicked(eventAnnuler -> {
					vBoxPasPourRessource.getChildren().remove(buttonPasRessourcePntFort);
					pointForts.add(pointFort);
					vBoxPourRessource.getChildren().add(ressource);

					if (vBoxPourRessource.getChildren().contains(vide)) {
						vBoxPourRessource.getChildren().remove(vide);
					}
				});
			});
		}

		Iterator<PointFort> iterateurPointPasRessource = projet.getPointForts().iterator();
		while (iterateurPointPasRessource.hasNext()) {
			PointFort pointFort = iterateurPointPasRessource.next();
			if (!pointForts.contains(pointFort)) {
				Button pasRessource = new Button(pointFort.getNom());
				pasRessource.setMinWidth(202);
				pasRessource.setMaxWidth(202);
				pasRessource.setId("labelRessourceNonObjectif");
				vBoxPasPourRessource.getChildren().add(pasRessource);

				pasRessource.setOnMouseClicked(event -> {
					if (vBoxPourRessource.getChildren().contains(vide)) {
						vBoxPourRessource.getChildren().remove(vide);
					}

					vBoxPasPourRessource.getChildren().remove(pasRessource);
					Button buttonRessourcePntFort = new Button(pasRessource.getText());
					pointForts.add(pointFort);
					buttonRessourcePntFort.setMinWidth(202);
					buttonRessourcePntFort.setMaxWidth(202);
					buttonRessourcePntFort.setId("labelRessourceObjectif");

					vBoxPourRessource.getChildren().add(buttonRessourcePntFort);

					buttonRessourcePntFort.setOnMouseClicked(eventAnnuler -> {
						vBoxPourRessource.getChildren().remove(buttonRessourcePntFort);
						pointForts.remove(pointFort);
						vBoxPasPourRessource.getChildren().add(vBoxPasPourRessource.getChildren().size() - 1,
								pasRessource);

						if (vBoxPourRessource.getChildren().isEmpty()) {
							vBoxPourRessource.getChildren().add(vide);
						}
					});
				});
			}
		}

		if (vBoxPourRessource.getChildren().isEmpty()) {
			vBoxPourRessource.getChildren().add(vide);
		}

		HBox creerPointFort = new HBox();
		TextField nomPointFort = new TextField();
		Button ajouter = new Button("ajouter");
		ajouter.setId("Valider");

		creerPointFort.getChildren().addAll(nomPointFort, ajouter);
		vBoxPasPourRessource.getChildren().add(creerPointFort);

		ajouter.setOnMouseClicked(event -> {
			if (textFieldVide(nomPointFort) || nomPointFort.getText().contains("FIN_POINT_FORT") || nomPointFort.getText().length()> 25) {
				warning.setText("Il faut un nom valide de moins de 25 caract�res � votre point fort");
			} else {
				PointFort newPointFort = new PointFort(nomPointFort.getText().trim());
				if (newPointFort.existe(projet)) {
					warning.setText("Point fort d�j� existant");
				} else {
					InterfaceProjet.modifier = true;
					projet.getPointForts().add(newPointFort);
					InterfaceProjet.historique(
							"nouveau point fort ajout� au projet " + projet.getNom() + " : " + newPointFort.getNom());
					Button buttonNewPointFort = new Button(newPointFort.getNom());
					buttonNewPointFort.setMinWidth(202);
					buttonNewPointFort.setMaxWidth(202);
					buttonNewPointFort.setId("labelRessourceNonObjectif");

					buttonNewPointFort.setOnMouseClicked(eventNew -> {
						vBoxPasPourRessource.getChildren().remove(buttonNewPointFort);
						pointForts.add(newPointFort);

						Button buttonRessourcePntFort = new Button(buttonNewPointFort.getText());
						buttonRessourcePntFort.setMinWidth(202);
						buttonRessourcePntFort.setMaxWidth(202);
						buttonRessourcePntFort.setId("labelRessourceObjectif");

						vBoxPourRessource.getChildren().add(buttonRessourcePntFort);

						if (vBoxPourRessource.getChildren().contains(vide)) {
							vBoxPourRessource.getChildren().remove(vide);
						}
						buttonRessourcePntFort.setOnMouseClicked(eventAnnuler -> {
							vBoxPourRessource.getChildren().remove(buttonRessourcePntFort);
							pointForts.remove(newPointFort);
							vBoxPasPourRessource.getChildren().add(vBoxPasPourRessource.getChildren().size() - 1,
									buttonNewPointFort);
							if (vBoxPourRessource.getChildren().isEmpty()) {
								vBoxPourRessource.getChildren().add(vide);
							}
						});
					});

					vBoxPasPourRessource.getChildren().add(vBoxPasPourRessource.getChildren().size() - 1,
							buttonNewPointFort);
					nomPointFort.setText("");
				}
			}

		});

		hBox.getChildren().addAll(scrollPanePourRessource, scrollPanePasPourRessource);

		return hBox;
	}

	@SuppressWarnings("deprecation")
	public static void ajoutObjectif(Stage primaryStage, Projet projet, Label info, HashSet<Ressource> ressources,
			Map<Integer, Tache> taches) {
		InterfaceProjet interfaceProjet = new InterfaceProjet(primaryStage);
		Stage stageAjouterObjectifs = new Stage();
		stageAjouterObjectifs.initModality(Modality.APPLICATION_MODAL);
		stageAjouterObjectifs.setResizable(false);

		Label titreObjectif = new Label("OBJECTIF");
		titreObjectif.setMaxWidth(Double.MAX_VALUE);
		titreObjectif.setId("titrePartie");

		Button btnFonctionnalite = new Button("G�n�ral");
		btnFonctionnalite.setId("selectedBtn");
		Button btnAjoutRessource = new Button("Ajout de Ressource");
		btnAjoutRessource.setId("btnMenu");
		Button btnAjoutTache = new Button("Ajout de T�che");
		btnAjoutTache.setId("btnMenu");

		HBox btns = new HBox(btnFonctionnalite, btnAjoutRessource, btnAjoutTache);
		btns.setId("btnMenuBar");

		GridPane gridPaneGoalAdd = new GridPane();
		gridPaneGoalAdd.setAlignment(Pos.CENTER);
		gridPaneGoalAdd.setPadding(new Insets(10, 10, 10, 10));
		gridPaneGoalAdd.setVgap(10);

		HBox hBoxAddRessourceGoal = ressourceRelieAvecGoal(projet, ressources);
		VBox vBoxAddTache = ajoutTache(taches);

		VBox vBoxObjectifs = new VBox(titreObjectif, btns, gridPaneGoalAdd);

		btnFonctionnalite.setOnMouseClicked(event -> {
			vBoxObjectifs.getChildren().remove(2);
			btnAjoutRessource.setId("btnMenu");
			btnAjoutTache.setId("btnMenu");
			btnFonctionnalite.setId("selectedBtn");
			vBoxObjectifs.getChildren().add(2, gridPaneGoalAdd);
		});

		btnAjoutRessource.setOnMouseClicked(event -> {
			vBoxObjectifs.getChildren().remove(2);
			btnAjoutRessource.setId("selectedBtn");
			btnFonctionnalite.setId("btnMenu");
			btnAjoutTache.setId("btnMenu");
			vBoxObjectifs.getChildren().add(2, hBoxAddRessourceGoal);
		});

		btnAjoutTache.setOnMouseClicked(event -> {
			vBoxObjectifs.getChildren().remove(2);
			btnAjoutTache.setId("selectedBtn");
			btnFonctionnalite.setId("btnMenu");
			btnAjoutRessource.setId("btnMenu");
			vBoxObjectifs.getChildren().add(2, vBoxAddTache);
		});

		/* LABEL, TEXTFIELD ET BUTTON */
		Label nom = new Label("Nom *: ");
		Label description = new Label("Description : ");
		Label dateDebut = new Label("Date de d�but : ");
		Label dateFin = new Label("Date de fin : ");
		Label labelTempsDate = new Label("Temps date : ");
		Label volumePriorite = new Label("Priorit� : ");
		Label categorie = new Label("Categorie : ");
		Label warning = new Label("");
		warning.setId("warning");

		TextField textFieldNom = new TextField();
		TextArea textAreaDescription = new TextArea();

		DatePicker datePikerDateDebut = new DatePicker();
		datePikerDateDebut.setValue(LocalDate.now());
		DatePicker datePikerDateFin = new DatePicker();
		datePikerDateFin.setValue(datePikerDateDebut.getValue().plusDays(1));
		datePikerDateFin.setDisable(true);
		dateFin.setDisable(true);

		TextField tempsDate = new TextField();
		tempsDate.setMaxWidth(50);
		tempsDate.setText("1");
		Label jourLabel = new Label("jour");

		HBox dates = new HBox(dateDebut, datePikerDateDebut, dateFin, datePikerDateFin);
		dates.setSpacing(15);

		HBox hBoxTemps = new HBox(labelTempsDate, tempsDate, jourLabel);
		hBoxTemps.setSpacing(10);

		RadioButton radioDate = new RadioButton("Date-Dur�e/Date");

		radioDate.setOnAction(event -> {
			if (radioDate.isSelected()) {
				tempsDate.setDisable(true);
				labelTempsDate.setDisable(true);
				jourLabel.setDisable(true);
				tempsDate.setText("1");
				datePikerDateFin.setDisable(false);
				dateFin.setDisable(false);
			} else {
				tempsDate.setDisable(false);
				labelTempsDate.setDisable(false);
				jourLabel.setDisable(false);
				datePikerDateFin.setDisable(true);
				dateFin.setDisable(true);
				datePikerDateFin.setValue(LocalDate.now().plusDays(1));
			}
		});

		ComboBox<VolumePriorite> comboBoxVolumePriorite = new ComboBox<>();
		comboBoxVolumePriorite.getItems().addAll(VolumePriorite.FAIBLE, VolumePriorite.NORMAL, VolumePriorite.FORTE);
		comboBoxVolumePriorite.setValue(comboBoxVolumePriorite.getItems().get(1));

		ComboBox<Categorie> comboBoxCategorie = new ComboBox<>();
		comboBoxCategorie.getItems().addAll(projet.getCategories());
		comboBoxCategorie.getItems().remove(projet.getCategorieVide());
		comboBoxCategorie.setMinWidth(100);
		comboBoxCategorie.setMaxWidth(comboBoxCategorie.getMinWidth());

		HBox hBoxLigne4 = new HBox(volumePriorite, comboBoxVolumePriorite, categorie, comboBoxCategorie);
		hBoxLigne4.setSpacing(60);

		Button btnValider = new Button(Constante.VALIDER);
		btnValider.setId("Valider");

		Button btnAnnuler = new Button(Constante.ANNULER);
		btnAnnuler.setId("Annuler");

		HBox btn = new HBox(btnValider, btnAnnuler);
		btn.setSpacing(150);
		btn.setId("barBtn");

		/* VERIF NOM BON */
		btnValider.setOnMouseClicked(event -> {
			if (comboBoxCategorie.getValue() == null) {
				comboBoxCategorie.setValue(projet.getCategorieVide());
			}

			if (taches.isEmpty()) {
				taches.put(1, new Tache("D�but"));
				taches.put(2, new Tache("Fin"));
			}

			textFieldNom.setText(textFieldNom.getText().trim());

			Goal objectif = new Goal(textFieldNom.getText().trim(), comboBoxVolumePriorite.getValue(),
					textAreaDescription.getText().trim(), projet,
					new Date(datePikerDateDebut.getValue().getYear() - 1900,
							datePikerDateDebut.getValue().getMonthValue() - 1,
							datePikerDateDebut.getValue().getDayOfMonth()),
					new Date(datePikerDateFin.getValue().getYear() - 1900,
							datePikerDateFin.getValue().getMonthValue() - 1,
							datePikerDateFin.getValue().getDayOfMonth()),
					ressources, comboBoxCategorie.getValue(), taches);
			comboBoxCategorie.getValue().getObjectifs().add(objectif);

			if (!(radioDate.isSelected())) {
				if (tempsDate.getText().matches("^[0-9]*")) {
					int nbrJour = Integer.parseInt(tempsDate.getText());
					LocalDate dateFinBonne = datePikerDateDebut.getValue().plusDays(nbrJour);
					objectif.setDateFin(new Date(dateFinBonne.getYear() - 1900, dateFinBonne.getMonthValue() - 1,
							dateFinBonne.getDayOfMonth()));
				} else {
					warning.setText("Erreur dans la dur�e");
				}
			}

			if (objectif.verif(warning, textFieldNom, textAreaDescription, projet, null)) {
				if (objectif.dateBonne(warning, datePikerDateFin)) {
					projet.setObjectif(objectif);
					info.setText("Un nouvel objectif a �t� ajout�");
					InterfaceProjet.historique("un nouvel objectif a �t� ajout� au projet " + projet.getNom()
							+ " nomm� " + objectif.getNom());
					InterfaceProjet.modifier = true;
					stageAjouterObjectifs.close();
					interfaceProjet.pageProjet(projet, InterfaceProjet.pageCentreNum�ro, "");
				} else {
					comboBoxCategorie.getValue().getObjectifs().remove(objectif);
				}
			} else {
				comboBoxCategorie.getValue().getObjectifs().remove(objectif);
			}
		});

		btnAnnuler.setOnMouseClicked(event -> {
			info.setText("Vous avez annul� un nouvel ajout d'Objectif");
			stageAjouterObjectifs.close();
			interfaceProjet.pageProjet(projet, InterfaceProjet.pageCentreNum�ro, "");
		});

		gridPaneGoalAdd.add(nom, 0, 0);
		gridPaneGoalAdd.add(textFieldNom, 1, 0);
		gridPaneGoalAdd.add(description, 0, 1);
		gridPaneGoalAdd.add(textAreaDescription, 1, 1);
		gridPaneGoalAdd.add(radioDate, 0, 2, 2, 1);
		gridPaneGoalAdd.add(dates, 0, 3, 2, 1);
		gridPaneGoalAdd.add(hBoxTemps, 0, 4, 2, 1);
		gridPaneGoalAdd.add(hBoxLigne4, 0, 5, 2, 1);
		gridPaneGoalAdd.add(warning, 1, 7);

		vBoxObjectifs.getChildren().add(btn);

		/* Scene */
		Scene sceneAjouterObjectif = new Scene(vBoxObjectifs);
		sceneAjouterObjectif.getStylesheets().add("CSS/" + InterfaceProjet.styleCss + ".css");

		stageAjouterObjectifs.setTitle("Ajouter Objectif");
		stageAjouterObjectifs.setScene(sceneAjouterObjectif);
		stageAjouterObjectifs.show();
	}

	private static VBox ajoutTache(Map<Integer, Tache> taches) {
		VBox vbox = new VBox();
		VBox vBoxContenu = new VBox();

		ScrollPane scrollPaneTache = new ScrollPane(vBoxContenu);

		Label titre = new Label("AJO�T DE T�CHE");
		titre.setId("titrePartie");
		titre.setMaxWidth(Double.MAX_VALUE);

		Label nom = new Label("Nom : ");
		nom.setId("InterfaceAjoutTache");
		TextField textFieldNom = new TextField();
		Button validerTache = new Button("Valider");
		validerTache.setId("Valider");

		Label warning = new Label("");
		warning.setId("warning");

		validerTache.setOnMouseClicked(event -> {
			if (textFieldVide(textFieldNom) || textFieldNom.getText().length()>50) {
				warning.setText("Il faut un nom de moins de 50 caract�res � votre t�che");
			} else {
				Button tache = new Button(textFieldNom.getText());

				vBoxContenu.getChildren().add(vBoxContenu.getChildren().size() - 1, tache);
				taches.put(vBoxContenu.getChildren().size() - 1, new Tache(textFieldNom.getText().trim()));
				textFieldNom.setText("");

				if (vBoxContenu.getChildren().size() > 10) {
					vBoxContenu.setMinWidth(363);
				}

				tache.setId("SupprimerTache");
				tache.setMaxWidth(Double.MAX_VALUE);
				tache.setOnMouseClicked(annuler -> {
					vBoxContenu.getChildren().remove(tache);
					if (vBoxContenu.getChildren().size() <= 10) {
						vBoxContenu.setMinWidth(376);
					}
				});
			}
		});

		HBox tacheCreator = new HBox(nom, textFieldNom, validerTache);
		tacheCreator.setSpacing(20);
		tacheCreator.setId("InterfaceAjoutTache");
		tacheCreator.setAlignment(Pos.CENTER);

		Iterator<Entry<Integer, Tache>> iterateurTache = taches.entrySet().iterator();
		while (iterateurTache.hasNext()) {
			Map.Entry<Integer, Tache> entry = iterateurTache.next();
			Button buttonTache = new Button(entry.getValue().getNom());
			buttonTache.setId("SupprimerTache");
			buttonTache.setMaxWidth(Double.MAX_VALUE);

			vBoxContenu.getChildren().add(buttonTache);

			buttonTache.setOnMouseClicked(annuler -> {
				vBoxContenu.getChildren().remove(buttonTache);
				taches.remove(entry.getKey());
				if (vBoxContenu.getChildren().size() <= 10) {
					vBoxContenu.setMinWidth(376);
				}
			});
		}

		vBoxContenu.getChildren().add(tacheCreator);
		vBoxContenu.setSpacing(10);
		vBoxContenu.setId("InterfaceAjoutTache");
		vBoxContenu.setPadding(new Insets(10, 10, 10, 10));
		vBoxContenu.setMinWidth(376);

		vbox.getChildren().addAll(titre, scrollPaneTache, warning);
		vbox.setAlignment(Pos.CENTER);
		vbox.setMaxWidth(400);
		VBox.setMargin(vbox, new Insets(0, 100, 0, 100));
		return vbox;
	}

	private static HBox ressourceRelieAvecGoal(Projet projet, Set<Ressource> ressources) {
		HBox hBox = new HBox();
		hBox.setMaxWidth(Double.MAX_VALUE);
		hBox.setPadding(new Insets(10, 10, 10, 10));
		hBox.setAlignment(Pos.CENTER);
		hBox.setSpacing(50);

		/* PREMIERE VBOX : AFFICHAGE */
		VBox vBoxGauche = new VBox();
		vBoxGauche.setBackground(
				new Background(new BackgroundFill(Color.web("#FFEEA4"), CornerRadii.EMPTY, Insets.EMPTY)));
		vBoxGauche.setPadding(new Insets(10, 0, 10, 0));
		vBoxGauche.setMinWidth(200);

		VBox vBoxDroite = new VBox();
		vBoxDroite.setBackground(
				new Background(new BackgroundFill(Color.web("#FFEEA4"), CornerRadii.EMPTY, Insets.EMPTY)));
		vBoxDroite.setPadding(new Insets(10, 0, 10, 0));
		vBoxDroite.setMinWidth(200);

		ScrollPane scrollPaneGauche = new ScrollPane();
		scrollPaneGauche.setMinSize(202, vBoxGauche.getHeight());
		scrollPaneGauche.setMaxHeight(350);

		ScrollPane scrollPaneDroit = new ScrollPane(vBoxDroite);
		scrollPaneDroit.setMinSize(202, vBoxDroite.getHeight());
		scrollPaneDroit.setMaxHeight(scrollPaneGauche.getMaxHeight());

		Iterator<Ressource> iterateurRessourceGauche = ressources.iterator();
		while (iterateurRessourceGauche.hasNext()) {
			Ressource ressource = iterateurRessourceGauche.next();

			Button nomRessources = new Button("");
			nomRessources.setId("labelRessourceObjectif");
			nomRessources.setAlignment(Pos.CENTER);
			nomRessources.setMaxWidth(Double.MAX_VALUE);
			nomRessources.setText(ressource.getNom() + " " + ressource.getPrenom());
			vBoxGauche.getChildren().add(nomRessources);

			nomRessources.setOnMouseClicked(event -> {
				ressources.remove(ressource);
				scrollPaneDroit.setContent(vBoxDroite);
				vBoxGauche.getChildren().remove(nomRessources);

				if (vBoxGauche.getChildren().isEmpty()) {
					Label vide = new Label("VIDE");
					vide.setId("vide");
					scrollPaneGauche.setContent(vide);
				}

				Button labelRessource = new Button(ressource.getNom() + " " + ressource.getPrenom());
				labelRessource.setId("labelRessourceNonObjectif");

				labelRessource.setOnMouseClicked(annuler -> {
					vBoxDroite.getChildren().remove(labelRessource);
					ressources.add(ressource);

					nomRessources.setMaxWidth(Double.MAX_VALUE);
					vBoxGauche.getChildren().add(nomRessources);
					scrollPaneGauche.setContent(vBoxGauche);

					if (vBoxDroite.getChildren().isEmpty()) {
						Label vide = new Label("VIDE");
						vide.setId("vide");
						scrollPaneDroit.setContent(vide);
					}

					nomRessources.setText(ressource.getNom() + " " + ressource.getPrenom());
					nomRessources.setId("labelRessourceNonObjectif");
				});

				labelRessource.setMaxWidth(Double.MAX_VALUE);
				vBoxDroite.getChildren().add(labelRessource);
			});
		}

		if (vBoxGauche.getChildren().isEmpty()) {
			Label vide = new Label("VIDE");
			vide.setId("vide");
			scrollPaneGauche.setContent(vide);
		} else {
			scrollPaneGauche.setContent(vBoxGauche);
		}

		/* DEUXIEME VBOX : TOUTES LES RESSOURCES */

		Iterator<Ressource> iterateurRessource = projet.getRessources().iterator();
		while (iterateurRessource.hasNext()) {
			Ressource ressource = iterateurRessource.next();

			if (!(ressources.contains(ressource))) {
				Button nomRessources = new Button("");
				nomRessources.setId("labelRessourceNonObjectif");
				nomRessources.setAlignment(Pos.CENTER);
				nomRessources.setMaxWidth(Double.MAX_VALUE);
				nomRessources.setText(ressource.getNom() + " " + ressource.getPrenom());
				vBoxDroite.getChildren().add(nomRessources);

				nomRessources.setOnMouseClicked(event -> {
					ressources.add(ressource);
					scrollPaneGauche.setContent(vBoxGauche);
					vBoxDroite.getChildren().remove(nomRessources);

					if (vBoxDroite.getChildren().isEmpty()) {
						Label vide = new Label("VIDE");
						vide.setId("vide");
						scrollPaneDroit.setContent(vide);
					}

					Button labelRessource = new Button(ressource.getNom() + " " + ressource.getPrenom());
					labelRessource.setId("labelRessourceObjectif");

					labelRessource.setOnMouseClicked(annuler -> {
						ressources.remove(ressource);
						vBoxGauche.getChildren().remove(labelRessource);

						nomRessources.setMaxWidth(Double.MAX_VALUE);
						vBoxDroite.getChildren().add(nomRessources);
						scrollPaneDroit.setContent(vBoxDroite);

						if (vBoxGauche.getChildren().isEmpty()) {
							Label vide = new Label("VIDE");
							vide.setId("vide");
							scrollPaneGauche.setContent(vide);
						}

						nomRessources.setText(ressource.getNom() + " " + ressource.getPrenom());
						nomRessources.setId("labelRessourceNonObjectif");
					});

					labelRessource.setMaxWidth(Double.MAX_VALUE);
					vBoxGauche.getChildren().add(labelRessource);
				});
			}
		}

		if (vBoxDroite.getChildren().isEmpty()) {
			Label vide = new Label("VIDE");
			vide.setId("vide");
			scrollPaneDroit.setContent(vide);
		} else {
			scrollPaneDroit.setContent(vBoxDroite);
		}

		hBox.getChildren().addAll(scrollPaneGauche, scrollPaneDroit);
		return hBox;
	}

	public static String toRGBCode(Color color) {
		return String.format("#%02X%02X%02X", (int) (color.getRed() * 255), (int) (color.getGreen() * 255),
				(int) (color.getBlue() * 255));
	}

	public static void ajouterCategorie(Projet projet, Stage primaryStage, Label info) {
		InterfaceProjet interfaceProjet = new InterfaceProjet(primaryStage);
		Stage stageAjouterCategorie = new Stage();
		stageAjouterCategorie.initModality(Modality.APPLICATION_MODAL);
		stageAjouterCategorie.setResizable(false);

		VBox vBoxAjouterCategorie = new VBox();
		
		Label titre = new Label("AJOUTER CATEGORIE");
		titre.setMaxWidth(Double.MAX_VALUE);
		titre.setId("titrePartie");
		
		Button infoGeneral = new Button("Information G�n�rale");
		infoGeneral.setId("selectedBtn");	
		Button objectif = new Button("Objectifs");
		objectif.setId("btnMenu");
		HBox menu = new HBox(infoGeneral,objectif);
		menu.setId("btnMenuBar");
		
		GridPane gridPaneAjouterCategorie = new GridPane();
		HBox hBoxObjectifs = hBoxOjectifsDansCategorie();
		
		infoGeneral.setOnMouseClicked(eventInfoGeneral ->{
			infoGeneral.setId("selectedBtn");
			objectif.setId("btnMenu");
			vBoxAjouterCategorie.getChildren().remove(2);
			vBoxAjouterCategorie.getChildren().add(2, gridPaneAjouterCategorie);
		});
		
		objectif.setOnMouseClicked(eventObjectif ->{
			infoGeneral.setId("btnMenu");
			objectif.setId("selectedBtn");
			vBoxAjouterCategorie.getChildren().remove(2);
			vBoxAjouterCategorie.getChildren().add(2, hBoxObjectifs);
		});

		/* LABELS, TEXTFIELD ET BOTTON */
		Label nom = new Label("Nom de la cat�gorie : ");
		Label couleurLabelFond = new Label("Couleur du fond : ");
		Label couleurLabelEcriture = new Label("Couleur de l'�criture : ");
		TextField textFieldNom = new TextField();
		ColorPicker colorFond = new ColorPicker();
		colorFond.setId("ColorPicker");
		ColorPicker colorEcriture = new ColorPicker();
		colorEcriture.setId("ColorPicker");

		Label warning = new Label("");
		warning.setId("warning");

		Button btnValider = new Button(Constante.VALIDER);
		btnValider.setId("Valider");
		Button btnAnnuler = new Button(Constante.ANNULER);
		btnAnnuler.setId("Annuler");
		
		HBox bouttons = new HBox(50, btnValider, btnAnnuler);
		bouttons.setMaxWidth(Double.MAX_VALUE);
		bouttons.setAlignment(Pos.CENTER);

		/* VERIF NOM BON */
		btnValider.setOnMouseClicked(eventValider -> {
			if (textFieldVide(textFieldNom)) {
				warning.setText("Il faut un nom");
			} else {
				Categorie categorie = new Categorie(textFieldNom.getText().trim(),
						colorFond.getValue().toString(),colorEcriture.getValue().toString());
				if (projet.existe(categorie)) {
					warning.setText("Ce nom de cat�gorie existe");
				} else {
					projet.setCategorie(categorie);
					info.setText("Vous avez ajout� une nouvelle cat�gorie");
					InterfaceProjet.historique("une nouvelle cat�gorie ajout� au projet " + projet.getNom()
							+ " du nom de " + categorie.getNom());
					InterfaceProjet.modifier = true;
					stageAjouterCategorie.close();
					interfaceProjet.pageProjet(projet, InterfaceProjet.pageCentreNum�ro, "");
				}

			}
		});

		btnAnnuler.setOnMouseClicked(eventAnnuler -> {
			info.setText("Vous avez annul� un nouvel ajout de cat�gorie");
			stageAjouterCategorie.close();
			interfaceProjet.pageProjet(projet, InterfaceProjet.pageCentreNum�ro, "");
		});

		/* REALISATION DU GRIDPANE */
		gridPaneAjouterCategorie.add(nom, 0, 1);
		gridPaneAjouterCategorie.add(textFieldNom, 1, 1);
		gridPaneAjouterCategorie.add(couleurLabelFond, 0, 2);
		gridPaneAjouterCategorie.add(colorFond, 1, 2);
		gridPaneAjouterCategorie.add(couleurLabelEcriture, 0, 3);
		gridPaneAjouterCategorie.add(colorEcriture, 1, 3);
		gridPaneAjouterCategorie.add(warning, 0, 5, 2, 1);
		
		vBoxAjouterCategorie.getChildren().addAll(titre, menu, gridPaneAjouterCategorie, bouttons);

		Scene sceneAjouterCategorie = new Scene(vBoxAjouterCategorie);
		sceneAjouterCategorie.getStylesheets().add("CSS/" + InterfaceProjet.styleCss + ".css");

		stageAjouterCategorie.setTitle("Validation d'un objectif");
		stageAjouterCategorie.setScene(sceneAjouterCategorie);
		stageAjouterCategorie.show();

	}

	private static HBox hBoxOjectifsDansCategorie() {
		HBox hBox = new HBox();
		return hBox;
	}

	public static boolean textFieldVide(TextField textField) {
		return textField == null || textField.getText() == null || "".equals(textField.getText());
	}

	public static void afficheRessource(ActionEvent event, Projet projet, Stage primaryStage) {
		InterfaceProjet interfaceProjet = new InterfaceProjet(primaryStage);
		if (((CheckMenuItem) event.getSource()).isSelected()) {
			InterfaceProjet.afficheRessourceBool = true;
		} else {
			InterfaceProjet.afficheRessourceBool = false;
		}
		projet.sauvegardeSaveProjet();
		interfaceProjet.pageProjet(projet, InterfaceProjet.pageCentreNum�ro, "");
	}

	public static void updateObjectif(Projet projet, Stage primaryStage, Goal objectifModifier) {
		VBox vBox = new VBox();

		Stage stage = new Stage();
		stage.initModality(Modality.APPLICATION_MODAL);
		stage.setResizable(false);

		Label titre = new Label("MODIFIER OBJETIF");
		titre.setId("titrePartie");
		titre.setMaxWidth(Double.MAX_VALUE);

		Label nomObjectif = new Label("Objectif : ");

		ComboBox<Goal> objectif = new ComboBox<>();
		objectif.getItems().addAll(projet.getObjectifs());
		objectif.getItems().removeIf(obj -> obj.getEtat().equals(EtatObjectif.FAIT));
		if (objectifModifier == null) {
			if (objectif.getItems().isEmpty()) {
				stage.close();
				InterfaceProjet interfaceProjet = new InterfaceProjet(primaryStage);
				interfaceProjet.pageProjet(projet, InterfaceProjet.pageCentreNum�ro, "");
			} else {
				objectif.setValue(objectif.getItems().get(0));

				HBox hBox = new HBox(nomObjectif, objectif);
				hBox.setSpacing(10);

				objectif.setOnAction(new EventHandler<ActionEvent>() {
					public void handle(ActionEvent event) {
						vBox.getChildren().set(2, modifierObjectif(projet, objectif.getValue(), stage, primaryStage));
					}
				});

				vBox.getChildren().addAll(titre, hBox,
						modifierObjectif(projet, objectif.getValue(), stage, primaryStage));

				Scene scene = new Scene(vBox);
				scene.getStylesheets().add("CSS/" + InterfaceProjet.styleCss + ".css");

				stage.setScene(scene);
				stage.setTitle("Modifier Objectif");
				stage.show();
			}
		} else {
			objectif.setValue(objectifModifier);

			HBox hBox = new HBox(nomObjectif, objectif);
			hBox.setSpacing(10);

			objectif.setOnAction(new EventHandler<ActionEvent>() {
				public void handle(ActionEvent event) {
					vBox.getChildren().set(2, modifierObjectif(projet, objectif.getValue(), stage, primaryStage));
				}
			});

			vBox.getChildren().addAll(titre, hBox, modifierObjectif(projet, objectif.getValue(), stage, primaryStage));

			Scene scene = new Scene(vBox);
			scene.getStylesheets().add("CSS/" + InterfaceProjet.styleCss + ".css");

			stage.setScene(scene);
			stage.setTitle("Modifier Objectif");
			stage.show();
		}

	}

	@SuppressWarnings("deprecation")
	private static VBox modifierObjectif(Projet projet, Goal objectif, Stage stage, Stage primaryStage) {
		InterfaceProjet interfaceProjet = new InterfaceProjet(primaryStage);
		
		Map<Integer, Tache> tachesUpdate = new HashMap<>();
		Iterator<Entry<Integer, Tache>> iterateurTacheUpdate = objectif.getTaches().entrySet().iterator();
		while(iterateurTacheUpdate.hasNext()) {
			Entry<Integer, Tache> tache = iterateurTacheUpdate.next();
			tachesUpdate.put(tache.getKey(), tache.getValue());
		}

		Button btnFonctionnalite = new Button("G�n�ral");
		btnFonctionnalite.setId("selectedBtn");
		Button btnAjoutRessource = new Button("Ajout de Ressource");
		btnAjoutRessource.setId("btnMenu");
		Button btnAjoutTache = new Button("Ajout de T�che");
		btnAjoutTache.setId("btnMenu");

		HBox btns = new HBox(btnFonctionnalite, btnAjoutRessource, btnAjoutTache);
		btns.setId("btnMenuBar");

		GridPane gridPaneGoalAdd = new GridPane();
		gridPaneGoalAdd.setAlignment(Pos.CENTER);
		gridPaneGoalAdd.setPadding(new Insets(10, 10, 10, 10));
		gridPaneGoalAdd.setVgap(10);

		HBox hBoxAddRessourceGoal = ressourceRelieAvecGoal(projet, objectif.getRessources());
		VBox vBoxAddTache = ajoutTache(tachesUpdate);

		VBox vBoxObjectifs = new VBox(btns, gridPaneGoalAdd);

		btnFonctionnalite.setOnMouseClicked(event -> {
			vBoxObjectifs.getChildren().remove(1);
			btnAjoutRessource.setId("btnMenu");
			btnAjoutTache.setId("btnMenu");
			btnFonctionnalite.setId("selectedBtn");
			vBoxObjectifs.getChildren().add(1, gridPaneGoalAdd);
		});

		btnAjoutRessource.setOnMouseClicked(event -> {
			vBoxObjectifs.getChildren().remove(1);
			btnAjoutRessource.setId("selectedBtn");
			btnFonctionnalite.setId("btnMenu");
			btnAjoutTache.setId("btnMenu");
			vBoxObjectifs.getChildren().add(1, hBoxAddRessourceGoal);
		});

		btnAjoutTache.setOnMouseClicked(event -> {
			vBoxObjectifs.getChildren().remove(1);
			btnAjoutTache.setId("selectedBtn");
			btnFonctionnalite.setId("btnMenu");
			btnAjoutRessource.setId("btnMenu");
			vBoxObjectifs.getChildren().add(1, vBoxAddTache);
		});

		/* LABEL, TEXTFIELD ET BUTTON */
		Label nom = new Label("Nom *: ");
		Label description = new Label("Description : ");
		Label dateDebut = new Label("Date de d�but : ");
		Label dateFin = new Label("Date de fin : ");
		Label labelTempsDate = new Label("Temps date : ");
		Label volumePriorite = new Label("Priorit� : ");
		Label categorie = new Label("Categorie : ");
		Label warning = new Label("");
		warning.setId("warning");

		TextField textFieldNom = new TextField();
		textFieldNom.setText(objectif.getNom());
		TextArea textAreaDescription = new TextArea();
		textAreaDescription.setText(objectif.getDescription());

		DatePicker datePikerDateDebut = new DatePicker();
		datePikerDateDebut.setValue(
				Instant.ofEpochMilli(objectif.getDateDebut().getTime()).atZone(ZoneId.systemDefault()).toLocalDate());
		DatePicker datePikerDateFin = new DatePicker();
		datePikerDateFin.setValue(
				Instant.ofEpochMilli(objectif.getDateFin().getTime()).atZone(ZoneId.systemDefault()).toLocalDate());
		datePikerDateFin.setDisable(true);
		dateFin.setDisable(true);

		TextField tempsDate = new TextField();
		tempsDate.setMaxWidth(50);
		long nbrJour = objectif.getDateFin().getTime() - objectif.getDateDebut().getTime();
		int res = (int) (nbrJour / (1000 * 60 * 60 * 24));
		tempsDate.setText(String.valueOf(res));
		Label jourLabel = new Label("jour");

		HBox dates = new HBox(dateDebut, datePikerDateDebut, dateFin, datePikerDateFin);
		dates.setSpacing(15);

		HBox hBoxTemps = new HBox(labelTempsDate, tempsDate, jourLabel);
		hBoxTemps.setSpacing(10);

		RadioButton radioDate = new RadioButton("Date-Dur�e/Date");

		radioDate.setOnAction(event -> {
			if (radioDate.isSelected()) {
				tempsDate.setDisable(true);
				labelTempsDate.setDisable(true);
				jourLabel.setDisable(true);
				int nbrJourNow = (int) (nbrJour / (1000 * 60 * 60 * 24));
				tempsDate.setText(String.valueOf(nbrJourNow));
				datePikerDateFin.setDisable(false);
				dateFin.setDisable(false);
			} else {
				tempsDate.setDisable(false);
				labelTempsDate.setDisable(false);
				jourLabel.setDisable(false);
				datePikerDateFin.setDisable(true);
				dateFin.setDisable(true);
				datePikerDateFin.setValue(Instant.ofEpochMilli(objectif.getDateFin().getTime())
						.atZone(ZoneId.systemDefault()).toLocalDate());
			}
		});

		ComboBox<VolumePriorite> comboBoxVolumePriorite = new ComboBox<>();
		comboBoxVolumePriorite.getItems().addAll(VolumePriorite.FAIBLE, VolumePriorite.NORMAL, VolumePriorite.FORTE);
		comboBoxVolumePriorite.setValue(objectif.getVolumePriorite());

		ComboBox<Categorie> comboBoxCategorie = new ComboBox<>();
		comboBoxCategorie.getItems().addAll(projet.getCategories());
		comboBoxCategorie.getItems().remove(projet.getCategorieVide());
		comboBoxCategorie.setValue(objectif.getCategorie());
		comboBoxCategorie.setMinWidth(100);
		comboBoxCategorie.setMaxWidth(comboBoxCategorie.getMinWidth());

		HBox hBoxLigne4 = new HBox(volumePriorite, comboBoxVolumePriorite, categorie, comboBoxCategorie);
		hBoxLigne4.setSpacing(60);

		Button btnValider = new Button(Constante.VALIDER);
		btnValider.setId("Valider");

		Button btnSupprimer = new Button("Supprimer");
		btnSupprimer.setId("Annuler");

		HBox btn = new HBox(btnValider, btnSupprimer);
		btn.setSpacing(150);
		btn.setId("barBtn");
		
		/* VERIF NOM BON */
		btnValider.setOnMouseClicked(event -> {
			if (comboBoxCategorie.getValue() == null) {
				comboBoxCategorie.setValue(projet.getCategorieVide());
			}

			if (tachesUpdate.isEmpty()) {
				tachesUpdate.put(1, new Tache("D�but"));
				tachesUpdate.put(2, new Tache("Fin"));
			}

			textFieldNom.setText(textFieldNom.getText().trim());

			comboBoxCategorie.getValue().getObjectifs().remove(objectif);
			Goal objectifNow = new Goal(textFieldNom.getText().trim(), comboBoxVolumePriorite.getValue(),
					textAreaDescription.getText().trim(), projet,
					new Date(datePikerDateDebut.getValue().getYear() - 1900,
							datePikerDateDebut.getValue().getMonthValue() - 1,
							datePikerDateDebut.getValue().getDayOfMonth()),
					new Date(datePikerDateFin.getValue().getYear() - 1900,
							datePikerDateFin.getValue().getMonthValue() - 1,
							datePikerDateFin.getValue().getDayOfMonth()),
					objectif.getRessources(), comboBoxCategorie.getValue(), tachesUpdate);
			comboBoxCategorie.getValue().getObjectifs().add(objectifNow);

			if (!(radioDate.isSelected())) {
				if (tempsDate.getText().matches("^[0-9]*")) {
					int nbrJourALaFin = Integer.parseInt(tempsDate.getText());
					LocalDate dateFinBonne = datePikerDateDebut.getValue().plusDays(nbrJourALaFin);
					objectifNow.setDateFin(new Date(dateFinBonne.getYear() - 1900, dateFinBonne.getMonthValue() - 1,
							dateFinBonne.getDayOfMonth()));
				} else {
					warning.setText("Erreur dans la dur�e");
				}
			}

			if (objectifNow.verif(warning, textFieldNom, textAreaDescription, projet, objectif.getNom())) {
				if (objectifNow.dateBonne(warning, datePikerDateFin)) {
					projet.getObjectifs().remove(objectif);

					int nbrTacheNow = 0;
					Iterator<Entry<Integer, Tache>> iterateurTache = objectifNow.getTaches().entrySet().iterator();
					while (iterateurTache.hasNext()) {
						Tache tache = iterateurTache.next().getValue();
						if (tache.isFait()) {
							nbrTacheNow++;
						}
					}

					objectifNow.setNbrTache(nbrTacheNow);

					if (!(objectif.getCategorie().equals(comboBoxCategorie.getValue()))) {
						objectif.getCategorie().getObjectifs().remove(objectif);
					}

					InterfaceProjet.historique(
							"l'objectif " + objectifNow.getNom() + " du projet " + projet.getNom() + " a �t� modifi� ");
					InterfaceProjet.modifier = true;
					projet.setObjectif(objectifNow);
					stage.close();
					interfaceProjet.pageProjet(projet, InterfaceProjet.pageCentreNum�ro, "");

				} else {
					comboBoxCategorie.getValue().getObjectifs().remove(objectifNow);
				}
			} else {
				comboBoxCategorie.getValue().getObjectifs().remove(objectifNow);
			}
		});

		btnSupprimer.setOnMouseClicked(event -> {
			comboBoxCategorie.getValue().getObjectifs().remove(objectif);
			projet.getObjectifs().remove(objectif);
			stage.close();
			interfaceProjet.pageProjet(projet, InterfaceProjet.pageCentreNum�ro, "");
		});

		gridPaneGoalAdd.add(nom, 0, 0);
		gridPaneGoalAdd.add(textFieldNom, 1, 0);
		gridPaneGoalAdd.add(description, 0, 1);
		gridPaneGoalAdd.add(textAreaDescription, 1, 1);
		gridPaneGoalAdd.add(radioDate, 0, 2, 2, 1);
		gridPaneGoalAdd.add(dates, 0, 3, 2, 1);
		gridPaneGoalAdd.add(hBoxTemps, 0, 4, 2, 1);
		gridPaneGoalAdd.add(hBoxLigne4, 0, 5, 2, 1);
		gridPaneGoalAdd.add(warning, 1, 7);

		vBoxObjectifs.getChildren().add(btn);
		return vBoxObjectifs;
	}

	public static void afficherClient(Projet projet) {
		Stage stage = new Stage();
		stage.initModality(Modality.APPLICATION_MODAL);
		stage.setResizable(false);

		Label nomClient = new Label(projet.getClient().getNomEntreprise());
		nomClient.setMaxWidth(Double.MAX_VALUE);
		nomClient.setId("titrePartie");
		Label adresse = new Label("Adresse : " + projet.getClient().getAdresseEntreprise());
		Label numeros = new Label("T�l�phone : " + projet.getClient().getNumEntreprise() + "		Fax : "
				+ projet.getClient().getFaxEntreprise());

		VBox vBox = new VBox();
		vBox.setPadding(new Insets(10));
		vBox.getChildren().addAll(nomClient, adresse, numeros);

		Scene scene = new Scene(vBox);
		scene.getStylesheets().add("CSS/" + InterfaceProjet.styleCss + ".css");

		stage.show();
		stage.setScene(scene);
		stage.setTitle("Affichage du client");
	}
}
