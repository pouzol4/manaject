package Projet;

import java.util.regex.Pattern;

public class Constante {
	
	private Constante() {}
	
	public static final String ANNULER = "Annuler";
	public static final String VALIDER = "Valider";
	public static final String ARIAL = "Arial";
	
	public static final int HAUTEUR_ECRAN = 505;
	public static final int LONGUEUR_ECRAN = 1000;
	
	public static final String CHEMIN_DOSSIER_SAVE = "./src/ressources/save/";
	public static final String FIN_RESSOURCE_TXT = "Ressource.txt";
	public static final String FIN_OBJECTIFS_TXT = "Objectifs.txt";
	public static final String GESTION_PROJET_SAVE = "./src/ressources/gestion Projet Save.txt";
	public static final String FIN_CATEGORIE_TXT = "Categorie.txt";
	public static final String FIN_TACHE_TXT = "Tache.txt";
	public static final String FIN_POINT_FORT_TXT = "PointFort.txt";
	public static final String FIN_CLIENT_TXT = "Client.txt";
	public static final String LOGO_PNG = "/imagesIcon/logo.png";
	
	
	private final static String VERIF_EMAIL = "^(.+)@(.+)$";
	public static final Pattern PATTERN_EMAIL = Pattern.compile(VERIF_EMAIL);
	private final static String VERIF_PHONE = "^[0-9]+[0-9] [0-9]+[0-9] [0-9]+[0-9] [0-9]+[0-9] [0-9]+[0-9]";
	public final static Pattern PATTERN_PHONE = Pattern.compile(VERIF_PHONE);
	
	public static final int TAILLE_ICON = 16;
	public static final double TAILLE_IMAGE_RESSOURCE = 25;

}
