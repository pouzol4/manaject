package Projet;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Scanner;
import java.util.TreeSet;

import Projet.affichage.InterfaceProjet;
import Projet.entitee.PointFort;
import Projet.entitee.Projet;
import Projet.entitee.Utilisateur;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class InscriptionConnexion {
	private static HashSet<Utilisateur> utilisateurs = new HashSet<>();
	private static Utilisateur utilisateur;

	public static HashSet<Utilisateur> getUtilisateurs() {
		return utilisateurs;
	}

	public static void setUtilisateurs(HashSet<Utilisateur> utilisateurs) {
		InscriptionConnexion.utilisateurs = utilisateurs;
	}

	public static void sauvegarderUtilisateurs() {
		try (FileWriter ecrivain = new FileWriter("./src/ressources/users.txt")) {
			Iterator<Utilisateur> iterateurUtilisateurs = InscriptionConnexion.utilisateurs.iterator();
			while (iterateurUtilisateurs.hasNext()) {
				Utilisateur utilisateur = iterateurUtilisateurs.next();
				ecrivain.write(utilisateur.getPrenom() + " " + utilisateur.getNom() + " " + utilisateur.getSurnom()
						+ " " + utilisateur.geteMail() + " " + utilisateur.getTelephone().replace(' ', '_') + " "
						+ utilisateur.getUser() + " " + utilisateur.getSalaireHorraire() + " " + utilisateur.getChemin()
						+ '\n');
				if(utilisateur.getUser()) {
					InscriptionConnexion.setUtilisateur(utilisateur);
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void lireUtilisateur() {
		try (FileReader lecteur = new FileReader("./src/ressources/users.txt"); Scanner s = new Scanner(lecteur)) {
			String nomRessources;
			String prenomRessources;
			String surnomRessources;
			String emailRessources;
			String telephoneRessources;
			boolean user;
			double salaire;
			String cheminPhoto;

			while (s.hasNext()) {
				nomRessources = s.next();
				prenomRessources = s.next();
				surnomRessources = s.next();
				emailRessources = s.next();
				telephoneRessources = s.next();
				user = s.nextBoolean();
				salaire = s.nextDouble();
				cheminPhoto = s.next();
				Utilisateur utilisateur = new Utilisateur(nomRessources.replace('_', ' '), prenomRessources,
						surnomRessources.replace('_', ' '), emailRessources, telephoneRessources, user, cheminPhoto,
						salaire, new TreeSet<PointFort>());
				
				if(utilisateur.getUser()) {
					InscriptionConnexion.setUtilisateur(utilisateur);
				}
				
				InscriptionConnexion.utilisateurs.add(utilisateur);

			}

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void inscriptionAffichage(Projet projet, Stage primaryStage) {
		Stage stageInscription = new Stage();
		Scene sceneInscription = new Scene(inscription(projet, primaryStage, stageInscription));

		sceneInscription.getStylesheets().add("CSS/" + InterfaceProjet.styleCss + ".css");
		stageInscription.setScene(sceneInscription);
		stageInscription.setTitle("Inscription");
		stageInscription.getIcons().add(new Image(Constante.LOGO_PNG));
		stageInscription.showAndWait();
	}

	public static GridPane inscription(Projet projet, Stage primaryStage, Stage stage) {
		/* INSCRIPTION */
		GridPane gridPaneInscription = new GridPane();
		gridPaneInscription.setHgap(10);
		gridPaneInscription.setVgap(10);
		gridPaneInscription.setPadding(new Insets(10, 10, 10, 10));

		Label titreInscription = new Label("INSCRIPTION");
		titreInscription.setMaxWidth(Double.MAX_VALUE);
		titreInscription.setId("titreInscription");

		Label nomUtilisateur = new Label("Nom* : ");
		nomUtilisateur.setId("labelCreation");
		nomUtilisateur.setMaxWidth(Double.MAX_VALUE);

		Label prenomUtilisateur = new Label("Pr�nom* : ");
		prenomUtilisateur.setId("labelCreation");
		prenomUtilisateur.setMaxWidth(Double.MAX_VALUE);

		Label emailUtilisateur = new Label("Email : ");
		emailUtilisateur.setId("labelCreation");
		emailUtilisateur.setMaxWidth(Double.MAX_VALUE);

		Label telephoneUtilisateur = new Label("Telephone : ");
		telephoneUtilisateur.setId("labelCreation");
		telephoneUtilisateur.setMaxWidth(Double.MAX_VALUE);

		Label warning = new Label("");
		warning.setId("warning");

		TextField textFieldNom = new TextField();
		textFieldNom.setPromptText("nom");
		TextField textFieldPrenom = new TextField();
		textFieldPrenom.setPromptText("Prenom");
		TextField textFieldEmail = new TextField();
		textFieldEmail.setPromptText("azerty@qsd.cvb");
		TextField textFieldTelephone = new TextField();
		textFieldTelephone.setPromptText("06 06 06 06 06");

		Button btnValider = new Button(Constante.VALIDER);
		btnValider.setId("Valider");

		Button btnAnnuler = new Button(Constante.ANNULER);
		btnAnnuler.setId("Annuler");

		HBox btn = new HBox(btnValider, btnAnnuler);
		btn.setSpacing(100);
		btn.setId("barBtn");

		btnValider.setOnMouseClicked(eventValider -> {

			Utilisateur utilisateur = new Utilisateur(textFieldNom.getText(), textFieldPrenom.getText(), null,
					textFieldEmail.getText(), textFieldTelephone.getText(), true, "nothing", 0.0,
					new TreeSet<PointFort>());
			if (utilisateur.verif(warning, projet, textFieldNom, textFieldPrenom, textFieldEmail, textFieldTelephone,
					false)) {
				if (!(InterfaceProjet.textFieldVide(textFieldTelephone))) {
					textFieldTelephone.setText(textFieldTelephone.getText().replace(' ', '_'));
				}
				textFieldPrenom.setText(textFieldPrenom.getText().toLowerCase());
				char maj = Character.toUpperCase(textFieldPrenom.getText().charAt(0));
				textFieldPrenom.setText(textFieldPrenom.getText()
						.replaceFirst(String.valueOf(textFieldPrenom.getText().charAt(0)), String.valueOf(maj)));

				Utilisateur utilisateurBon = new Utilisateur(textFieldNom.getText(), textFieldPrenom.getText(), null,
						textFieldEmail.getText(), textFieldTelephone.getText(), true, "nothing", 0.0,
						new TreeSet<PointFort>());

				try (FileWriter ecrivainUser = new FileWriter("./src/ressources/user.txt")) {
					ecrivainUser.write(textFieldNom.getText().toUpperCase() + " " + textFieldPrenom.getText() + " "
							+ null + " " + textFieldEmail.getText() + " " + textFieldTelephone.getText() + " " + true);

					InscriptionConnexion.utilisateurs.add(utilisateurBon);
					InscriptionConnexion.sauvegarderUtilisateurs();
					stage.close();
					projet.nouveau(primaryStage, new Label(""));
				} catch (IOException e) {
					e.printStackTrace();
				}

				try (FileWriter ecrivainUser = new FileWriter("./src/ressources/historique.txt")) {
					ecrivainUser.write("");
				} catch (IOException e) {
					e.printStackTrace();
				}
			}

		});

		btnAnnuler.setOnMouseClicked(eventAnnuler -> System.exit(0));

		gridPaneInscription.add(titreInscription, 0, 0, 4, 1);
		gridPaneInscription.add(prenomUtilisateur, 0, 1);
		gridPaneInscription.add(textFieldPrenom, 1, 1);
		gridPaneInscription.add(nomUtilisateur, 2, 1);
		gridPaneInscription.add(textFieldNom, 3, 1);
		gridPaneInscription.add(emailUtilisateur, 0, 2);
		gridPaneInscription.add(textFieldEmail, 1, 2);
		gridPaneInscription.add(telephoneUtilisateur, 2, 2);
		gridPaneInscription.add(textFieldTelephone, 3, 2);
		gridPaneInscription.add(btn, 0, 4, 4, 1);
		gridPaneInscription.add(warning, 0, 5, 4, 1);

		return gridPaneInscription;
	}

	public static void page(Projet projet, Stage primaryStage) {
		Stage stageInscriptionConnexion = new Stage();

		Button connexionChoix = new Button("Connexion");
		connexionChoix.setId("selectedBtn");
		connexionChoix.setMinWidth(250);

		Button inscriptionChoix = new Button("Inscription");
		inscriptionChoix.setId("btnMenu");
		inscriptionChoix.setMinWidth(250);

		HBox choixInscriptionConnexion = new HBox(connexionChoix, inscriptionChoix);
		VBox vBoxInscriptionConnexion = new VBox(choixInscriptionConnexion,
				InscriptionConnexion.connexion(projet, primaryStage, stageInscriptionConnexion));

		connexionChoix.setOnMouseClicked(eventConnexion -> {
			connexionChoix.setId("selectedBtn");
			inscriptionChoix.setId("btnMenu");
			vBoxInscriptionConnexion.getChildren().set(1,
					InscriptionConnexion.connexion(projet, primaryStage, stageInscriptionConnexion));
		});

		inscriptionChoix.setOnMouseClicked(eventInscription -> {
			connexionChoix.setId("btnMenu");
			inscriptionChoix.setId("selectedBtn");
			vBoxInscriptionConnexion.getChildren().set(1,
					InscriptionConnexion.inscription(projet, primaryStage, stageInscriptionConnexion));

		});

		Scene sceneInscriptionConnexion = new Scene(vBoxInscriptionConnexion);
		sceneInscriptionConnexion.getStylesheets().add("CSS/" + InterfaceProjet.styleCss + ".css");

		stageInscriptionConnexion.setScene(sceneInscriptionConnexion);
		stageInscriptionConnexion.setTitle("Connexion/Inscription");
		stageInscriptionConnexion.show();
	}

	private static GridPane connexion(Projet projet, Stage primaryStage, Stage stageInscriptionConnexion) {
		GridPane gridPane = new GridPane();
		gridPane.setHgap(20);
		gridPane.setVgap(20);
		gridPane.setAlignment(Pos.CENTER);
		gridPane.setPadding(new Insets(10));

		Label titre = new Label("CONNEXION");
		titre.setMaxWidth(Double.MAX_VALUE);
		titre.setId("titreInscription");

		Label prenom = new Label("Pr�nom : ");
		Label nom = new Label("Nom :");
		Label attention = new Label("");
		attention.setId("warning");

		TextField prenomTextField = new TextField();
		prenomTextField.setPromptText("pr�nom");
		TextField nomTextField = new TextField();
		nomTextField.setPromptText("nom");

		Button valider = new Button("Valider");
		valider.setId("Valider");

		Button annuler = new Button("Annuler");
		annuler.setId("Annuler");
		
		HBox buttons = new HBox(100, valider, annuler);
		buttons.setAlignment(Pos.CENTER);

		gridPane.add(titre, 0, 0, 4, 1);
		gridPane.add(prenom, 0, 1);
		gridPane.add(prenomTextField, 1, 1);
		gridPane.add(nom, 2, 1);
		gridPane.add(nomTextField, 3, 1);
		gridPane.add(buttons, 0, 2, 4, 1);
		gridPane.add(attention, 0, 3, 4, 1);
		return gridPane;
	}

	public static Utilisateur getUtilisateur() {
		return utilisateur;
	}

	public static void setUtilisateur(Utilisateur utilisateur) {
		InscriptionConnexion.utilisateur = utilisateur;
	}
}
