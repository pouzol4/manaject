package Projet.affichage;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Optional;
import java.util.TreeSet;

import Projet.Constante;
import Projet.InscriptionConnexion;
import Projet.autre.FonctionnaliteInterfaceProjet;
import Projet.entitee.PointFort;
import Projet.entitee.Projet;
import Projet.entitee.Ressource;
import Projet.entitee.Tache;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonType;
import javafx.scene.control.CheckMenuItem;
import javafx.scene.control.Label;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.SeparatorMenuItem;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCodeCombination;
import javafx.scene.input.KeyCombination;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.stage.Stage;

public class InterfaceProjet {

	Label info = new Label("");
	public static boolean afficheRessourceBool = true;
	public static int pageCentreNuméro = 0;
	public static boolean modifier = false;
	public static String styleCss = "OriginalTheme";

	private PageCentre pageCentre = new PageCentre();
	Stage primaryStage;

	public InterfaceProjet(Stage primaryStage) {
		this.primaryStage = primaryStage;
	}

	public void pageProjet(Projet projet, int pageCentrale, String rechercheEnCours) {
		BorderPane projetBorderPane = new BorderPane();
		Scene scene = new Scene(projetBorderPane, Constante.LONGUEUR_ECRAN, Constante.HAUTEUR_ECRAN);
		scene.getStylesheets().add("CSS/" + InterfaceProjet.styleCss + ".css");

		VBox vBoxNord = this.creerVBoxNord(projet, primaryStage);

		if (afficheRessourceBool) {
			VBox vBoxRessources = this.vBoxEst(projet);
			projetBorderPane.setLeft(vBoxRessources);
		}

		/* CREATION DU BORDERPANE */
		projetBorderPane.setTop(vBoxNord);
		projetBorderPane.setCenter(pageCentre.creer(projet, primaryStage, pageCentrale, rechercheEnCours));
		projetBorderPane.setBottom(info);
		BorderPane.setMargin(projetBorderPane.getCenter(), new Insets(0, 0, 0, 10));

		/* SCENE */
		scene.addEventFilter(KeyEvent.KEY_PRESSED, new EventHandler<KeyEvent>() {
			final KeyCombination keyCombSave = new KeyCodeCombination(KeyCode.S, KeyCombination.CONTROL_DOWN);
			final KeyCombination keyCombOpenProject = new KeyCodeCombination(KeyCode.O, KeyCombination.CONTROL_DOWN);
			final KeyCombination keyCombNewProject = new KeyCodeCombination(KeyCode.N, KeyCombination.CONTROL_DOWN);

			public void handle(KeyEvent event) {
				if (keyCombSave.match(event)) {
					try {
						projet.sauvegarder(info, primaryStage);
					} catch (IOException e) {
						e.printStackTrace();
					}
				}

				if (keyCombOpenProject.match(event)) {
					FonctionnaliteInterfaceProjet.ouvrirProjet(projet, primaryStage, false, info);
				}

				if (keyCombNewProject.match(event)) {
					FonctionnaliteInterfaceProjet.nouveauProjet(primaryStage, false, projet, info);
				}

			}
		});

		primaryStage.setOnCloseRequest(event -> {
			projet.sauvegardeSaveProjet();
			InscriptionConnexion.sauvegarderUtilisateurs();
			if (modifier) {
				Alert fermeture = new Alert(AlertType.CONFIRMATION);

				fermeture.setHeaderText(null);
				fermeture.setTitle("Femeture projet");
				fermeture.setContentText("Voulez-vous sauvegarder ?");

				ButtonType sauvegarder = new ButtonType("sauvegarder");
				ButtonType quitter = new ButtonType("quitter");
				ButtonType annuler = new ButtonType("annuler");
				fermeture.getButtonTypes().setAll(sauvegarder, quitter, annuler);

				Optional<ButtonType> choix = fermeture.showAndWait();

				if (choix.get() == sauvegarder) {
					try {
						projet.sauvegarder(info, primaryStage);
					} catch (IOException e) {
						e.printStackTrace();
					}
				}

				if (choix.get() == quitter) {
					System.exit(0);
				}

				if (choix.get() == annuler) {

				}
			}
		});

		primaryStage.setScene(scene);

		String titrePrimaryStage;

		if (modifier) {
			titrePrimaryStage = projet.getNom() + "*";
		} else {
			titrePrimaryStage = projet.getNom();
		}

		primaryStage.setTitle("Manaject - " + titrePrimaryStage);
		primaryStage.getIcons().add(new Image(Constante.LOGO_PNG));
		primaryStage.show();
	}

	public static void historique(String ajout) {
		try (BufferedWriter bufferedWriter = new BufferedWriter(
				new FileWriter("./src/ressources/historique.txt", true))) {
			bufferedWriter.write(
					LocalDateTime.now().format(DateTimeFormatter.ofPattern("dd:MM:yyyy hh:mm:ss")) + " : " + ajout);
			bufferedWriter.newLine();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static boolean textFieldVide(TextField textField) {
		return textField == null || textField.getText() == null || "".equals(textField.getText());
	}

	private VBox creerVBoxNord(Projet projet, final Stage primaryStage) {
		VBox vBox = new VBox();
		VBox menu = new VBox();
		MenuBar menuBar = new MenuBar();

		/* MENU */
		Menu menuFichier = new Menu("Fichier");
		Menu menuAffichage = new Menu("Affichage");
		Menu menuGestion = new Menu("Gestion");
		Menu menuProfil = new Menu("Profil");
		Menu menuAide = new Menu("Aide");
		
		/*MENU DANS MENU GESTION*/
		Menu menuObjectifs = new Menu("Objectifs");
		Menu menuCategories = new Menu("Catégories");

		/* SEPARATOR MENU ITEM */
		SeparatorMenuItem separatorMenuItemFile1 = new SeparatorMenuItem();
		SeparatorMenuItem seperatorMenuItemFile2 = new SeparatorMenuItem();

		/*MENU ITEM FICHIER*/
		MenuItem newProject = new MenuItem("Nouveau Projet				CTRL + N");
		MenuItem saveProject = new MenuItem("Sauvegarder Projet				CTRL + S");
		MenuItem openProject = new MenuItem("Ouvrir Projet					CTRL + O");
		MenuItem deleteProject = new MenuItem("Supprimer Projet");
		MenuItem importProject = new MenuItem("Importer Projet");
		MenuItem exportProjet = new MenuItem("Exporter Projet");
		MenuItem exitProject = new MenuItem("Quitter");
		
		/*MENU ITEM AFFICHAGE*/
		MenuItem afficherClient = new MenuItem("Afficher le client");
		
		/*MENU ITEM GESTION*/
		MenuItem addRessource = new MenuItem("Ajouter Ressource");
		MenuItem addCategorie = new MenuItem("Ajouter une catégorie");
		MenuItem rangerObjectif = new MenuItem("Ranger un objectif");
		MenuItem addGoal = new MenuItem("Ajouter Objectif");
		MenuItem modifierGoal = new MenuItem("Modifier un objectif");
		
		/*MENU ITEM PROFIL*/
		MenuItem deconnecter = new MenuItem("Déconnecter");
		
		/*MENU ITEM AIDE*/
		MenuItem aideLogiciel = new MenuItem("Aide sur le logiciel");
		MenuItem parametres = new MenuItem("Paramètres");

		CheckMenuItem afficheRessource = new CheckMenuItem("Afficher les ressources");
		afficheRessource.setSelected(afficheRessourceBool);

		/* =============ACTIONS============== */
		/* NEW PROJECT */
		newProject.setOnAction(new EventHandler<ActionEvent>() {
			public void handle(ActionEvent eventNewProjet) {
				try {
					projet.sauvegarder(info, primaryStage);
				} catch (Exception e) {

					System.err.println("Sauvegarde mauvaise");
					System.exit(1);
				}

				FonctionnaliteInterfaceProjet.nouveauProjet(primaryStage, false, projet, info);
			}
		});

		/* SAVE */
		saveProject.setOnAction(new EventHandler<ActionEvent>() {
			public void handle(ActionEvent event) {
				try {
					projet.sauvegarder(info, primaryStage);
				} catch (Exception e) {

					System.err.println("Sauvegarde mauvaise");
					System.exit(1);
				}
			}
		});

		/* OPEN PROJECT */
		openProject.setOnAction(new EventHandler<ActionEvent>() {
			public void handle(ActionEvent eventOpenProject) {
				try {
					projet.sauvegarder(info, primaryStage);
				} catch (IOException e) {
					e.printStackTrace();
				}

				File repertoireSave = new File("./src/ressources/save");
				String liste[] = repertoireSave.list();

				if (liste.length >= 2) {
					FonctionnaliteInterfaceProjet.ouvrirProjet(projet, primaryStage, false, info);
				} else {
					info.setText("Ouverture d'un nouveau projet Impossible : Aucun projet peut être ouvert");
				}
			}
		});

		/* SUPPRIMER PROJET */
		deleteProject.setOnAction(new EventHandler<ActionEvent>() {
			public void handle(ActionEvent event) {
				File repertoireSave = new File("./src/ressources/save");
				String liste[] = repertoireSave.list();

				if (liste.length > 1) {
					FonctionnaliteInterfaceProjet.ouvrirProjet(projet, primaryStage, true, info);
				} else {
					FonctionnaliteInterfaceProjet.nouveauProjet(primaryStage, true, projet, info);
					try {
						primaryStage.close();
						projet.nouveau(primaryStage, info);
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}
		});

		/* EXIT */
		exitProject.setOnAction(new EventHandler<ActionEvent>() {
			public void handle(ActionEvent event) {
				try {
					projet.sauvegarder(info, primaryStage);
				} catch (IOException e) {
					System.err.println("Sauvegarde mauvaise");
					System.exit(1);
				}
				System.exit(0);
			}
		});

		/* AFFICHER LE CLIENT */
		afficherClient.setOnAction(new EventHandler<ActionEvent>() {
			public void handle(ActionEvent event) {
				FonctionnaliteInterfaceProjet.afficherClient(projet);
			}
		});

		/* AFFICHER RESSOURCE */

		afficheRessource.setOnAction(new EventHandler<ActionEvent>() {
			public void handle(ActionEvent event) {
				FonctionnaliteInterfaceProjet.afficheRessource(event, projet, primaryStage);
			}
		});

		/* AJOUTER RESSOURCES */
		addRessource.setOnAction(new EventHandler<ActionEvent>() {
			public void handle(ActionEvent event) {
				FonctionnaliteInterfaceProjet.addRessource(projet, primaryStage, info, new TreeSet<PointFort>());
			}
		});

		/* AJOUTER CATEGORIE */
		addCategorie.setOnAction(new EventHandler<ActionEvent>() {
			public void handle(ActionEvent event) {
				FonctionnaliteInterfaceProjet.ajouterCategorie(projet, primaryStage, info);
			}
		});

		/* RANGER OBJECTIF */
		rangerObjectif.setOnAction(new EventHandler<ActionEvent>() {

			public void handle(ActionEvent arg0) {
				FonctionnaliteInterfaceProjet.rangerObjectif(projet, primaryStage, info);
			}
		});

		/* AJOUTER ETAPES */
		addGoal.setOnAction(new EventHandler<ActionEvent>() {
			public void handle(ActionEvent event) {
				FonctionnaliteInterfaceProjet.ajoutObjectif(primaryStage, projet, info, new HashSet<Ressource>(),
						new HashMap<Integer, Tache>());
			}
		});

		modifierGoal.setOnAction(new EventHandler<ActionEvent>() {
			public void handle(ActionEvent event) {
				if (projet.getObjectifs().isEmpty()) {
					info.setText("Impossible : il y a pas d'objectifs");
				} else {
					FonctionnaliteInterfaceProjet.updateObjectif(projet, primaryStage, null);
				}

			}
		});
		
		deconnecter.setOnAction(new EventHandler<ActionEvent>() {
			public void handle(ActionEvent event) {
				primaryStage.close();
				InscriptionConnexion.page(projet, primaryStage);
			}
		});

		aideLogiciel.setOnAction(new EventHandler<ActionEvent>() {
			public void handle(ActionEvent event) {
				Aide.creerAide();
			}
		});
		
		parametres.setOnAction(new EventHandler<ActionEvent>() {
			public void handle(ActionEvent event) {
				Parametres.afficherParametres(primaryStage, projet);
			}
		});

		/* AJOUT MENU ITEM */
		menuFichier.getItems().addAll(newProject, saveProject, openProject, deleteProject, separatorMenuItemFile1,
				importProject, exportProjet, seperatorMenuItemFile2, exitProject);

		if (projet.getClient() == null) {
			menuAffichage.getItems().addAll(afficheRessource);
		} else {
			menuAffichage.getItems().addAll(afficheRessource, afficherClient);
		}

		menuObjectifs.getItems().addAll(addGoal, modifierGoal);
		menuCategories.getItems().addAll(addCategorie,rangerObjectif);
		menuGestion.getItems().addAll(addRessource, menuObjectifs, menuCategories);
		menuProfil.getItems().addAll(deconnecter);
		menuAide.getItems().addAll(aideLogiciel,parametres);

		/* MENU BAR */
		menuBar.getMenus().addAll(menuFichier, menuAffichage, menuGestion, menuProfil, menuAide);
		menu.getChildren().add(menuBar);
		menuBar.setMinWidth(Constante.LONGUEUR_ECRAN);

		MenuIcon menuIcon = new MenuIcon();

		/* LABEL TITRE */

		vBox.getChildren().addAll(menu, menuIcon.menuIconFabricator(primaryStage, projet, info));
		return vBox;
	}

	public VBox vBoxEst(Projet projet) {
		VBox vBoxRessources = new VBox();
		GridPane ressourcesGridPane = new GridPane();
		ressourcesGridPane.setVgap(5);

		Label titreRessources = new Label("Ressources");
		titreRessources.setFont(new Font(Constante.ARIAL, 20));

		int nbrLabelRessource = 0;

		Iterator<Ressource> monIterateur = projet.getRessources().iterator();
		while (monIterateur.hasNext()) {
			Ressource ressources = monIterateur.next();

			ImageView imageView;
			if (ressources.getChemin().equals("nothing")) {
				char ch;

				if (ressources.getSurnom() == null || ressources.getSurnom().equals("null")) {
					ch = ressources.getNom().charAt(0);
				} else {
					ch = ressources.getSurnom().charAt(0);
				}

				if ((ch < 'A' && ch > 'Z') || (ch < 'a' && ch > 'z')) {
					ch = '#';
				}
				imageView = new ImageView(new Image(getClass().getResourceAsStream("/lettre/" + ch + ".png")));

			} else {
				imageView = new ImageView(new Image(ressources.getChemin()));
			}

			imageView.setFitWidth(Constante.TAILLE_IMAGE_RESSOURCE);
			imageView.setFitHeight(Constante.TAILLE_IMAGE_RESSOURCE);

			Label nomRessource = new Label();
			if (!("null".equals(ressources.getSurnom())) && ressources.getSurnom() != null) {
				nomRessource.setText(ressources.getSurnom());
			} else {
				nomRessource.setText(ressources.getNom() + " " + ressources.getPrenom());
			}

			if (ressources.getUser()) {
				nomRessource.setText(nomRessource.getText() + " (MOI)");
			}
			ressourcesGridPane.add(imageView, 0, nbrLabelRessource);
			ressourcesGridPane.add(nomRessource, 1, nbrLabelRessource);
			ressourcesGridPane.setHgap(5);
			nbrLabelRessource++;
		}
		ScrollPane scrollPaneEst = new ScrollPane(ressourcesGridPane);
		scrollPaneEst.setMinWidth(160);
		scrollPaneEst.setMaxWidth(160);

		vBoxRessources.getChildren().addAll(titreRessources, scrollPaneEst);
		vBoxRessources.setPadding(new Insets(0, 0, 0, 5));
		return vBoxRessources;
	}

	public void ecritSaveRien() {
		try (FileWriter ecrivain = new FileWriter("./src/ressources/gestion Projet Save.txt")) {
			ecrivain.write("rien\n" + InterfaceProjet.afficheRessourceBool);
		} catch (Exception e) {
			System.err.println("Erreur");
		}

	}
}
