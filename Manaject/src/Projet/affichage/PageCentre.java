package Projet.affichage;

import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;
import java.util.Iterator;
import java.util.Map.Entry;
import java.util.TreeSet;

import Projet.Constante;
import Projet.InscriptionConnexion;
import Projet.VariableCommune;
import Projet.entitee.Categorie;
import Projet.entitee.EtatObjectif;
import Projet.entitee.Goal;
import Projet.entitee.PointFort;
import Projet.entitee.Projet;
import Projet.entitee.Ressource;
import Projet.entitee.Tache;
import Projet.entitee.Utilisateur;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.stage.Stage;

public class PageCentre {

	private Button objectifsGlobaux = new Button("Objectifs");
	private Button vosObjectifs = new Button("Vos objectifs");
	private Button ressourcesDetails = new Button("Detail des ressources");
	private Button gant = new Button("Diagramme de Gant");
	private HBox hBoxChoix = new HBox(objectifsGlobaux, vosObjectifs, ressourcesDetails, gant);
	private VBox vBoxCenter = new VBox();
	private Label labelTitre = new Label();
	private VBox center = new VBox(labelTitre, hBoxChoix);
	private GridPane gridPaneCentre = new GridPane();

	public VBox creer(Projet projet, Stage primaryStage, int page, String rechercheEnCours) {
		labelTitre.setText(projet.getNom().toUpperCase());
		labelTitre.setFont(Font.font(Constante.ARIAL, FontWeight.BOLD, 30));
		labelTitre.setMaxWidth(Double.MAX_VALUE);
		labelTitre.setAlignment(Pos.CENTER);

		vBoxCenter.setPadding(new Insets(0, 10, 0, 0));
		vBoxCenter.setSpacing(5);

		Iterator<Goal> iterateurObjectif = projet.getObjectifs().iterator();
		while (iterateurObjectif.hasNext()) {
			Goal objectif = iterateurObjectif.next();

			Date today = Date.from(LocalDate.now().atStartOfDay(ZoneId.systemDefault()).toInstant());
			
			if(!(objectif.getEtat().equals(EtatObjectif.FAIT) && !(objectif.getEtat().equals(EtatObjectif.EN_COURS)))){
				if (objectif.getDateDebut().compareTo(today) < 0 && objectif.getDateFin().compareTo(today) >= 0) {
					objectif.setEtat(EtatObjectif.WARNING);

					if (objectif.getDateFin().compareTo(today) < 0
							&& objectif.getDateDebut().compareTo(today) >= 0) {
						objectif.setEtat(EtatObjectif.EN_RETARD);
					}
				}	
			}

			if ((objectif.getEtat().equals(EtatObjectif.A_FAIRE) || objectif.getEtat().equals(EtatObjectif.WARNING)
					|| objectif.getEtat().equals(EtatObjectif.EN_RETARD)) && objectif.getNbrTache() != 0
					&& objectif.getNbrTache() != objectif.getTaches().size()) {
				objectif.setEtat(EtatObjectif.EN_COURS);
			}

			if (objectif.getNbrTache() == objectif.getTaches().size()) {
				objectif.setEtat(EtatObjectif.FAIT);
			}
		}

		int numeroLigne = 0;
		this.afficherCategorie(projet, gridPaneCentre, numeroLigne, primaryStage, rechercheEnCours);
		gridPaneCentre.setHgap(10);

		Label titreObjectifs = new Label("OBJECTIFS");
		titreObjectifs.setMaxWidth(Double.MAX_VALUE);
		titreObjectifs.setId("titrePartie");

		Label rechercheEtatObjectif = new Label("Etat Objectif : ");
		CheckBox checkoxAFaire = new CheckBox("A faire");
		checkoxAFaire.setSelected(VariableCommune.aFaireAfficher);

		CheckBox checkoxEnCours = new CheckBox("En cours");
		checkoxEnCours.setSelected(VariableCommune.enCoursAfficher);

		CheckBox checkBoxFait = new CheckBox("Fait");
		checkBoxFait.setSelected(VariableCommune.faitAfficher);

		CheckBox checkBoxAuRetard = new CheckBox("En retard");
		checkBoxAuRetard.setSelected(VariableCommune.auRetardAfficher);

		CheckBox checkBoxWarning = new CheckBox("Attention");
		checkBoxWarning.setSelected(VariableCommune.warningAfficher);

		Label rechercher = new Label("Rechercher : ");
		TextField rechercherTextField = new TextField();
		if (rechercheEnCours.equals("")) {
			rechercherTextField.setPromptText("Recherche");
		} else {
			rechercherTextField.setText(rechercheEnCours);
		}
		rechercherTextField.setId("RechercherObjectif");

		rechercherTextField.setOnAction(event -> {
			gridPaneCentre.getChildren().clear();
			this.afficherCategorie(projet, gridPaneCentre, numeroLigne, primaryStage, rechercherTextField.getText());
		});

		HBox hBoxRechercher = new HBox();
		hBoxRechercher.getChildren().addAll(rechercher, rechercherTextField);

		HBox critereDeRecherche = new HBox();
		critereDeRecherche.getChildren().addAll(rechercheEtatObjectif, checkoxAFaire, checkoxEnCours, checkBoxFait,
				checkBoxAuRetard, checkBoxWarning, hBoxRechercher);
		critereDeRecherche.setSpacing(35);

		ScrollPane scrollPaneCentre = new ScrollPane(gridPaneCentre);

		if (page == 0) {
			vosObjectifs.setId("btnMenu");
			ressourcesDetails.setId("btnMenu");
			objectifsGlobaux.setId("selectedBtn");
			gant.setId("btnMenu");
			vBoxCenter.getChildren().addAll(titreObjectifs, critereDeRecherche, scrollPaneCentre);
			center.getChildren().add(vBoxCenter);
		}

		if (page == 1) {
			vosObjectifs.setId("selectedBtn");
			ressourcesDetails.setId("btnMenu");
			objectifsGlobaux.setId("btnMenu");
			gant.setId("btnMenu");
			vBoxCenter.getChildren().clear();
			HBox vosObjectifs = mesObjectifs(projet, primaryStage);
			center.getChildren().add(vosObjectifs);
		}

		if (page == 2) {
			vosObjectifs.setId("btnMenu");
			ressourcesDetails.setId("selectedBtn");
			objectifsGlobaux.setId("btnMenu");
			gant.setId("btnMenu");
			vBoxCenter.getChildren().clear();
			VBox vBoxToutesRessources = affichageRessourceDetail(projet, primaryStage);
			center.getChildren().add(vBoxToutesRessources);
		}

		if (page == 3) {
			vosObjectifs.setId("btnMenu");
			ressourcesDetails.setId("btnMenu");
			objectifsGlobaux.setId("btnMenu");
			gant.setId("selectedBtn");
			vBoxCenter.getChildren().clear();
			VBox vBoxGant = creerUnGant(projet);
			center.getChildren().add(vBoxGant);
		}

		objectifsGlobaux.setOnMouseClicked(eventFiche -> {
			vosObjectifs.setId("btnMenu");
			ressourcesDetails.setId("btnMenu");
			objectifsGlobaux.setId("selectedBtn");
			gant.setId("btnMenu");
			vBoxCenter.getChildren().clear();
			vBoxCenter.getChildren().addAll(titreObjectifs, critereDeRecherche, scrollPaneCentre);
			InterfaceProjet.pageCentreNuméro = 0;
			center.getChildren().set(2, vBoxCenter);
		});

		vosObjectifs.setOnMouseClicked(eventFiche -> {
			objectifsGlobaux.setId("btnMenu");
			ressourcesDetails.setId("btnMenu");
			vosObjectifs.setId("selectedBtn");
			gant.setId("btnMenu");
			InterfaceProjet.pageCentreNuméro = 1;
			HBox vosObjectifsCentre = mesObjectifs(projet, primaryStage);
			center.getChildren().set(2, vosObjectifsCentre);
		});

		ressourcesDetails.setOnMouseClicked(eventFiche -> {
			objectifsGlobaux.setId("btnMenu");
			ressourcesDetails.setId("selectedBtn");
			vosObjectifs.setId("btnMenu");
			gant.setId("btnMenu");
			InterfaceProjet.pageCentreNuméro = 2;
			VBox vBoxToutesRessources = affichageRessourceDetail(projet, primaryStage);
			center.getChildren().set(2, vBoxToutesRessources);
		});

		gant.setOnMouseClicked(eventGant -> {
			objectifsGlobaux.setId("btnMenu");
			ressourcesDetails.setId("btnMenu");
			vosObjectifs.setId("btnMenu");
			gant.setId("selectedBtn");
			InterfaceProjet.pageCentreNuméro = 3;
			VBox vBoxGant = creerUnGant(projet);
			center.getChildren().set(2, vBoxGant);
		});

		checkoxAFaire.setOnAction(event -> {
			VariableCommune.aFaireAfficher = checkoxAFaire.isSelected();
			projet.sauvegardeSaveProjet();
			gridPaneCentre.getChildren().clear();
			this.afficherCategorie(projet, gridPaneCentre, numeroLigne, primaryStage, rechercheEnCours);
		});

		checkoxEnCours.setOnAction(event -> {
			VariableCommune.enCoursAfficher = checkoxEnCours.isSelected();
			projet.sauvegardeSaveProjet();
			gridPaneCentre.getChildren().clear();
			this.afficherCategorie(projet, gridPaneCentre, numeroLigne, primaryStage, rechercheEnCours);
		});

		checkBoxFait.setOnAction(event -> {
			VariableCommune.faitAfficher = checkBoxFait.isSelected();
			projet.sauvegardeSaveProjet();
			gridPaneCentre.getChildren().clear();
			this.afficherCategorie(projet, gridPaneCentre, numeroLigne, primaryStage, rechercheEnCours);
		});

		checkBoxAuRetard.setOnAction(event -> {
			VariableCommune.auRetardAfficher = checkBoxAuRetard.isSelected();
			projet.sauvegardeSaveProjet();
			gridPaneCentre.getChildren().clear();
			this.afficherCategorie(projet, gridPaneCentre, numeroLigne, primaryStage, rechercheEnCours);
		});

		checkBoxWarning.setOnAction(event -> {
			VariableCommune.warningAfficher = checkBoxWarning.isSelected();
			projet.sauvegardeSaveProjet();
			gridPaneCentre.getChildren().clear();
			this.afficherCategorie(projet, gridPaneCentre, numeroLigne, primaryStage, rechercheEnCours);
		});

		hBoxChoix.setId("btnMenuBar");
		return center;
	}

	private VBox creerUnGant(Projet projet) {
		VBox vBox = new VBox();
		vBox.setPadding(new Insets(0, 10, 0, 0));

		Label titre = new Label("DIAGRAMME DE GANT");
		titre.setMaxWidth(Double.MAX_VALUE);
		titre.setId("titrePartie");

		GridPane gridPane = new GridPane();
		LocalDate date = LocalDate.now();
		gridPane.add(new Label(String.valueOf(date.getYear())), 1, 0);
		gridPane.add(new Label(String.valueOf(date.getMonthValue())), 1, 1);
		gridPane.add(new Label(String.valueOf(date.getDayOfMonth())), 1, 2);

		int j = 2;
		while (j < 50) {
			date = date.plusDays(1);
			gridPane.add(new Label(String.valueOf(date.getDayOfMonth())), j, 2);

			if (date.getDayOfMonth() == 15) {
				gridPane.add(new Label(String.valueOf(date.getMonthValue())), j, 1);
			}

			if (date.getMonthValue() == 6) {
				gridPane.add(new Label(String.valueOf(date.getYear())), j, 0);
			}
			j++;
		}

		Iterator<Goal> iterateurObjectif = projet.getObjectifs().iterator();
		int i = 3;
		while (iterateurObjectif.hasNext()) {
			Label nomObjectif = new Label(iterateurObjectif.next().getNom());
			gridPane.add(nomObjectif, 0, i);
			i++;
		}

		vBox.getChildren().addAll(titre, new ScrollPane(gridPane));
		return vBox;
	}

	private VBox affichageRessourceDetail(Projet projet, Stage primaryStage) {
		VBox vBox = new VBox();
		vBox.setMaxWidth(Double.MAX_VALUE);

		Label titre = new Label("DETAILS DES RESSOURCES");
		titre.setMaxWidth(Double.MAX_VALUE);
		titre.setId("titrePartie");

		GridPane gridPane = new GridPane();
		gridPane.setMaxWidth(Double.MAX_VALUE);
		gridPane.setMaxHeight(Double.MAX_VALUE);
		gridPane.setHgap(40);
		gridPane.setVgap(40);

		ScrollPane scrollPane = new ScrollPane(gridPane);

		scrollPane.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);

		int ligneNum = 0;
		int colonneNum = 0;
		int nbrRessourceMax = 3;

		Iterator<Ressource> iterateurRessource = projet.getRessources().iterator();
		while (iterateurRessource.hasNext()) {
			InterfaceProjet interfaceProjet = new InterfaceProjet(primaryStage);
			Ressource ressource = iterateurRessource.next();
			VBox vBoxRessource = new VBox();
			ressource.affichageDetails(170.0, vBoxRessource);
			vBoxRessource.setId("gridPanneAfficherRessource");

			Button modifier = new Button("Modifier");
			modifier.setId("Valider");
			Button supprimer = new Button("Supprimer");
			supprimer.setId("Annuler");

			HBox btnsModifSuppr;
			if (ressource.getUser()) {
				btnsModifSuppr = new HBox(modifier);
			} else {
				btnsModifSuppr = new HBox(modifier, supprimer);
			}

			btnsModifSuppr.setSpacing(70);
			vBoxRessource.getChildren().add(btnsModifSuppr);

			modifier.setOnMouseClicked(event -> {
				vBoxRessource.getChildren().clear();

				Label email = new Label("Email : ");
				email.setId("labelAfficherRessource");
				Label tel = new Label("Telephone : ");
				tel.setId("labelAfficherRessource");

				Label salaireHorraire = new Label("Salaire horraire : ");
				salaireHorraire.setId("labelAfficherRessource");
				Label eurosParHeure = new Label("€/h");
				eurosParHeure.setId("labelAfficherRessource");
				Label warning = new Label("");
				warning.setId("warning");

				TextField nomRessource = new TextField(ressource.getNom());
				nomRessource.setPromptText("nom");
				TextField prenomRessource = new TextField(ressource.getPrenom());
				prenomRessource.setPromptText("prenom");

				TextField surnomRessource = new TextField(ressource.getSurnom());
				surnomRessource.setPromptText("surnom");
				if (ressource.getSurnom() == null || "null".equals(ressource.getSurnom())) {
					surnomRessource.setText("");
				}

				TextField emailRessource = new TextField(ressource.geteMail());
				emailRessource.setMinWidth(173);
				emailRessource.setPromptText("azerty@ghjk.mlp");
				if (ressource.geteMail() == null || "null".equals(ressource.geteMail())) {
					emailRessource.setText("");
				}

				TextField telRessource = new TextField();
				telRessource.setMaxWidth(147);
				telRessource.setPromptText("06 06 06 06 06");
				if (ressource.getTelephone() != null && !("null".equals(ressource.getTelephone()))) {
					telRessource.setText(ressource.getTelephone().replace('_', ' '));
				}

				TextField coutHorraireRessource = new TextField(String.valueOf(ressource.getSalaireHorraire()));
				coutHorraireRessource.setMaxWidth(50);

				Button buttonPlus = new Button("+");
				buttonPlus.setId("AjoutPointForts");

				TreeSet<PointFort> pntsForts = new TreeSet<>();
				GridPane gridPanePntsForts = new GridPane();
				int collonePntFort = 0;
				Iterator<PointFort> iterateurPntsFortsModif = ressource.getPointsForts().iterator();
				while (iterateurPntsFortsModif.hasNext()) {
					PointFort pntFort = iterateurPntsFortsModif.next();
					pntsForts.add(pntFort);
					Button buttonPntsForts = new Button(pntFort.getNom());
					buttonPntsForts.setId("EnleverPointForts");
					gridPanePntsForts.add(buttonPntsForts, collonePntFort, 0);
					collonePntFort++;

					buttonPntsForts.setOnAction(eventEnleverPntsForts -> {
						gridPanePntsForts.getChildren().remove(buttonPntsForts);
						pntsForts.remove(pntFort);

						if (pntsForts.size() < projet.getPointForts().size()
								&& !(gridPanePntsForts.getChildren().contains(buttonPlus))) {
							gridPanePntsForts.add(buttonPlus, GridPane.getColumnIndex(buttonPntsForts),
									GridPane.getRowIndex(buttonPntsForts));
						}
					});
				}

				if (pntsForts.size() < projet.getPointForts().size()) {
					gridPanePntsForts.add(buttonPlus, collonePntFort, 0);
				}

				buttonPlus.setOnAction(eventAjoutPointsForts -> {
					if (projet.getPointForts().size() > pntsForts.size()) {

						HBox hBoxAjoutPointsForts = new HBox();

						ComboBox<PointFort> pointsFortsRestant = new ComboBox<>();
						pointsFortsRestant.setId("PointsFortsComboboxModif");
						pointsFortsRestant.getItems().addAll(projet.getPointForts());
						pointsFortsRestant.getItems().removeAll(pntsForts);
						pointsFortsRestant.setValue(pointsFortsRestant.getItems().get(0));

						Button valPointFort = new Button("V");
						valPointFort.setId("AjoutPointForts");

						hBoxAjoutPointsForts.getChildren().addAll(pointsFortsRestant, valPointFort);
						int colonneButtonPlus = GridPane.getColumnIndex(buttonPlus);
						int ligneButtonPlus = GridPane.getRowIndex(buttonPlus);
						gridPanePntsForts.getChildren().remove(buttonPlus);
						gridPanePntsForts.add(hBoxAjoutPointsForts, colonneButtonPlus, ligneButtonPlus);

						valPointFort.setOnAction(eventValider -> {
							gridPanePntsForts.getChildren().remove(hBoxAjoutPointsForts);
							PointFort pointFortAjoute = pointsFortsRestant.getValue();
							Button buttonNewPointForts = new Button(pointFortAjoute.getNom());
							buttonNewPointForts.setId("EnleverPointForts");

							buttonNewPointForts.setOnAction(eventEnleverNouveauPntsForts -> {
								gridPanePntsForts.getChildren().remove(buttonNewPointForts);
								pntsForts.remove(pointFortAjoute);
								if (pntsForts.size() < projet.getPointForts().size()
										&& !(gridPanePntsForts.getChildren().contains(buttonPlus))) {
									gridPanePntsForts.add(buttonPlus, GridPane.getColumnIndex(buttonNewPointForts),
											GridPane.getRowIndex(buttonNewPointForts));
								}
							});

							int colonneHBoxPointsForts = GridPane.getColumnIndex(hBoxAjoutPointsForts);
							int ligneHBoxPointsForts = GridPane.getRowIndex(hBoxAjoutPointsForts);
							gridPanePntsForts.add(buttonNewPointForts, colonneHBoxPointsForts, ligneHBoxPointsForts);
							pntsForts.add(pointFortAjoute);
							if (pntsForts.size() < projet.getPointForts().size()) {
								gridPanePntsForts.add(buttonPlus, colonneHBoxPointsForts + 1, ligneHBoxPointsForts);
							}
						});
					}
				});

				Button valider = new Button("Valider");
				valider.setId("Valider");
				Button annuler = new Button("Annuler");
				annuler.setId("Annuler");

				valider.setOnMouseClicked(eventValider -> {
					prenomRessource.setText(prenomRessource.getText().toLowerCase());
					prenomRessource.setText(
							prenomRessource.getText().replaceFirst(String.valueOf(prenomRessource.getText().charAt(0)),
									String.valueOf(Character.toUpperCase(prenomRessource.getText().charAt(0)))));
					if (!(InterfaceProjet.textFieldVide(coutHorraireRessource))) {
						if (coutHorraireRessource.getText().matches("^[0-9]*")
								|| coutHorraireRessource.getText().matches("^[0-9]*.[0-9]")
								|| coutHorraireRessource.getText().matches("^[0-9]*.[0-9][0-9]")) {
							Ressource bonneRessource = new Ressource(nomRessource.getText().toUpperCase(),
									prenomRessource.getText(), surnomRessource.getText(), emailRessource.getText(),
									telRessource.getText().replace(' ', '_'), ressource.getUser(),
									ressource.getChemin(), Double.valueOf(coutHorraireRessource.getText()), pntsForts);
							if (ressource.verif(warning, projet, nomRessource, prenomRessource, emailRessource,
									telRessource, true)) {
								bonneRessource.setTelephone(telRessource.getText().replace(' ', '_'));
								bonneRessource.setEmail(emailRessource.getText());

								if (InterfaceProjet.textFieldVide(surnomRessource)) {
									bonneRessource.setSurnom("null");
								}

								if (ressource.getUser()) {
									InscriptionConnexion.getUtilisateurs().removeIf(r -> r.getUser());
									Utilisateur utilisateur = new Utilisateur(nomRessource.getText().toUpperCase(),
											prenomRessource.getText(), surnomRessource.getText(),
											emailRessource.getText(), telRessource.getText().replace(' ', '_'),
											ressource.getUser(), ressource.getChemin(),
											Double.valueOf(coutHorraireRessource.getText()), pntsForts);

									InscriptionConnexion.getUtilisateurs().add(utilisateur);
									try (FileWriter ecrivainUser = new FileWriter("./src/ressources/user.txt")) {
										ecrivainUser.write(bonneRessource.getNom().toUpperCase() + " "
												+ bonneRessource.getPrenom() + " " + bonneRessource.getSurnom() + " "
												+ bonneRessource.geteMail() + " " + bonneRessource.getTelephone() + " "
												+ true + " " + bonneRessource.getSalaireHorraire() + " "
												+ bonneRessource.getChemin());
									} catch (IOException e) {
										e.printStackTrace();
									}
								}

								projet.getRessources().remove(ressource);
								projet.getRessources().add(bonneRessource);
								InterfaceProjet.modifier = true;
								InterfaceProjet.historique("La ressource " + ressource.getNom() + " "
										+ ressource.getPrenom() + " du projet " + projet.getNom() + " a été modifié");
								interfaceProjet.pageProjet(projet, InterfaceProjet.pageCentreNuméro, "");
							}
						} else {
							warning.setText("Le salaire horraire : valeure impossible");
						}
					} else {
						warning.setText("Veuillez indiquez un salaire horraire");
					}
				});

				annuler.setOnMouseClicked(eventAnnuler -> {
					vBoxRessource.getChildren().clear();
					ressource.affichageDetails(170.0, vBoxRessource);

					HBox btnsModifSupprimer;
					if (ressource.getUser()) {
						btnsModifSupprimer = new HBox(modifier);
					} else {
						btnsModifSupprimer = new HBox(modifier, supprimer);
					}
					btnsModifSupprimer.setSpacing(70);
					vBoxRessource.getChildren().add(btnsModifSupprimer);

				});

				HBox hBoxNomPrenom = new HBox(10, nomRessource, prenomRessource);
				HBox hBoxEmail = new HBox(10, email, emailRessource);
				HBox hBoxTel = new HBox(10, tel, telRessource);
				HBox hBoxSalaireHorraire = new HBox(10, salaireHorraire, coutHorraireRessource, eurosParHeure);
				HBox btnsModif = new HBox(70, valider, annuler);

				vBoxRessource.getChildren().addAll(hBoxNomPrenom, surnomRessource, hBoxEmail, hBoxTel,
						hBoxSalaireHorraire, gridPanePntsForts, btnsModif, warning);

			});

			supprimer.setOnMouseClicked(eventSupprimer -> {
				
				projet.getRessources().remove(ressource);

				Iterator<Goal> iterateurObjectif = projet.getObjectifs().iterator();
				while (iterateurObjectif.hasNext()) {
					Goal objectif = iterateurObjectif.next();
					if (objectif.getRessources().contains(ressource)) {
						objectif.getRessources().remove(ressource);
					}
				}
				InterfaceProjet.modifier = true;
				InterfaceProjet.historique("La ressource " + ressource.getNom() + " " + ressource.getPrenom()
						+ " du projet " + projet.getNom() + " a été supprimé");

				interfaceProjet.pageProjet(projet, InterfaceProjet.pageCentreNuméro, "");
			});

			if (ligneNum >= nbrRessourceMax) {
				ligneNum = 0;
				colonneNum++;
			}

			gridPane.add(vBoxRessource, ligneNum, colonneNum);
			ligneNum++;
		}

		vBox.getChildren().addAll(titre, scrollPane);
		vBox.setPadding(new Insets(0, 10, 0, 10));
		return vBox;
	}

	private HBox mesObjectifs(Projet projet, Stage primaryStage) {

		VBox vBoxAFaire = affichageObjectifsPerso(projet, EtatObjectif.A_FAIRE, "#A4FFE5", primaryStage);
		VBox vBoxEnCours = affichageObjectifsPerso(projet, EtatObjectif.EN_COURS, "#FFEEA4", primaryStage);
		VBox vBoxFait = affichageObjectifsPerso(projet, EtatObjectif.FAIT, "#CBFFA4", primaryStage);

		HBox hBoxVosObjectifs = new HBox(vBoxAFaire, vBoxEnCours, vBoxFait);
		hBoxVosObjectifs.setSpacing(10);
		hBoxVosObjectifs.setAlignment(Pos.CENTER);
		hBoxVosObjectifs.setPadding(new Insets(10, 0, 10, 0));
		return hBoxVosObjectifs;
	}

	private VBox affichageObjectifsPerso(Projet projet, EtatObjectif etatObjectif, String color, Stage primaryStage) {
		GridPane gridPane = new GridPane();
		gridPane.setHgap(10);
		gridPane.setVgap(10);
		gridPane.setPadding(new Insets(10, 10, 10, 10));

		ScrollPane scrollPane = new ScrollPane(gridPane);
		scrollPane.setMinSize(265, 309);
		scrollPane.setMaxWidth(scrollPane.getMinWidth());

		Label titrePartie = new Label("OBJECTIFS " + etatObjectif.getNom().toUpperCase());
		titrePartie.setMaxWidth(Double.MAX_VALUE);
		titrePartie.setId("titrePartie");

		VBox vBox = new VBox(titrePartie, scrollPane);

		int numeroLigne = 0;
		int numeroColonne = 0;
		Iterator<Goal> iterateurObjectifs = projet.getObjectifs().iterator();
		while (iterateurObjectifs.hasNext()) {
			Goal objectif = iterateurObjectifs.next();

			if (objectif.getEtat().equals(EtatObjectif.WARNING) || objectif.getEtat().equals(EtatObjectif.EN_RETARD)) {
				objectif.setEtat(EtatObjectif.A_FAIRE);
			}

			if (objectif.getEtat().equals(etatObjectif)) {
				Iterator<Ressource> iterateurRessource = objectif.getRessources().iterator();
				while (iterateurRessource.hasNext()) {
					Ressource ressource = iterateurRessource.next();
					if (ressource.getUser()) {
						Label titre = new Label(objectif.getNom().toUpperCase());
						titre.setId("titreVosObjectifs");
						titre.setMaxWidth(Double.MAX_VALUE);

						Label description = new Label(objectif.getDescription());
						description.setId("descriptionVosObjectifs");

						VBox taches = new VBox();
						if (!(objectif.getEtat().equals(EtatObjectif.FAIT))) {
							Iterator<Entry<Integer, Tache>> iterateurTache = objectif.getTaches().entrySet().iterator();
							while (iterateurTache.hasNext()) {
								Tache tache = iterateurTache.next().getValue();
								CheckBox tacheCheckBox = new CheckBox(tache.getNom());
								tacheCheckBox.setSelected(tache.isFait());
								if (tache.isFait()) {
									tacheCheckBox.setDisable(true);
								}

								tacheCheckBox.setId("AffichageTache");
								tacheCheckBox.setMaxWidth(Double.MAX_VALUE);
								taches.getChildren().add(tacheCheckBox);

								tacheCheckBox.setOnAction(event -> {
									tache.setFait(true);
									objectif.addNbrTache();
									tacheCheckBox.setDisable(true);
									
									if(objectif.getNbrTache()==objectif.getTaches().size()) {
										objectif.objectifclot();
									}
									InterfaceProjet.modifier = true;
									InterfaceProjet interfaceProjet = new InterfaceProjet(primaryStage);
									interfaceProjet.pageProjet(projet, InterfaceProjet.pageCentreNuméro, "");
								});
							}
						}

						VBox vBoxObjectif = new VBox(titre, description, taches);
						vBoxObjectif.setMinWidth(116);
						vBoxObjectif.setMaxWidth(vBoxObjectif.getMinWidth());
						vBoxObjectif.setSpacing(10);
						vBoxObjectif.setBackground(
								new Background(new BackgroundFill(Color.web(color), CornerRadii.EMPTY, Insets.EMPTY)));
						gridPane.add(vBoxObjectif, numeroColonne, numeroLigne);
						numeroColonne++;
						if (numeroColonne >= 2) {
							numeroLigne++;
							numeroColonne = 0;
						}
					}
				}
			}
			Date today = Date.from(LocalDate.now().atStartOfDay(ZoneId.systemDefault()).toInstant());
			if (objectif.getDateDebut().compareTo(today) < 0
					&& !(objectif.getEtat().equals(EtatObjectif.FAIT) && objectif.getDateFin().compareTo(today) >= 0)
					&& !(objectif.getEtat().equals(EtatObjectif.EN_COURS))) {
				objectif.setEtat(EtatObjectif.WARNING);
			}

			if (objectif.getDateFin().compareTo(today) < 0
					&& !(objectif.getEtat().equals(EtatObjectif.FAIT) && objectif.getDateDebut().compareTo(today) >= 0)
					&& !(objectif.getEtat().equals(EtatObjectif.EN_COURS))) {
				objectif.setEtat(EtatObjectif.EN_RETARD);
			}
		}
		return vBox;
	}

	private void afficherCategorie(Projet projet, GridPane gridPaneObjectifs, int numeroLigne, Stage primaryStage,
			String rechercheEnCours) {

		Iterator<Categorie> iterateurCategorie = projet.getCategories().iterator();
		while (iterateurCategorie.hasNext()) {
			Categorie categorie = iterateurCategorie.next();

			Label nomCategorie = new Label("SANS CATEGORIES");
			if (!("Vide".equals(categorie.getNom()))) {
				nomCategorie = new Label(categorie.getNom().toUpperCase());
			}

			if (!("Vide".equals(categorie.getNom()) && categorie.getObjectifs().isEmpty())) {
				nomCategorie.setFont(Font.font("Arial", FontWeight.BOLD, 12));
				nomCategorie.setMaxWidth(Double.MAX_VALUE);

				nomCategorie.setTextFill(Color.web(categorie.getColorEcriture()));
				nomCategorie.setBackground(new Background(
						new BackgroundFill(Color.web(categorie.getColorFond()), CornerRadii.EMPTY, Insets.EMPTY)));
				gridPaneObjectifs.add(nomCategorie, 0, numeroLigne, 8, 1);
				numeroLigne++;
				numeroLigne += afficherObjectifs(gridPaneObjectifs, categorie, numeroLigne, primaryStage,
						rechercheEnCours);
			}
		}
	}

	private int afficherObjectifs(GridPane gridPaneObjectifs, Categorie categorie, int numeroLigne, Stage primaryStage,
			String rechercheEnCours) {
		int nbrLigne = 0;

		Iterator<Goal> iterateurObjectif = categorie.getObjectifs().iterator();
		while (iterateurObjectif.hasNext()) {
			Goal objectif = iterateurObjectif.next();

			if (VariableCommune.aFaireAfficher && objectif.getNom().contains(rechercheEnCours)
					&& objectif.getEtat().equals(EtatObjectif.A_FAIRE)) {
				objectif.affichage(gridPaneObjectifs, numeroLigne, primaryStage);
				numeroLigne++;
				nbrLigne++;
			}

			if (VariableCommune.enCoursAfficher && objectif.getNom().contains(rechercheEnCours)
					&& objectif.getEtat().equals(EtatObjectif.EN_COURS)) {
				objectif.affichage(gridPaneObjectifs, numeroLigne, primaryStage);
				numeroLigne++;
				nbrLigne++;
			}

			if (VariableCommune.faitAfficher && objectif.getNom().contains(rechercheEnCours)
					&& objectif.getEtat().equals(EtatObjectif.FAIT)) {
				objectif.affichage(gridPaneObjectifs, numeroLigne, primaryStage);
				numeroLigne++;
				nbrLigne++;
			}

			if (VariableCommune.warningAfficher && objectif.getNom().contains(rechercheEnCours)
					&& objectif.getEtat().equals(EtatObjectif.WARNING)) {
				objectif.affichage(gridPaneObjectifs, numeroLigne, primaryStage);
				numeroLigne++;
				nbrLigne++;
			}

			if (VariableCommune.auRetardAfficher && objectif.getNom().contains(rechercheEnCours)
					&& objectif.getEtat().equals(EtatObjectif.EN_RETARD)) {
				objectif.affichage(gridPaneObjectifs, numeroLigne, primaryStage);
				numeroLigne++;
				nbrLigne++;
			}
		}
		return nbrLigne;

	}

}
