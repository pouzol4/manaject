package Projet.affichage;

import java.io.File;
import java.util.Optional;

import Projet.Constante;
import Projet.autre.FonctionnaliteInterfaceProjet;
import Projet.entitee.Projet;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.image.Image;
import javafx.scene.layout.GridPane;
import javafx.stage.Modality;
import javafx.stage.Stage;

public class Parametres {

	private Parametres() {
	}

	public static void afficherParametres(Stage primaryStage, Projet projet) {
		Stage stage = new Stage();
		stage.initModality(Modality.APPLICATION_MODAL);
		stage.setResizable(false);
		
		GridPane gridPanePrincipal = new GridPane();
		gridPanePrincipal.setVgap(10);
		gridPanePrincipal.setPadding(new Insets(10));

		Label titre = new Label("PARAMETRES");
		titre.setId("titrePartie");

		Label styleLabel = new Label("Style : ");
		ComboBox<String> style = new ComboBox<>();
		style.getItems().addAll("OriginalTheme", "BlackTheme");
		style.setValue(InterfaceProjet.styleCss);

		style.setOnAction(new EventHandler<ActionEvent>() {
			public void handle(ActionEvent event) {
				InterfaceProjet.styleCss = style.getValue();
				stage.close();
				primaryStage.close();
				InterfaceProjet interfaceProjet = new InterfaceProjet(primaryStage);
				interfaceProjet.pageProjet(projet, InterfaceProjet.pageCentreNum�ro, "");
				Parametres.afficherParametres(primaryStage, projet);
			}
		});

		Button renitialiser = new Button("R�nitialiser le logiciel");
		renitialiser.setId("Renitialisation");

		renitialiser.setOnAction(new EventHandler<ActionEvent>() {
			public void handle(ActionEvent event) {
				Parametres.renitialiserAlert(stage, primaryStage);
			}
		});

		gridPanePrincipal.add(titre, 0, 0, 2, 1);
		gridPanePrincipal.add(styleLabel, 0, 1);
		gridPanePrincipal.add(style, 1, 1);
		gridPanePrincipal.add(renitialiser, 0, 2,2,1);

		Scene scene = new Scene(gridPanePrincipal);
		scene.getStylesheets().add("CSS/" + InterfaceProjet.styleCss + ".css");

		stage.setScene(scene);
		stage.getIcons().add(new Image(Constante.LOGO_PNG));
		stage.setTitle("Param�tres");
		stage.show();

	}

	protected static void renitialiserAlert(Stage stageParametre, Stage primaryStage) {
		Alert verifRenitialisation = new Alert(AlertType.CONFIRMATION);
		verifRenitialisation.setTitle("Confirmation de la R�nitialisation");
		verifRenitialisation.setHeaderText(
				"Voulez vraiment r�nitialiser le logiciel ? \nCette requ�te vous ferra perdre toutes vos donn�es !");
		ButtonType buttonSur = new ButtonType("J'en suis s�r");
		ButtonType buttonRefuser = new ButtonType("Refuser cette action");
		
		verifRenitialisation.getButtonTypes().setAll(buttonSur, buttonRefuser);
		Optional<ButtonType> choix = verifRenitialisation.showAndWait();
		
		if(choix.get()==buttonSur) {
			stageParametre.close();
			primaryStage.close();
			FonctionnaliteInterfaceProjet.emptyDirectory(new File("./src/ressources/"));
			System.exit(0);
		}
		
		if(choix.get()==buttonRefuser) {
			verifRenitialisation.close();
		}

	}

}
