package Projet.affichage;

import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.TreeSet;

import Projet.Constante;
import Projet.autre.FonctionnaliteInterfaceProjet;
import Projet.entitee.PointFort;
import Projet.entitee.Projet;
import Projet.entitee.Ressource;
import Projet.entitee.Tache;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;

public class MenuIcon {

	public HBox menuIconFabricator(Stage primaryStage, Projet projet, Label info) {
		HBox hBoxMenuItem = new HBox();
		hBoxMenuItem.getChildren().addAll(nouveauMenuItem(primaryStage, projet, info), openMenuItem(primaryStage, projet, info),
				saveMenuItem(projet, info, primaryStage), new Label("    "), nouvelleRessource(primaryStage, projet, info),
				nouvelObjectif(projet, primaryStage, info), nouveauCategorie(projet, primaryStage, info));
		
		hBoxMenuItem.setId("btnsIconsBar");
		return hBoxMenuItem;
	}

	private Button nouveauCategorie(Projet projet, Stage primaryStage, Label info) {
		Image nouvelCategorie = new Image(getClass().getResourceAsStream("/imagesIcon/ajouterCategorie.png"));
		ImageView nouvelCategorieView = new ImageView(nouvelCategorie);
		nouvelCategorieView.setFitHeight(Constante.TAILLE_ICON);
		nouvelCategorieView.setFitWidth(Constante.TAILLE_ICON);
		Button buttonNouvelCategorie = new Button("", nouvelCategorieView);
		buttonNouvelCategorie.setPadding(new Insets(1, 4, 1, 4));

		buttonNouvelCategorie.setOnMouseClicked(event -> FonctionnaliteInterfaceProjet.ajouterCategorie(projet, primaryStage, info));
		
		buttonNouvelCategorie.setId("btnsIcons");
		buttonNouvelCategorie.setTooltip(new Tooltip("nouvelle catégorie"));
		return buttonNouvelCategorie;
	}

	private Button nouvelObjectif(Projet projet, Stage primaryStage, Label info) {
		Image nouvelObjectif = new Image(getClass().getResourceAsStream("/imagesIcon/ajouterObjectif.png"));
		ImageView nouvelObjectifView = new ImageView(nouvelObjectif);
		nouvelObjectifView.setFitHeight(Constante.TAILLE_ICON);
		nouvelObjectifView.setFitWidth(Constante.TAILLE_ICON);
		Button buttonNouvelObjectif = new Button("", nouvelObjectifView);
		buttonNouvelObjectif.setPadding(new Insets(1, 4, 1, 4));

		buttonNouvelObjectif.setOnMouseClicked(event -> {
			FonctionnaliteInterfaceProjet.ajoutObjectif(primaryStage, projet, info, new HashSet<Ressource>(), new HashMap<Integer, Tache>());
		});

		buttonNouvelObjectif.setId("btnsIcons");
		buttonNouvelObjectif.setTooltip(new Tooltip("nouvel objectif"));
		return buttonNouvelObjectif;
	}

	private Button openMenuItem(Stage primaryStage, Projet projet, Label info) {
		Image openProjet = new Image(getClass().getResourceAsStream("/imagesIcon/openProject.png"));
		ImageView openProjectView = new ImageView(openProjet);
		openProjectView.setFitHeight(Constante.TAILLE_ICON);
		openProjectView.setFitWidth(Constante.TAILLE_ICON);
		Button buttonOpenProject = new Button("", openProjectView);
		buttonOpenProject.setPadding(new Insets(1, 4, 1, 4));

		buttonOpenProject.setOnMouseClicked(event -> {
			FonctionnaliteInterfaceProjet.ouvrirProjet(projet, primaryStage, false, info);
		});
		
		buttonOpenProject.setId("btnsIcons");
		buttonOpenProject.setTooltip(new Tooltip("ouvrir un projet"));
		return buttonOpenProject;
	}

	private Button nouvelleRessource(Stage primaryStage, Projet projet, Label info) {
		Image newRessource = new Image(getClass().getResourceAsStream("/imagesIcon/newRessource.png"));
		ImageView newRessourceView = new ImageView(newRessource);
		newRessourceView.setFitHeight(Constante.TAILLE_ICON);
		newRessourceView.setFitWidth(Constante.TAILLE_ICON);
		Button buttonNewRessource = new Button("", newRessourceView);
		buttonNewRessource.setPadding(new Insets(1, 4, 1, 4));

		buttonNewRessource.setOnMouseClicked(event -> {
			FonctionnaliteInterfaceProjet.addRessource(projet, primaryStage, info, new TreeSet<PointFort>());

		});
		
		buttonNewRessource.setId("btnsIcons");
		buttonNewRessource.setTooltip(new Tooltip("nouvelle ressource"));
		return buttonNewRessource;
	}

	private Button saveMenuItem(Projet projet, Label info, Stage primaryStage) {
		Image saveFile = new Image(getClass().getResourceAsStream("/imagesIcon/saveFile.png"));
		ImageView saveFileView = new ImageView(saveFile);
		saveFileView.setFitHeight(Constante.TAILLE_ICON);
		saveFileView.setFitWidth(Constante.TAILLE_ICON);
		Button buttonSave = new Button("", saveFileView);
		buttonSave.setPadding(new Insets(1, 4, 1, 4));

		buttonSave.setOnMouseClicked(event -> {
			try {
				projet.sauvegarder(info, primaryStage);
			} catch (IOException e) {
				e.printStackTrace();
			}
		});
		
		buttonSave.setId("btnsIcons");
		buttonSave.setTooltip(new Tooltip("sauvegarder"));
		return buttonSave;
	}

	private Button nouveauMenuItem(Stage primaryStage, Projet projet, Label info) {
		Image newFile = new Image(getClass().getResourceAsStream("/imagesIcon/newFile.png"));
		ImageView newFileView = new ImageView(newFile);
		newFileView.setFitHeight(Constante.TAILLE_ICON);
		newFileView.setFitWidth(Constante.TAILLE_ICON);
		Button buttonNew = new Button("", newFileView);
		buttonNew.setPadding(new Insets(1, 4, 1, 4));

		buttonNew.setOnMouseClicked(event -> FonctionnaliteInterfaceProjet.nouveauProjet(primaryStage, false, projet, info));

		buttonNew.setId("btnsIcons");
		buttonNew.setTooltip(new Tooltip("nouveau projet"));
		return buttonNew;
	}
}
