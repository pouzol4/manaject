package Projet.affichage;


import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class Aide {

	public static void creerAide() {
		Label titrePrincipal = new Label("AIDE");
		titrePrincipal.setId("titrePrincipal");
		titrePrincipal.setMaxWidth(Double.MAX_VALUE);
		
		Label titreProjet = new Label("I- PROJET");
		titreProjet.setId("titrePartie");
		
		Label titreRessources = new Label("II- RESSOURCES");
		titreRessources.setId("titrePartie");
		
		Label titrePointsForts = new Label("III- POINTS FORTS");
		titrePointsForts.setId("titrePartie");
		
		Label titreObjectifs = new Label("IV- OBJECTIFS");
		titreObjectifs.setId("titrePartie");
		
		Label titreCategories = new Label("V- CATEGORIES");
		titreCategories.setId("titrePartie");
		
		Label titreTaches = new Label("VI- TACHES");
		titreTaches.setId("titrePartie");
		
		VBox vBox = new VBox();
		vBox.getChildren().addAll(titreProjet,aideProjet(), titreRessources,titrePointsForts,titreObjectifs, titreCategories, titreTaches);
		
		ScrollPane scrollPane = new ScrollPane(vBox);
		scrollPane.setMinSize(480, 320);
		scrollPane.setMaxSize(scrollPane.getMinWidth(), scrollPane.getMinHeight());
		
		VBox vBoxPrincipal = new VBox(titrePrincipal, scrollPane);
		vBoxPrincipal.setPadding(new Insets(0,10,10,10));
		
		Scene scene = new Scene(vBoxPrincipal, 500, 400);
		scene.getStylesheets().add("CSS/" + InterfaceProjet.styleCss + ".css");
		
		Stage stageAide = new Stage();
		stageAide.setScene(scene);
		stageAide.setTitle("Aide");
		stageAide.show();
	}

	private static Label aideProjet() {
		Label labelAideProjet = new Label("	Sur ce logiciel, vous avez la possibilit� de g�rer l'�tat de plusieurs de vos projets. Les \nprojets cr��s vous permettront de g�rer les objectifs � r�aliser dans celui-ci et les \nressources qui devront le r�aliser");
		return labelAideProjet;
	}

}
