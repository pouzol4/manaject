# Manaject (Juin 2020 - Juillet 2020)

_Cette application réalisée en JAVA est un logiciel décidé de façon personnelle ayant pour but final d'aider à mieux gérer l'avancée d'un projet. Il m'a permis de me perfectionner d'avantages en JAVA, en JAVAFX et de découvrir les difficultés de réaliser un projet personnel._

## Comment utiliser le projet ?

Avoir un IDE pour le java tel que eclypse et avoir le framework JavaFX

## Etat du projet

Non-abouti : il reste une multitude de fonctionnalités telle que celle pour l'implémentation d'un GANT et de bugs.

## Langages

**FrontEnd :** JAVAFX

**BackEnd :** JAVA

## Auteur

Hadrien POUZOL
